# Who this training is for
- Businesses just getting started
- Businesses with lumpy sales & cash flow
- Businesses where the owner's capacity is getting maxed out

---

# My goal for this FREE training

How to grow a build a leak-proof lead-generation system for your business that'll keep you booked all summer - and beyond!

;;;

# What we'll cover

1. The 3 systems you NEED to build if you want to have a business that creates FREEDOM for you...
1. The 4 stages of growth from "Start" to "Scale" <!-- .element: class="fragment" -->
2. The biggest challenges to watch out for at each stage <!-- .element: class="fragment" -->
3. The biggest OPPORTUNITIES to LEVERAGE at each stage <!-- .element: class="fragment" -->
4. A step-by-step plan for each stage! <!-- .element: class="fragment" -->

---

# About Me

I'm the owner of SuperWebPros and we've done nearly 1,000 website projects over the past 4-5 years.

Note:
- Including many moving companies
- Maybe seen us on the 7 Figure Moving Academy 
- Also a local business owner
- Also built a software company
- (Don't hold it against me) Have an MBA & management role for a billion-dollar company.
- Full gamut: small to large, local, and tech
- A lot of experience to help you out
- Fox and ABC (Expert Connexions)

;;;

# Last quarter, I reviewed data for dozens of customers and found patterns for nearly all local businesses...

;;;

# Which led to this GrowthMap for Local Businesses:

<iframe width="768" height="432" src="https://miro.com/app/live-embed/o9J_lHFQ38I=/?moveToViewport=-2731,-1605,4876,2793" frameBorder="0" scrolling="no" allowFullScreen></iframe>

---

# Let's start with a misconception

The Internet WILL NOT solve all of your growth problems.

- It WILL NOT print customers on demand
- It IS NOT "free"
- it IS NOT "easy"
- Also, your competitors are probably NOT doing it well

;;;

# But it does allow LEVERAGE

Note:
- So you can accomplish the goals and dreams you have for your business

---

# The 3 Systems you need to have for growth

1. Customer Attraction System - a way to GET people in the door
2. Customer Conversion System - a way to turn leads into customers <!-- .element: class="fragment" -->
3. Customer Retention & Referral System - a way to increase value & referrals <!-- .element: class="fragment" -->

;;;

# At different stages of growth, your focus for each part differs...

---

# How local businesses ACTUALLY grow

;;;

# 4 Stages

- Scale
- Systematize <!-- .element: class="fragment" -->
- Sell <!-- .element: class="fragment" -->
- Serve <!-- .element: class="fragment" -->

---

# Different Businesses have Different Needs at Different Stages

- Different cash flow
- Different assets
- Different resources
- Different strategies

;;;

# What's True For You:

_You are not a large company with "unlimited" resources_

(So don't try to be)
(You'll go broke...fast) <!-- .element: class="fragment" -->

---

# So, Let's Dive into the 4 Stages of Growth and how YOU can profitably attract, convert, retain, and extend more customers....

;;;

# Stage 1 ($0 - $100k)

| What | How |
| --- | --- |
| **Focus On:** | Figuring out 'who you serve' and your USP |
| **Biggest Challenge:** | The grind. Finding people who can help you. Earning customer trust. |
| **Biggest Opportunity:** | Your personal relationships. Speed & Flexibility |

;;;

# How to do this

1. Set up your Google Business Profile <!-- .element: class="fragment" -->
2. Create a handful of 'ideal' personas <!-- .element: class="fragment" -->
3. Find people in your network & community who can make introductions <!-- .element: class="fragment" -->
4. Research where these people hang out in-person and online <!-- .element: class="fragment" -->
5. Get an inexpensive "Super Site" to legitimize referrals & encourage lead generation <!-- .element: class="fragment" -->
6. Set up a simple CRM and Email Service provider to help with follow ups <!-- .element: class="fragment" -->

;;;

# You're looking to build your _foundation_ for growth

;;;

## Your Google Business Profile

![Google Business Profile](https://www.dropbox.com/s/jmhwvybttz9lcrh/CleanShot%202021-01-05%20at%2016.18.33.png?dl=1)

;;;

## Your "Ideal" Customer Profile

<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <h3>Characteristics to Consider</h3>
      <ul>
        <li>Geography</li>
        <li>Demographics</li>
        <li>Psychographics</li>
        <li>Channels</li>
      </ul>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <img src="https://s3.amazonaws.com/swp-media/images/swp-avatar-examples-for-moving.jpg">
    </div>
  </div>
</div> 

;;;

## Find Partners in the Community

(Varies by industry)

Note:
- Think 'channels'

;;;

## Create 'inexpensive' Website

1. DIY website
2. "Off the shelf" Super Site

;;;

## Set up a simple CRM / Email Marketing

1. Hubspot free CRM
2. Mailchimp
3. ActiveCampaign
4. Etc.

;;;

# This lays the FOUNDATION for you start attracting your ideal customers and FOLLOWING UP when they reach out

Note:
- Goal is not to grow super fast, it's to cover costs 

---


# Stage 2 ($100k - $300k)

| What | How |
| --- | --- |
| **Focus On:** | Get more of your "ideal customer" in the door |
| **Biggest Challenge:** | "Lumpiness" |
| **Biggest Opportunity:** | Testing offers, extending customer value, and growing referrals |

Note:
- Lumpy cash flow
- Imperfect service delivery

;;;

# How to do it

1. Focus on consistent service delivery <!-- .element: class="fragment" -->
2. Lead generation landing pages <!-- .element: class="fragment" -->
3. Test "attractive" messaging & "better bait" <!-- .element: class="fragment" -->
4. Measure/test CPL <!-- .element: class="fragment" -->
5. Grow referrals & reviews <!-- .element: class="fragment" -->
6. Communicate! <!-- .element: class="fragment" -->

;;;

## Consistency is key

1. Get back to prospects quickly
2. Show up on time
3. Ask for reviews

;;;

## Lead-generation landing pages

<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <ul>
        <li>Focused on one offer or resource</li>
        <li>Convert a focused visitor into a prospect</li>
      </ul>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <img src="https://s3.amazonaws.com/swp-media/images/example-moving-landing-page.png">
    </div>
  </div>
</div>

;;;

## Attractive Messaging

<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <h3>Characteristics of "Attractive Messaging"</h3>
      <ul>
        <li>Speaks to ideal customer</li>
        <li>Speaks to pains, gains, jobs</li>
        <li>Reduces risk and effort</li>
      </ul>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <blockquote>
        The "cross-town, stress-free" moving kit that (almost) makes moving FUN!
      </blockquote>
    </div>
  </div>
</div> 

;;;

## Measure your Cost per Lead (CPL)

Amount Spent / Leads Generated = CPL

Benchmark: $50/lead

See why being able to charge higher prices helps...a lot? <!-- .element: class="fragment" -->

Note:
- Varies by channel
- Varies by offer

;;;

## Communicate!

1. Email when the lead is generated
2. Follow up emails when a quote is given
3. Follow up emails to get reviews
4. Follow up emails to get referrals

;;;


# What'll happen when you do this...

1. Great service/delivery -> Happy customers
2. Happy customers leave reviews
3. Happy customers refer their friends

;;;

# What'll happen when you do this...

1. Great service/delivery -> Show you what customers _really_ value (if you listen)
2. Allows you to raise prices
3. And informs attractive messaging & better bait
4. Leads to more leads
5. Which leads to more profitable customers

---

# Let's stop and recognize what you've _already_ done

1. Figured out who you serve
2. Started building your FOCUSED network <!-- .element: class="fragment" -->
3. Laid a foundation for a customer attraction system <!-- .element: class="fragment" -->
4. Laid a foundation for a customer conversion system <!-- .element: class="fragment" -->
5. Laid a foundation for a customer retention & referral system <!-- .element: class="fragment" -->

;;;

# Stage 3 ($300k - $1M)

| What | How |
| --- | --- |
| **Focus On:** | Systematic customer attraction |
| **Biggest Challenge:** | Your capacity getting 'maxed out' |
| **Biggest Opportunity:** | Turn your 'tests' into a system and plug the leaks. |

Note:
- Should still be doing everything you _were_ doing

;;;

# How to do it

1. Delegate operational responsibilities <!-- .element: class="fragment" -->
2. Double down on successful sales channels (cut the rest) <!-- .element: class="fragment" -->
3. Redesign your website to communicate value & help customers <!-- .element: class="fragment" -->
4. Start thinking about SEO <!-- .element: class="fragment" -->

;;;

# Stage 3 is starting _to build_ on that foundation by doubling down on the resources that support it.

---

# Stage 4 ($1M+)

| What | How |
| --- | --- |
| **Focus On:** | Scaling your systems |
| **Biggest Challenge:** | Getting high quality help |
| **Biggest Opportunity:** | Building and relying on your team to grow the business for you. |

Note:
- Should still be doing everything you _were_ doing

;;;

# How to do it

1. Develop good hiring systems <!-- .element: class="fragment" -->
2. Outsource work to good partners <!-- .element: class="fragment" -->
2. Use an LMS to deliver basic training <!-- .element: class="fragment" -->
3. Ramp up customer success teams <!-- .element: class="fragment" -->
4. Transition website from 'information' to 'experience' <!-- .element: class="fragment" -->
5. Continue to test new offers, lead-generation bait, and sales funnels <!-- .element: class="fragment" -->
6. Invest in high-converting SEO to drive down the cost of customer acquisition <!-- .element: class="fragment" -->

Note: 
- Bookkeeping
- Finance
- Etc.

;;;

# In Stage 4, you're scaling the system and starting to test new offers, channels, campaigns, and segments.

Note:
- This is where it gets really sophisticated.

---

# What this Looks Like
<iframe width="768" height="432" src="https://miro.com/app/live-embed/o9J_lHFQ38I=/?moveToViewport=-1351,-1790,2747,1386" frameBorder="0" scrolling="no" allowFullScreen></iframe>


---

# Let's recap what we've covered:

1. The 3 systems you NEED to build if you want to have a business that creates FREEDOM for you...
1. The 4 stages of growth from "Start" to "Scale" <!-- .element: class="fragment" -->
2. The biggest challenges to watch out for at each stage <!-- .element: class="fragment" -->
3. The biggest OPPORTUNITIES to LEVERAGE at each stage <!-- .element: class="fragment" -->
4. A step-by-step plan for each stage! <!-- .element: class="fragment" -->

---

# This is alot.

;;;

# So, how do you get started?

## First, Grab these three FREE Bonuses to get started... <!-- .element: class="fragment" -->

1. Download our free training on Google Business Profile <!-- .element: class="fragment" -->
2. Download the one-page growth map as a reference <!-- .element: class="fragment" -->
3. Use our FREE avatar planner to figure out your "ideal" customer <!-- .element: class="fragment" -->

;;;

## Text "Guide" to (517) 300-7999 and they'll be sent to you directly

;;;

## Next, Start Building Your Foundation

1. CRM <!-- .element: class="fragment" -->
1. Super Site <!-- .element: class="fragment" -->
3. Communication Tool(s) <!-- .element: class="fragment" -->

Note:
- If you entered your website at the beginning of this video, a Pro is already reviewing it for you
- If you don't have one - or you didn't enter your website - click the button to book a call with me or one of our Web Pros to discuss _your specific_ growth plan.

;;;

## Finally, Put the System Together

---

# Now, I know this can be overwhelming, even with all this help and resources...

;;;

## So we have a special offer JUST FOR MiMOB Participants to help them get ready for moving season

;;;

## We ALREADY have an inventory of PRE-OPTIMIZED SUPER SITES ready to go for every budget.

(Plus, we offer financing for qualified customers) <!-- .element: class="fragment" -->  

;;;

## These sites include:

- SEO-optimized, mobile friendly website <!-- .element: class="fragment" -->
- Modern, clean design to give you a professional look & feel <!-- .element: class="fragment" -->
- Pro-fessionally optimized for conversions <!-- .element: class="fragment" -->
- Pro-fessionally written copy <!-- .element: class="fragment" -->
- Ready to be integrated with your ESP and CRM <!-- .element: class="fragment" -->
- Access to 5M+ premium stock photos to distinguish you from amateur sites that use free photo providers <!-- .element: class="fragment" -->

;;;

## But because we're committed to your success, we're also providing

- 1 Year of FREE Hosting ($540 value) <!-- .element: class="fragment" -->
- 3 Pro-fessional plugins for SEO & Conversion for FREE ($325 value) <!-- .element: class="fragment" -->
- Access to Pro Academy, where we're building & hosting our Pro-fessional trainings ($199 value) <!-- .element: class="fragment" -->
- 3 half-hour check-ins each quarter with a Pro, to help you build the systems you need to succeed! ($1,200 value) <!-- .element: class="fragment" -->

;;;

# Since it's almost summer, if you sign up before May 15th, we'll guarantee you'll have your off-the-shelf Super Site done by July, or your money back!

;;;

# And for the FIRST 5 customers who order their Super Site, 50% off Super Support for 1 Year ($2,994 value)!

Note:
- No customer is every OBLIGATED to get Super Support, but the most successful ones do...

---

# I know you have lots of choices when it comes to entrusting your website to other web guys...

;;;

# Many of them would charge A MINIMUM of $5,000 to build you a simple site AND STILL take months to deliver it.

(Then charge a $1k+/month retainer for "support") <!-- .element: class="fragment" -->

;;;

# That's a total of $17,000 for a website + 1 year of support from other "web guys"

;;;

# We AREN'T going to charge you anything like that...

;;;

# Instead, we're going to provide you our $5,000 Super Sites for just $2,794...

;;;

# If that was ALL we offered, you'd already save $2,206...

;;;

# But, awesome weather is coming and I'm feeling generous...

<ul>
  <li class="fragment">1 year of FREE hosting: <span style="color:#ff0000;"><s>$540</s></span> <strong>$0</strong></li>
  <li class="fragment">Pro-fessional theme & plugins: <span style="color:#ff0000;"><s>$325</s></span> <strong>$0</strong></li>
  <li class="fragment">Access to Pro Academy: <span style="color:#ff0000;"><s>$199</s></span> <strong>$0</strong></li>
  <li class="fragment">3 Check-ins with a Pro: <span style="color:#ff0000;"><s>$1,200</s></span> <strong>$0</strong></li>
  <li class="fragment">Total BONUSES: <span style="color:#ff0000;"><s>$2,264</s></span> <strong>$0</strong></li>
</ul>

;;;

# That's $7,264 of value that you're getting FOR JUST $2,794!!

Note:
- Saving $4,470, which is basically like getting a website for free in weeks, not months.

;;;

# If you're one of the 1st 5 to sign up and qualify for the Super Support discount, then that's AN ADDITIONAL $2,994 of SAVINGS!

---

## If all we did was get you a Super Site for $2,794 (intead of the $5,000 most agencies would charge), you'd be saving OVER $2,000 on a new website.

;;;

### With the added bonuses, you're saving A MINIMUM of 62% on development, hosting, and support!

Note:
- I can't tell you how to manage your business, but these kinds of deals and decisions you don't see that often....

---

# But there are three catches...

;;;

# 1. This only applies to our pre-optimized, off-the-shelf Super Sites

(And not a fully-custom, designed-from-scratch website)

Note:
- We're going to handle all design, copy, and images, though you can add your logo and any photos you have AS LONG AS YOU GET THEM IN WITHIN 48 HOURS OF OUR ASKING FOR THEM.
- As you saw, you don't need one until stage 3, but if that's you, let's talk.

;;;

# 2. This offer is ONLY GOOD until May 15th!

;;;

## If you're here, you know whether you need help or not, so there's no sense in dragging this out.

---

## Again, we're talking about:

<ul>
  <li class="fragment">Super Site: <span style="color:#ff0000;"><s>$5,000</s></span> <strong>$2,794</strong></li>
  <li class="fragment">1 year of FREE hosting: <span style="color:#ff0000;"><s>$540</s></span> <strong>$0</strong></li>
  <li class="fragment">Pro-fessional theme & plugins: <span style="color:#ff0000;"><s>$325</s></span> <strong>$0</strong></li>
  <li class="fragment">Access to Pro Academy: <span style="color:#ff0000;"><s>$199</s></span> <strong>$0</strong></li>
  <li class="fragment">3 Check-ins with a Pro: <span style="color:#ff0000;"><s>$1,200</s></span> <strong>$0</strong></li>
  <li class="fragment">Total VALUE: <span style="color:#ff0000;"><s>$7,264</s></span> <strong style="text-decoration:underline;">$2,794</strong></li>
</ul>

<p class="fragment">
(Plus an option to save an additional $2,294 on Super Support for the first 25 customers! )
</p>
