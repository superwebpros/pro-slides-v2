# Is this training right for you?
- Most of your customers are in your community or network <!-- .element: class="fragment" -->
- Relationships are important to your business <!-- .element: class="fragment" -->
- You find yourself wondering where to focus your marketing time, attention, and money <!-- .element: class="fragment" -->
- Sales and/or cash flow is uncomfortably lumpy <!-- .element: class="fragment" -->
- You have too much marketing left at the end of the money <!-- .element: class="fragment" -->

Note:
- "Too much marketing at the end of the money"
- Not for SAAS or tech companies
- Not for Enterprise Sales

---

# My goal for this FREE training

To help you _prioritize_ your marketing _plan_ so that you can maximize _profit_ for your business & your customers.

Note:
- We have to be profitable to be sustainable
- We have to be sustainable if we wish to keep serving our customers
- Unfortunately, so many small and local businesses struggle to be profitable, struggle to be sustainable, and end up becoming slaves to their business (vs. the other way around)
- But it's difficult to be profitable without a plan
- And the cornerstone of planning are prioritizes
- It is the case that every person, every business has different priorities. So this map will provide a general map to scale - one that I believe enough to follow - and we will trust you to know where it makes sense for you and your business to 'stop.'

;;;

# The 3 Things We'll Cover Today

1. The "Green ACREs" model for Prioritizing focus 
2. A 4-Step "Start to Scale" Growth Plan <!-- .element: class="fragment" -->
3. Step-by-step processes for Profitable Action at each state <!-- .element: class="fragment" -->

Note: 
- The biggest challenges to watch out for at each stage
- The biggest OPPORTUNITIES to LEVERAGE at each stage

---

# About Me

I'm the owner of SuperWebPros and we've completed nearly 1,000 website projects over the past 5 years. <!-- .element: class="fragment fade-in-then-out" -->

I reviewed dozens of projects and hundreds of Support Tickets to find patterns of success in what we offer and what our customers are doing. <!-- .element: class="fragment" -->

Note:
- Including many moving companies
- Maybe seen us on the 7 Figure Moving Academy 
- Also a local business owner
- Also built a software company
- (Don't hold it against me) Have an MBA & management role for a billion-dollar company.
- Full gamut: small to large, local, and tech
- A lot of experience to help you out
- Fox and ABC (Expert Connexions)
- I'm lucky: I learn as much from my customers as they learn from me
- I get exposed to their creativity...but also their mistakes.

---

# Let's start with a BIG misconception about the Internet...

Note:
The Internet WILL solve all my marketing problems.

;;;

## The Internet...

- WILL NOT print customers on demand
- IS NOT "free"
- IS NOT "easy"

;;;

## But it does allow LEVERAGE

Note:
- So you can accomplish the goals and dreams you have for your business
- And, so many local businesses manage it poorly that it _is_ able to compete

;;;

## Though it is occassionally confusing
<iframe src="https://giphy.com/embed/L2qukNXGjccyuAYd3W" width="480" height="480" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>

---

# So let's De-mystify!
<iframe src="https://giphy.com/embed/n933UOFwlWgcfFu6al" width="480" height="267" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>

---

# How will we do that?

---

# The "Green ACREs" Model for Process Prioritization

Note:
- Green for the "money"
- ACREs for fertile _systems_...

;;;

# The Green ACREs Model:

To build a _profitable_, _sustainable_ business, you need 4 Core Processes:

1. Customer Attraction Process <!-- .element: class="fragment" -->
2. Customer Conversion Process <!-- .element: class="fragment" -->
3. Customer Retention Process <!-- .element: class="fragment" -->
4. Customer Extension Process <!-- .element: class="fragment" -->

;;;

# Customer Attraction Process
A way to get _new_ customers _in_ the door.

;;;

# Customer Conversion Process
A way to turn _prospects_ into _customers_

;;;

# Customer Retention Process
A way to keep attracted customers _coming back_.

;;;

# Customer Extension Process
A way to add _new_ value or generate _more value_ from your customers

Note:
- Product development
- Referrals

;;;

## If your business isn't growing as much as you like, check one of these 4 Processes...

Knowing which one helps you to _prioritize_ where you should spend your time and money. 

Note:
- Focusing on one at a time allows you to compound efforts
- Exercise: Evaluating which system(s) is broken/doesn't exist

---

# Great...so how do we build these systems?

---

# The 4-Stage Growth Map to help YOU _plan_ to Attract, Convert, Retain, and Extend Value systematically for more customers....

;;;

## Different Businesses have Different Needs at Different Stages

- Different cash flow <!-- .element: class="fragment" -->
- Different assets <!-- .element: class="fragment" -->
- Different resources <!-- .element: class="fragment" -->
- Different strategies <!-- .element: class="fragment" -->

;;;

## What's True For You:

_You are not a large company with "unlimited" resources_

(So don't act like one!) <!-- .element: class="fragment" data-fragment-index="3" -->

Note:
- great way to grow broke

---

# What is this 4 Stage Growth Plan?
We're going to begin with the "end" in mind...

- 🚀 Scale <!-- .element: class="fragment" -->
- ⚙️ Systematize <!-- .element: class="fragment" -->
- 💰 Sell <!-- .element: class="fragment" -->
- 🤝 Serve <!-- .element: class="fragment" -->

Note:
- That will help you prioritze, plan, and profit?
- Allows us to see what the foundation(s) should be and then work from there....

---

# Stage 1: Get Clear on Who You **SERVE** ($0 - $100k) 🤝

| What | How |
| --- | --- |
| **Focus On:** | Figuring out 'who to serve' |
| **Biggest Challenge:** | The grind. Finding people who can help you. Earning customer trust. |
| **Biggest Opportunity:** | Your personal relationships. Speed & Flexibility |

;;;

# How to do this

1. Create a handful of 'ideal' personas you'd LOVE to serve <!-- .element: class="fragment" -->
2. Find networks, channels, and relationships who can help you 'access' those personas <!-- .element: class="fragment" -->
3. Think 'service' and 'testing,' not 'optimization' <!-- .element: class="fragment" -->
2. Focus on the fundamentals <!-- .element: class="fragment" -->

Note:
- So many businesses get derailed because they don't want to focus on ideal customer
- Example: Playmakers: Not a "Sports" store that gets consumed or put out of business by the likes of "Dicks" or "Academy." They focus just on runners. 
- Limits their ad spend

;;;

## Some of the "Fundamentals" include:

1. Google Business Profile
2. Google Reviews
3. Simple, professional website
4. Simple CRM & Email Service provider (it's never too early to start communicating)

;;;

# This lays the FOUNDATION for you start ATTRACTing your ideal customers and FOLLOWING UP when they reach out

Note:
- If you don't know who you want to attract, you won't attract anyone consistently. Magnets don't occasionally attract metal. They _always_ attract metal. They _only_ attract metal.
- Goal is not to grow super fast, it's to cover costs 

---


# Stage 2: Learn to "Sell" ($100k - $300k) 💰

| What | How |
| --- | --- |
| **Focus On:** | Get more of your "ideal customer" in the door with your **Unique Sales Proposition** |
| **Biggest Challenge:** | "Lumpiness" |
| **Biggest Opportunity:** | Adapting an "Agile" mindset to marketing, extending value to existing customers, and growing referrals |

Note:
- Lumpy cash flow
- Imperfect service delivery
- Example: Weathervane Roofing

;;;

# How to do it

1. Focus on consistent service delivery <!-- .element: class="fragment" -->
2. Ask for referrals & reviews <!-- .element: class="fragment" -->
3. Test "attractive" messaging & "better bait" with focused landing pages <!-- .element: class="fragment" -->
4. Measure/test CPL <!-- .element: class="fragment" -->
5. Communicate! <!-- .element: class="fragment" -->

;;;

## Attractive Messaging

<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <h3>Characteristics of "Attractive Messaging"</h3>
      <ul>
        <li>Speaks to ideal customer</li>
        <li>Speaks to pains, gains, jobs</li>
        <li>Reduces risk and effort</li>
      </ul>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <blockquote>
        The "cross-town, stress-free" moving kit that (almost) makes moving FUN!
      </blockquote>
    </div>
  </div>
</div> 

;;;

## Lead-generation landing pages

<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <ul>
        <li>Focused on one offer or resource</li>
        <li>Convert a focused visitor into a prospect</li>
      </ul>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <img src="https://s3.amazonaws.com/swp-media/images/example-moving-landing-page.png">
    </div>
  </div>
</div>

;;;

## Measure your Cost per Lead (CPL)

Amount Spent / Leads Generated = CPL

Benchmark: $50/lead

See why being able to charge higher prices helps...a lot? <!-- .element: class="fragment" -->

Note:
- Varies by channel
- Varies by offer

;;;

## Communicate!

1. Email when the lead is generated
2. Follow up emails when a quote is given
3. Follow up emails to get reviews
4. Follow up emails to get referrals

---

## What'll happen when you do this...

1. Great service/delivery -> Happy customers
2. Happy customers leave reviews
3. Happy customers refer their friends

;;;

## In addition...

1. Great service/delivery -> Show you what customers _really_ value (if you listen)
2. Allows you to raise prices
3. And informs attractive messaging & better bait
4. Leads to more leads
5. Which leads to more profitable customers

---

# Let's stop and recognize where we are...

1. We know if we want to be profitable, we need to prioritize correctly
2. The Green ACREs framework describes 4 processes we need to build
3. Which one(s) to prioritize depends on our Growth Stage
4. For each Growth Stage, there's a Plan to follow to help us get from stage to stage...

;;;

## If we've followed along, we've...

1. Figured out who you serve (so we know _who_ to attract)
2. Started building our Customer Extension & Referral System
3. Are starting to build our Customer Attraction System

---

# Stage 3: Solidify Systems ($300k - $1M) ⚙️

| What | How |
| --- | --- |
| **Focus On:** | Systematic customer attraction & conversion |
| **Biggest Challenge:** | Your capacity getting 'maxed out' |
| **Biggest Opportunity:** | Turn your 'tests' into a system and plug the leaks. |

Note:
- Should still be doing everything you _were_ doing

;;;

# How to do it

1. Delegate operational responsibilities <!-- .element: class="fragment" -->
2. Quick to hire, quick to fire landing pages & offers <!-- .element: class="fragment" -->
3. Automate transactional communication <!-- .element: class="fragment" -->
4. Reward retention <!-- .element: class="fragment" -->
2. Outsource work to good partners <!-- .element: class="fragment" -->
5. Redesign your website to communicate value & help customers <!-- .element: class="fragment" -->
6. Start thinking about SEO <!-- .element: class="fragment" -->

Note:
- Looking for best market/media/message fit

;;;

# Stage 3 completes the foundational work of the Green ACREs system.

1. Landing pages attract & convert prospects <!-- .element: class="fragment" -->
2. Email automation converts, nurtures, and extends value for prospects <!-- .element: class="fragment" -->
3. Rewards retain customers <!-- .element: class="fragment" -->
4. Delegation & outsourcing retain customers <!-- .element: class="fragment" -->
5. Website attracts & converts customers <!-- .element: class="fragment" -->

---

# Stage 4: Scale Your Systems ($1M+) 🚀

| What | How |
| --- | --- |
| **Focus On:** | Scaling your systems |
| **Biggest Challenge:** | Getting high quality help |
| **Biggest Opportunity:** | Building and relying on your team to grow the business for you. |

Note:
- Should still be doing everything you _were_ doing

;;;

# How to do it

1. Develop good hiring systems <!-- .element: class="fragment" -->
2. Use an LMS to deliver basic training <!-- .element: class="fragment" -->
3. Install customer success teams <!-- .element: class="fragment" -->
4. Transition website from 'information' to 'experience' <!-- .element: class="fragment" -->
5. Continue to test new offers, lead-generation bait, and sales funnels <!-- .element: class="fragment" -->
6. Invest in high-converting SEO to drive down the cost of customer acquisition <!-- .element: class="fragment" -->

;;;

# In Stage 4, you're scaling the system and starting to test new offers, channels, campaigns, and segments.

Note:
- This is where it gets really sophisticated.
- And it never ends

---

# What this Looks Like
<iframe width="768" height="432" src="https://miro.com/app/live-embed/o9J_lHFQ38I=/?moveToViewport=-1351,-1790,2747,1386" frameBorder="0" scrolling="no" allowFullScreen></iframe>


---

# Let's recap what we've covered:

1. The "Green ACREs" processes you need for profitable growth
1. The 4 stages of growth from "Start" to "Scale" <!-- .element: class="fragment" -->
2. The biggest challenges to watch out for at each stage <!-- .element: class="fragment" -->
3. The biggest OPPORTUNITIES to LEVERAGE at each stage <!-- .element: class="fragment" -->
4. A step-by-step plan for each stage! <!-- .element: class="fragment" -->

---

# So, how do you get started?

---

# Two Options

;;;

## Option 1: DIY with this free resource

<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <h2>The Web Pro Growth Plan</h2>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <img src="https://s3.amazonaws.com/swp-media/images/lm/mockup-of-growthplan.png"/>
    </div>
  </div>
</div>

---

## Option 2: The Fast Track
<iframe src="https://giphy.com/embed/hqlVh0is0Hzes90gGc" width="480" height="480" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/berlin-marathon-kipchoge-hqlVh0is0Hzes90gGc">via GIPHY</a></p>

;;;

## The 4 Things You Need to Succeed

1. Know what to do
2. Know when to do them
3. Accountability & implementation
4. Speed

Note:
- Everything moves quickly. We need to get started quickly.

;;;

## Have a Chat with a Pro

1. We'll get to know you, your business, and your situation.
2. We'll help you identify where and why you're stuck.
3. We'll help you clarify your goals and create a plan of action to achieve them.
4. If it feels like a good fit, and you'd like to know more...we can let you know how we can support you further.

Note:
- Model doesn't work for everyone
- Cut to me
- Book a call
- Block out 45 minutes
- Need to complete the form. If its not filled out, we will cancel
- Can't wait to speak with you.


