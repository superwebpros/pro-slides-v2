You know BF + SBS + CM is the biggest weekend for most retailers - including online retailers!

---

I'm Jesse Flores, the owner of SuperWebPros, and we want to make sure no small e-commerce retailer is left behind this holiday season!

---

For a limited time, we'll be offering a Black Friday Bonanza website review designed to help you get ready for this SUPER important weekend.

---

Here's what we're going to do:

;;;

First, take a look a look at your analytics to see how you stack up against baseline e-commerce stores.

Note:
Hint: If your conversion rates are less than 2%, then you need help.

;;;

Next, we're going to run some diagnostics on your website's speed and performance.

;;;

For example, we know that images are so important, but many small e-commerce companies don't optimize them.

Result: slow, bloated sites <!-- .element: class="fragment" -->

;;;

But other things like installing code snippets like tracking pixels without using Google Tag Manager or intelligent loading can also hurt.

;;;

But we'll look at all that techy stuff and let you know what you need to do get your site running faster and smoother.

---

After that, we'll take a look at your sales process to see how you're leveraging best practices to increase your average cart value.

;;;

Things like your upsells, downsells, cross sells, and email marketing

---

But that's not all...

---

Because you need to get people to your website, we'll also include our Swipe File of 63 killer Facebook Ads that are crushing it, which you can use for inspiration in your promotions

;;;

We'll also include a training on how to approach Facebook ads _profitably_, so that you aren't wasting a bunch of money unnecessarily

;;;


Finally, we'll include a promo calendar you can use to schedule your holiday promos, which you can share with your development team to keep them coordinated and on-point.

---

Now, here's the thing, there is a 'catch' you should know about...

;;;

First, the holidays are approaching quickly and we can only keep this open for a brief window in order to make sure we have enough time to evaluate each website thoroughly AND give you time to make fixes

;;;

Or, if you hire us to make the fixes - which you're not obligated to do - we need enough time to turn them around well in advance of that weekend.

So this is truly a limited time offer.

---

Second, if you sell a ton of products, we're only going to sample 500 pages of your site.

;;;

The reason is that we only a sample of 500 or so pages to identify the biggest systemic issues that need addressing.

It also means we get back to you faster.

---

So, what does it take from you?

;;;

First, order your BFCM Super Site Review using the form on this site for just $499

;;;

Next, we'll need access to your website, your Google Analytics, Google Tag Manager (if you have one), and email marketing software. On the next page, you'll see instructions about how to do that, so please do it now so we don't delay getting started.

;;;

Finally, schedule a time to review your results, so we can help you put together a plan to be ready for BF/SBS/CM ASAP. 

---

Again, we'll do a thorough review of your website's performance, use of e-commerce best practices, and let you know how it's stacking up for the biggest sale weekend of the year. That alone is typically $799.

But, to help you have the most successfull BFCM, we'll:

- Also give you our internal Facebook Ad Swipe File
- Our internal 'Black Friday' checklist
- And a calendar you can use to schedule promotions to get your team aligned for the holiday weekend

Finally, we'll do a half hour results walk-through with you, in order to help you kill it this holiday season. 

---

All you have to do is fill out the form to get started and we'll be happy to help you get a leg up this holiday season!