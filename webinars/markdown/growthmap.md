# About Me

- ND, '03 '05
- Emory MBA '10
- Teacher
- Corporate guy
- Tech startup guy
- Software development guy
- Web Pro

Note:
Origin story

---

# Congratulations on being an entrepreneur!

Note:
Starting stuff is hard. 

---

# But it's challenging, right?

---

### My Goal
# Declutter noise, _focus_ on a _strategy._

;;;

The tactics will work themselves out

---

## Disclaimer
This strategy is componentized and you may recognize some of the components. You may have skipped some and been successful. 

;;;

### That's great!
There are multiple ways to grow. This is just _one framework_ that works.

Note:
- Marsha full-time job
- Allison 200 leads in 48 hours
- Playmakers 7 figure e-commerce website in less than 1 year

---

# The GrowthMap

1. Based on 1st 4 Phases of Entrepreneurship & Management
2. Each stage has different objectives
3. Each stage requires different tools, resources, and activities
4. Stages can be iterative (e.g., new product/service launches)

;;;

# The 4 Phases

1. Serve - Figure out who you serve
2. Sell - Attract more customers
3. Systematize - Systematize customer attraction
4. Scale - Scale the system

---

# Let's Take a Look!

[The GrowthMap](https://miro.com/app/board/o9J_lHFQ38I=/)