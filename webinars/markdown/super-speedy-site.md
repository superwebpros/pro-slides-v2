# The Big Domino

## Who am I?
<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <h4>Jesse Flores, SuperWebPros</h4>
      <ul>
        <li>Corporate guy turned tech entrepreneur</li>
        <li>Started a software development company</li>
        <li>Practioner of "Inbound Marketing"</li>
        <li>Founder of web development company</li>
      </ul>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box" >
      <img alt="Jesse Flores Bitmoji" src="https://www.dropbox.com/s/115lxlk4golij5k/jesse-flores-bitmoji.jpeg?dl=1"/>
    </div>
  </div>
</div>

---

## Let me start with a secret...

;;;

## For the past few years, I've been really _frustrated_ with digital marketing.

;;;

We'd follow "best practices," but see little-to-no results

;;;

## Why?

---

## Stumbled across this little graphic:
![Content over the past 20 years](https://www.dropbox.com/s/zpj2vz38cifrdg1/internet-over-time.png?dl=1)

;;;

I was good at SEO in 2011

;;;

Who wasn't?

;;;

Today, it's _harder_, there's waaaaay more content.

;;;

And waaay more competition.

;;;

<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <p>I also started to notice that search results were changing. It's not just text anymore.</p>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <img src="https://www.dropbox.com/s/4dmqabd70mpskrh/different-types-of-data.png?dl=1">
    </div>
  </div>
</div>

Note:
Videos, maps, smart snippets, products also returned

---

# It was clear that *search was about way more than "just" keywords or blog posts.*

TODO: AD COST ISSUE

INTRODUCE FRAMEWORK



## Strategy

## Social Proof

# The Vehicle (Speedy Site Framework)

## Story - Test

## Strategy

## Social Proof

## Story - Remove

## Strategy

## Social Proof

## Story - Reduce

## Strategy

## Social Proof

## Story - Preload

## Strategy

## Social Proof

## Story - Cache

## Strategy

## Social Proof

# Internal Beliefs (Things You Can Do Right Now)

## Story

## Strategy

## Social Proof

# External Beliefs (Things)

Note:
- Transition to pitch speed test
- Reaffirm that you could learn a lot of this stuff with the right plugins
- Show what to Google

## Story

Note:
- I didn't know all this stuff
- I tried and broke things; our site looked wonky
- Keep playing with dials until we figured it out
- Try on a development server if possible

## Strategy

Note:
- Helpful plugins
- Limitations of different platforms/hosting providers

## Social Proof

Note: 
- Himelhochs screenshot

# Close

