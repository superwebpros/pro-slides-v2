
<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <img src="https://s3.amazonaws.com/swp-media/images/jesse-flores-writing.jpg">
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <h2>Jesse Flores</h2>      
      <H3>Chief Web Pro</h3>
    </div>
  </div>
</div>

Note:
- How to use upside down development to make your next (or first!) website a Super Site!

---

# Joke or Story?

---

# Have you heard of the "Tradeoff Triangle?"
<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <h3>The main idea...</h3>
      <h4>Pick 2</h4>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <img src="https://www.superwebpros.com/wp-content/uploads/2020/12/tradeoff-triangle-1024x1024.png">
    </div>
  </div>
</div>


note:
- quick epiphany bridge story

;;;

# Have you heard of the "Tradeoff Triangle?"
<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <h3>But what if you could have <em>all 3?</em></h3>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <img src="https://www.superwebpros.com/wp-content/uploads/2020/12/tradeoff-triangle-1024x1024.png">
    </div>
  </div>
</div>

---

# My Goal for this Seminar

1. What separates websites from <strong>Super Sites</strong>
2. What makes a website "Super"
3. Where a Super Site fits in your strategy

---

# Who is this for?

- Local retail or service businesses
- Already have a business website
- With moderate amounts of content

---

# A Quick Note About Us...

## This past year...

- Completed > 440 Projects
- Completed > 1413 Support Tickets
- Median response time: 23 hours
- Median completion time: 30 days
- Customer traffic > X million visits/month across different industries

So, we know a thing or two about development... ;-)<!-- .element: class="fragment" -->

note:
- Qualify ourselves

---

# Everyone hates web development

![](https://www.superwebpros.com/wp-content/uploads/2020/12/2-Informational-Graphics_D3_Graphic_1-1024x455.png)

;;;

## We used to do this too... 🤦‍♂️

;;;

## But then we learned, we <em>studied</em>...

![](https://s3.amazonaws.com/swp-media/images/process-brainstorm-edited.png)

;;;

## Turns out that most small businesses have similar needs:

- Generate leads
- Make sales
- Share what they do in a clear, concise, persuasive way

;;;

## Similarity = Modularity. Modularity = Scalability

;;;

## My thought:

If we could:
- Create the right components <!-- .element: class="fragment" -->
- That facilitate business goals <!-- .element: class="fragment" -->
- While streamlining communication <!-- .element: class="fragment" -->

Then we could: <!-- .element: class="fragment" -->
- Get this done faster <!-- .element: class="fragment" -->
- Build a better foundation <!-- .element: class="fragment" -->
- And pass the savings on to customers <!-- .element: class="fragment" -->

;;;

# The main problem with web development...

It's all 100% custom. All the time.  
(Whether that's called for or not.)

<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <iframe src="https://giphy.com/embed/26FfgKNH0YYBUw6Bi" width="480" height="360" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <iframe src="https://giphy.com/embed/SXUDM0rmrq0UsDISL9" width="480" height="270" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>
    </div>
  </div>
</div>

Note:
- Justify failures
- Allay fears
- Throw rocks at enemies
- Confirm suspicions
- encourage dreams

---

# Just a few examples

![](https://www.dropbox.com/s/p9nb53clrr3mn4p/CleanShot%202022-03-15%20at%2010.41.25.png?dl=1)

Note:

Break out by industry...

---

# The 3 Keys to a Super (not mediocre!) Site

1. Key 1: "Who," not "What"
2. Key 2: Reflective Content
3. Key 3: "Upside Down" Development

---

# Pop Quiz: What is your business' most valuable asset?

;;;

## The Customer!

No customer -> no business 

;;;

# Key 1: "Who," not "What"

;;;

## Most people are so eager to focus on the _what_ (they do), they neglect the _who_

But "the who" pays the bills!

Note:
- Chains of false belief
- Experience
- Story
- New bridge

;;;

### We exist to serve people
- But we can't serve everyone

;;;

## So, how do you determine your 'ideal' who?

;;;

## Step 1: Brainstorm everything you can about the kind of customer you _want_ to work

1. Demographics
2. Psychographics
3. etc.

## Step 2: Evaluate each persona against 3 criteria

1. How much you _like_ working with them
2. How much _they_ like working with you AND value what you do
3. Whether they can afford to pay you to serve them (and help you serve others like them)

;;;

### Step 3: Get out of the office and meet those people!

;;;

### The initial growth for all businesses comes from:

1. Network
2. Referral
3. Prospecting

;;;

### You need reps before you can figure out your 'ideal' customer

;;;

### Step 4: Evaluate your initial assumptions

;;;

### Examples of completed customer profile: 

[ Example of a filled out profile]

### But what if you really can serve anyone...

- Focused campaigns.

Note: 
- Do you really have those resources?


---

## Step 2: Reflective Content

;;;

## Because most businesses focus on the 'what' instead of the 'who,' the content all sounds the same

;;;

### So there's no real competitive distinction

;;;

### Which means no competitive margin

;;;

### What to do instead...

;;;

## Step 1: Recognize what it is you _really_ do

;;;

### Businesses exist for 3 reasons:

1. Solve problems
2. Realize goals/dreams/aspirations
3. Complete jobs faster/better/cheaper

;;;

### And typically impact one of the following main dimensions of human life:

1. Health
2. Wealth
3. Relationships

;;;

## Step 2: The value proposition matrix:

[ Example ]

## Step 3: The Problem/Service Matrix

[ link to spreadsheet ]

;;;

## Step 4: Reframe your messaging

1. Gather your information from steps 2 & 3
2. Rewrite your messaging from "me" language to "you" language,
3. Focused on the customer

## Step 4: Test your messaging

1. Test your messaging with in-person networking
2. Run some light 'engagement' campaigns to test messaging on social media (or some other productive channel)

;;;

## This _alone_ will start to set you apart from the competition and increase your 'close rates!'

---

# Key 3: "Upside Down" Development

## Strengthen the Core (Pages)

![](https://s3.amazonaws.com/swp-media/images/core-pages.png)

;;;

## Smart Structure = Better Outcomes

Note:
- Chains of false belief
- Experience
- Story
- New bridge

;;;

## How to Use "Upside Down" Development to Build Your Website Faster

❌ Rookie Mistake: Starting with the homepage <!-- .element: class="fragment" -->

;;;

## Step 1: Identify your ideal customer actions

- Call
- Chat
- Form fill

;;;

## Step 2: List Your "Reflective" Services
- Take your content from Keys 1 & 2
- Each service becomes a page
- The _list_ of services becomes your service page

;;;

## Step 3: Outline Your Story
- Your "Why" becomes your "about" page
- Add photos, gallery, personality

;;;

## Step 4: List Contact Information

;;;

## Step 5: Finally, Build Your Homepage
- Your homepage is _easy_ now because it's just _aggregating_ all the other content in the _most compelling_ way.

;;;

## The More Content You Have, the More Valuable this Process is

;;;

## Example: Lovims

<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2FkT0WymdOMQhajplAA7wQDI%2FLovims%3Fnode-id%3D0%253A1" allowfullscreen></iframe>


;;;

## Who this Works for

- Anyone who knows what service they provide
- Anyone who knows what product(s) they sell
- Anyone who knows what action they want a prospect to take

;;;

## But what if you need other pages?

<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <h3>Great question!</h3>
      <p>That's why we created these for multiple industries</p>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <img src="https://s3.amazonaws.com/swp-media/images/example-sitemaps.png">
    </div>
  </div>
</div>

;;;

## Restate Steps, but with new beliefs

---



;;;

## Restate Steps, but with new beliefs

---

# Used to Be Hard...Not Anymore

---

# Let Me Ask You a Question...

---

# Offer

