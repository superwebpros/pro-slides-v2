# Phase 1: Serve

**Main Focus**  
Figure out 'what you do' and 'who you serve'

**Biggest Challenge**  
Comfort with pressure, ambiguity, imperfection, and lack of optimization.

**Biggest Opportunity**  
Your hustle, personal relationships & the ability to pivot (messaging/offering/branding) very quickly.

;;;

# Phase 1: Serve

**Customer Experience Focus**  
Deliver the goods

**Website**
Basic DIY/brochure site

**Sales & Advertising**  
- Relationships
- Direct outreach
- **Start building email list**

**SEO**  
- Unimportant, unless a content company
- "On-page" basics is good enough

**Automation & Integration**
Basic CRM is all you need.
- Google Sheets
- Free Hubspot CRM
- Pipedrive

---

# Phase 2: Sell

**Main Focus**  
Get more of your 'ideal customer' in the door.

**Biggest Challenge**  
"Lumpy" cash flow & inconsistent service delivery. "Getting it all done"

**Biggest Opportunity**  
Testing ads & direct response campaigns to attract more of your 'ideal' customer. Grow your mailing list.

;;;

# Phase 2: Sell

**Customer Experience Focus**  
- Deliver the goods, consistently
- Develop attractive messaging
- Start to create customer roadmaps for success

**Website**
Landing pages designed to test offers, messaging, conversions

**Sales & Advertising**  
- (Keep doing) Relationships
- (Keep doing) Direct outreach
- (Keep doing) Email marketing
- Referrals
- Direct response advertising

**SEO**  
- Unimportant, unless a content company
- Earn reviews

**Automation & Integration**
- Send leads to Email Service Provider
- Introduce follow up sequences to nudge people through the pipeline
- Lean on CRM to increase real/perceived value of existing customers