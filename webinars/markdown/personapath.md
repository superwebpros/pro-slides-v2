## Here's What We're Going to Cover Today

**Part 1:** Old vs New Model of SEO  
**Part 2:** The PersonaPath SEO Model  
**Part 3:** How to Use this Model to Improve SEO & Lead Generation  

---

## Testimonials

Note:
- We need more of these
- I need events
- I need the Pro Academy

---

## Old Model

;;;

<section id="intro">
    <section>
        <h2>Google is 20 Years Old</h2>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <div class="box">
                        <h4>Google, 1998</h4>
                        <img class="shadowed" src="https://www.dropbox.com/s/zokvxzb00ny2ukz/google-1998-garage.jpg?dl=1" alt="Google office in 1998">
                        <p class="text__small">Source: Google</p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="box">
                        <h4>Google, today</h4>
                        <img class="shadowed" src="https://www.dropbox.com/s/irkhk8lq9wf1hl6/google-offices-2020.jpg?dl=1" alt="Google offices 2020">
                        <p class="text__small">Source: Google</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>

;;;

## The Internet has grown, too

![The Internet over time](https://www.dropbox.com/s/jzus2e78iewcno5/internet-over-time.png?dl=1)

;;;

### From 17M websites in 2000 to 1.8 BILLION in 2020

;;;

# 106x Bigger!!

;;;

## The Old Algorithm

---

## Today

1. Search is about Intent
2. Content is ~~Text~~ ~~Multimedia~~ Data.
3. Data is Ubiquitous

;;;

## Much of the old stuff still matters.

---

## The PersonaPath SEO Model

;;;

## Google's Mission: Organize the world's information

;;;

## An Example
![Example of structured data using actor Chadwick Boseman's information](https://www.dropbox.com/s/ew5z4ojufz17ozv/chadwick_boseman_structured_data.png?dl=1)

;;;

## What is the PersonaPath?

1. Identify a relevant who
2. Identify the steps they take from awareness to purchase
3. Identify the questions they ask AND resources they need at each step
4. Create content that answers those questions and provides those resources
5. Measure, adjust, and repeat

---

## How to Implement this Model

1. Get clear on your revelevant "who"
2. Brainstorm the steps they take from awareness to purchase
3. Identify the questions they ask AND resources they need at each step
4. Create content that answers those questions and provides those resources
5. Measure, adjust, and repeat

;;;

## 1. Get Clear on your Relevant "Who"

Note:
- If you have a page that you think _should_ be working, check it out in your Google Search Console.
- Use on-page SEO best practices

;;;

## 2. Brainstorm the Steps They Take

;;;

## 3. Identify the Questions They Ask & Resources They Need

;;;

## 4. Create Content

;;;

## Content should take people to the _next step_ of their journey.

;;;

## Make sure you have a way to convert!

---

## 5. Measure, Adjust, Repeat
- Google Search Console
- Google Analytics
- SEO Dashboard

;;;

## What We've Covered

1. Old vs New Model
2. The PersonaPath
3. How to Use this Model

---

## 2 Options

1. Dabble
2. Fast track <!-- .element class="fragment" -->

---

## 4 things to succeed

1. Know the right things to do
2. Know the right order to do them in
3. Accountability & coaching to implement
4. Speed of implementation

;;;

## Accelerator call

;;;

## Here's what happens on that call

1. We get to know you, your business, your situation
2. We'll help you identify where and why you're stuck
3. We'll help you decide on your goals and create a plan of action to achieve your goals
4. If it feels like a good fit, we can let you know how we can help you further...

;;;

## Metrics to watch
- Sessions (how many people come)
- Bounce rate (come, then leave)
- Time on site (how long on the site)
- Goal completions (do they do what you need them to do)



That used to work and doesn't work anymore

It used to be easy...
- Competition was low
- Keywords were easy to rank for

Note:
- This is teaching

Problem is that it's not working anymore

SEO got more and more expensive
- People still trying this old model and using AI as if that's going to help.
- It just proliferatees the problem

---

## New Model

- Start with the offer
- Start with the audience
- Tie to journey & tie to user intent

Different types of search...

Search as competition...

Gone are the days when...

We have to up our game

---

IF at this point you're interested in exploring having me and my team work with you in implementing this for you and your business, then click the link below to book a call with us. 

We'll see if the right fit for each other and if we can help you get the results you're looking for. 

We have tons of resources, tools, and strategies that we can implement for you.

---

## Complicated question before
- wasted a lot of time
- opportunity cost

"An ocean of complications"

---

## One Make or Break Factor

- The Path...

---

## Here's what we've covered....

- Old vs New Model
- The Process
- The Path

---

Compbined, this is an absolute game changer

---



---

look back at past 12 months...

---

Accelerator call

---

Here's what happens on that call

1. we get to know you, your business, your situation
2. we'll help you identify where and why you're stuck
3. We'll help you decide on your goals and create a plan of action to achieve your goals
4. If it feels like a good fit, we can let you know how we can help you further...

;;;

If you're interested in exploring having me and my team work with you in implementing this for you and your business, then click the link below to book a call with us. Block off 45 minutes. Form to fill out with a few questions, give us an opportunity to get to know you and your business so we can hit the ground running.

If you don't complete the form, we'll give your spot to someone else who's ready to take action.




