# Is this training right for you?

- You find yourself competing as a 'me too,' 'ho hum' business against other companies who pretty much do the same thing <!-- .element: class="fragment" -->
- You find yourself having to reduce or justify prices to 'win' business <!-- .element: class="fragment" -->
- You feel like you offer a lot of value to your customers, but are struggling to communicate what that value is (let alone capture it) <!-- .element: class="fragment" -->

---

## If this sounds like you...

You're probably in "Commodistan" <!-- .element: class="fragment" -->

---

# My goal for this FREE Training...

To get you _out_ of "Commodistan" and into the "OfferLand" - the Land of bigger value, wider margins, and a better business!

---

# The 3 Things We'll Cover Today

1. The 4 "Value Levers" you MUST manipulate to get out of "Commodistan" <!-- .element: class="fragment" -->
2. How to use those 4 Levers to take your first steps into "OfferLand" <!-- .element: class="fragment" -->
3. How to communicate your offers, so customers aren't confused about which "Land" they're in <!-- .element: class="fragment" -->

---

# About Me

I'm the owner of SuperWebPros and we've completed nearly 1,000 website projects over the past 5 years. <!-- .element: class="fragment fade-in-then-out" -->

Which means we get to learn a lot from our customers... <!-- .element: class="fragment" -->

;;;

<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <h2>But, in my free time...</h2>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <video data-autoplay loop src="https://s3.amazonaws.com/resources.superwebpros.com/video_jesse-shanking-right.MOV"></video>
    </div>
  </div>
</div>


;;;

## Here's the problem(s)...

- I don't have great fundamentals
- When I look them up and practice, I still play poorly on the course
- I don't have enough time to practice as much as I'd like
- Finding a golf coach is time-consuming and (probably) expensive...how can I be sure I'll get a return on my golf game?
- What is a good return anyhow?
- And over what time period should I expect results?

---

# Sound Familiar?

Our customers ask the same kinds of questions....

- I have this problem I can't solve.... <!-- .element: class="fragment" -->
- I don't have the skills to solve it myself... <!-- .element: class="fragment" -->
- Maybe I do have the skills, but I don't have the time to solve it myself... <!-- .element: class="fragment" -->
- Even if I have the time, is the solution worth it? Will it work for me? <!-- .element: class="fragment" -->

;;;

## If we were to categorize the problems:

| Comment | Category |
| --- | --- |
| I have this problem I can't solve | Outcome <!-- .element: class="fragment" --> |
| I don't have the skills to solve it myself | Effort <!-- .element: class="fragment" --> |
| I don't have the time to solve it myself | Time <!-- .element: class="fragment" -->|
| I'm not sure what solution will work for me? | Risk <!-- .element: class="fragment" -->|

;;;

## Is it worth it?

Is a function of:

1. Desired outcome <!-- .element: class="fragment" -->
2. Effort <!-- .element: class="fragment" -->
2. Time <!-- .element: class="fragment" -->
3. Risk <!-- .element: class="fragment" -->

;;;

## Through this lens...

Value (and pricing) is just a way to represent the complex calculation we do in our heads of the factors above. <!-- .element: class="fragment" -->

Note:
- Repeat this a few times
- When we look at a price, we're running through these facets in our heads as we think about the value propsition, our needs, and our resources

---

# OK, but how are these things related?

;;;

# Moment of Terror

Time to get "mathy" for a moment...

<iframe src="https://giphy.com/embed/4JVTF9zR9BicshFAb7" width="480" height="345" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>

;;;

## Which number is bigger?

$\frac{1}{4}$, $\frac{1}{3}$, $\frac{1}{2}$, $\frac{1}{1}$, $\frac{2}{1}$

;;;

## Answer

$\frac{1}{4}$ < $\frac{1}{3}$ < $\frac{1}{2}$ < $\frac{1}{1}$ < $\frac{2}{1}$

Rule: The BIGGER the number at the TOP and the smaller the number at the bottom, the BIGGER the overall number. <!-- .element: class="fragment" -->

---

## Now for our value equation:

`$$\frac{(Outcome)}{(Risk*Time*Effort)}$$`

Where:
- We want to INCREASE customer outcomes (top number), <!-- .element: class="fragment" -->
- DECREASE customer risk (bottom number), <!-- .element: class="fragment" -->
- DECREASE customer timescales, & (bottom number) <!-- .element: class="fragment" -->
- DECREASE customer effort (bottom number) <!-- .element: class="fragment" -->

;;;

## The result

`$$\frac{(Outcome)}{(Risk*Time*Effort)}$$`

- BIGGER top number 
- smaller bottom numbers
- BIGGER OVERALL NUMBER =

**More Value**

---

<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <h2>Back to my example...</h2>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <video data-autoplay src="https://s3.amazonaws.com/resources.superwebpros.com/video_jesse-shanking-right.MOV"></video>
    </div>
  </div>
</div>

;;;

## Imagine I see the following ads:

| Ad | Lever | Value |
| --- | --- | --- |
| "Best golf coach in town. 25 years experience!" | None <!-- .element: class="fragment" --> | 🥱 <!-- .element: class="fragment" --> |
| "Take 20 strokes off your game!" <!-- .element: class="fragment" --> | Outcome <!-- .element: class="fragment" --> | 🤨 <!-- .element: class="fragment" --> |
| "Take 20 strokes off your game, guaranteed!" <!-- .element: class="fragment" --> | Outcome, Risk <!-- .element: class="fragment" --> | 🤔 <!-- .element: class="fragment" --> |
| "Cut 20 strokes in one week, guaranteed!" <!-- .element: class="fragment" --> | Outcome, Risk, Time <!-- .element: class="fragment" --> | 😃 <!-- .element: class="fragment" --> |
| "Cut 20 strokes in one week, guaranteed, without picking up a club!" <!-- .element: class="fragment" --> | Outcome,Risk, Time, Effort <!-- .element: class="fragment" --> | 🤯 <!-- .element: class="fragment" -->|

Note:
- See what a difference this makes?

# So how do we transition?

1. List your services & value levers <!-- .element: class="fragment" -->
2. Identify your baseline <!-- .element: class="fragment" -->
3. Imagine ways to "move the lever" <!-- .element: class="fragment" -->
4. Test <!-- .element: class="fragment" -->

;;;

## Step 1: List your services & value levers

1. Make a list of all the services you provide for your customers <!-- .element: class="fragment" -->
2. For each service, name the outcome that customer gets as a result <!-- .element: class="fragment" -->
3. Then, list all the things required of your customers to get that outcome <!-- .element: class="fragment" -->
4. Next, identify how long it takes for a customer to get that outcome <!-- .element: class="fragment" -->
4. Finally, identify the 'risk factors' where things can go wrong or derail the service <!-- .element: class="fragment" -->

;;;

## You should have a table that looks something like this:

<iframe src="https://docs.google.com/spreadsheets/d/e/2PACX-1vS1AwmUYnxrIUit40C-tb4OIoi5x8EiAmOWkJBTKldhn9cS7dm6Oj-q2q3alL9Yfts_uVPESV0L6_rN/pubhtml?gid=0&amp;single=true&amp;widget=true&amp;headers=false" width="100%" height="400"></iframe>

;;;

## Step 2: Determine your baseline

For each item, give it a baseline score _from your customer's perspective_ and determine your current value number.

<iframe src="https://docs.google.com/spreadsheets/d/e/2PACX-1vS1AwmUYnxrIUit40C-tb4OIoi5x8EiAmOWkJBTKldhn9cS7dm6Oj-q2q3alL9Yfts_uVPESV0L6_rN/pubhtml?gid=1719106466&amp;single=true&amp;widget=true&amp;headers=false" width="100%" height="250"></iframe>

;;;

## Step 3: Imagine ways to move the lever

Then, identify ways you could move the lever...and increase value at the same time!

<iframe src="https://docs.google.com/spreadsheets/d/e/2PACX-1vS1AwmUYnxrIUit40C-tb4OIoi5x8EiAmOWkJBTKldhn9cS7dm6Oj-q2q3alL9Yfts_uVPESV0L6_rN/pubhtml?gid=1083607006&amp;single=true&amp;widget=true&amp;headers=false" width="100%" height=300></iframe>

;;;

### Don't limit yourself!

(After all, this is your business...its your job to solve these problems for customers) <!-- .element: class="fragment" -->

---

# Quick Recap

We're stuck in "Commodistan", so we want to manipulate these 4 levers to get into "OfferLand":

1. Customer Outcome
2. Customer Effort
3. Customer Risk
4. Customer Timescales

;;;

# To manipulate these levers...

1. We identify our services
2. We baseline our current "value score"
3. We brainstorm ways to "move the levers"

---

# Next...

We pick an idea & test it!

- Start with something small
- Create an 'offer' that speaks to more than one lever
- Gauge feedback

;;;

## Examples:

- "Our ready-made Super Sites are industry-specifc websites designed to convert & guaranteed to deliver in 7 days or less" (Outcome + Time)
- "Get access to 3M+ stock photos & videos on your Super Site and have our Design Pros choose ones that work best for your brand!" (Outcome, Effort) <!-- .element: class="fragment" -->
- "Receive access to our free "how to use your website like a Pro" course when you get your Super Site!" (outcome, risk) <!-- .element: class="fragment" -->

Note:
- If you're feeling really ambitious, turn multiple levers at once!

;;;

### The point is to (literally) change the map

We want to start putting together offers and packages that _customers can't compare with other providers_.

;;;

### That's how we get out of "Commodistan" & into "OfferLand"

Note:
- We change the map
- We manipulate the 4 levers (outcome, time, risk, effort)
- We combine the elements

---

# Communicate the "Offer"

;;;

## Communication (almost) takes care of itself!

- We don't have to embellish <!-- .element: class="fragment" -->
- We don't have to "copyhack" <!-- .element: class="fragment" -->
- We don't have to "lie" <!-- .element: class="fragment" -->

;;;

## We just tell people what they get!

Get your fresh, hot pizza delivered in 30 minutes or less, or it's free!

<iframe src="https://giphy.com/embed/Y2QbGVHPalIis" width="480" height="353" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>

;;;

## 3 simple copywriting formulas

1. The "Dominos Pizza" formula
2. The "Before-After-Bridge" Formula
3. The "PAS" Formula

;;;

# Here's what we're going to do...

1. Review the formula
2. Break it down
3. Highlight the offer
4. Construct an example

---

## Formula 1: The Dominos Formula

> Get your fresh, hot pizza delivered in 30 minutes or less, or it's free!
> 
> Dominos Pizza

;;;

### The Formula:

- Get your {{insert appealing outcome here}} <!-- .element: class="fragment" -->
- in {{amazingly fast speed}} <!-- .element: class="fragment" -->
- {{guarantee}} + {{consequence}} <!-- .element: class="fragment" -->

;;;

### The Formula in Action:

Get your fresh, hot pizza delivered in 30 minutes or less, or it's free!

;;;

### The Offer:

- Outcome: Get your fresh, hot pizza
- Time: in 30 minutes or less
- Risk: or it's free!
- Effort: (call now)

;;;

### An Example:

- Outcome: ready-made, conversion-optimized Super Site
- Time: 7 days
- Risk: industry-specific
- Effort: Done for you

;;;

### Becomes:

Get your ready-made, conversion-optimized, industry-specific websites done for you & delivered in 14 days or less - or we host it for free!

---

## Formula 2: Before-After-Bridge

> A 15-minute call could save you 15% or more.
>
> Geico

;;;

### The Formula:

1. Before: Describe the (negative) current state of affairs
2. After: Describe the (desirable) future state of affairs <!-- .element: class="fragment" -->
3. Bridge the gap <!-- .element: class="fragment" -->

;;;

### The Formula in Action:

1. Before (current state): You're paying X for insurance <!-- .element: class="fragment" -->
2. After (future state): You could save 15% or more <!-- .element: class="fragment" -->
3. Bridge (what it takes to get from B to A): A 15-minute call <!-- .element: class="fragment" -->

Note:
- The "before" state is implied in this case

;;;

### The Offer?

- Outcome/Risk: Save 15%
- Effort/Time: 15-minute phone call

;;;

### An example:

- Outcome: Support tickets handled quickly
- Time: 24 hours
- Effort: Sign up
- Risk: Free trial

;;;

## Becomes:

Tired of waiting on your web guy? Have your fix by this time tomorrow with Fast, friendly Super Support. Click to try it free for 15 days.

---

## Formula 3: Problem - Agitate - Solution 

> Reacting to market volatility could hurt your chances of reaching your long-term investing goals. The trouble is that many people don’t understand stock market volatility and how it impacts their portfolio strategy. Use this guide to learn more about volatility and how to survive the market’s ups and downs.
>
> Fisher Investments Guide to Surviving Market Volatility

;;;

### The Formula:

1. Identify a problem <!-- .element: class="fragment" -->
2. Agitate it <!-- .element: class="fragment" -->
3. Propose a solution <!-- .element: class="fragment" -->

;;;

### The Formula in Action:

1. Problem: Reacting to market volatility could hurt your chances of reaching your long-term investing goals.
2. Agitate the Problem: The trouble is that many people don’t understand stock market volatility and how it impacts their portfolio strategy. <!-- .element: class="fragment" -->
3. Proposed Solution: Use this guide to learn more about volatility and how to survive the market’s ups and downs. <!-- .element: class="fragment" -->

;;;

### The Offer?

![](https://s3.amazonaws.com/resources.superwebpros.com/images/example_fisher-surviving-volatilty.png)

;;;

### NOT a Guide!

Note:
- The guide is a product, a vehicle

;;;

### The Offer?

- Outcome: Reach long term investment goals
- Risk: Decrease fear/risk by understanding volatility's impact on strategy <!-- .element: class="fragment" -->
- Effort: Download <!-- .element: class="fragment" -->
- Time: (Instant) <!-- .element: class="fragment" -->

---

# So, what have we done?

;;;

# We're in OfferLand!

- We've constructed valuable offers <!-- .element: class="fragment" -->
- We've figured out how to communicate them <!-- .element: class="fragment" -->
- And we've gotten away from "Commodistan!" <!-- .element: class="fragment" -->

;;;

### How'd we do it?

1. We identified 4 levers of value
2. We looked at our services, set a baseline, and brainstormed ways to tweak the levers <!-- .element: class="fragment" -->
3. We constructed some compelling offers to try and test <!-- .element: class="fragment" -->
4. We've learned some ways to communicate that value effectively  <!-- .element: class="fragment" -->

---

# What do we do next?

1. Review your product & service pages on your website <!-- .element: class="fragment" -->
2. Take each product/service and evaluate it through the lens of the 4 Value Levers <!-- .element: class="fragment" -->
3. Use 2 or more Value Levers to create a unique offer for that service <!-- .element: class="fragment" -->
4. Update that service page with your new offer and a strong call to action <!-- .element: class="fragment" -->
5. Evaluate & tweak <!-- .element: class="fragment" -->

;;;

## ProTip

Use the offer to create a "lead magnet" to start building your email list

;;;

# It's been a long road...

<iframe src="https://giphy.com/embed/d5R4qMAmHwWKUhqbAd" width="480" height="270" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>

---

# But worth it!

<iframe src="https://giphy.com/embed/D8yWu1EZLAvdNA6sql" width="480" height="270" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>

