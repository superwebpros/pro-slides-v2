
[Comment]: <> (Page status: draft)

## Nearly all of our customers ask us about SEO

;;;

Specifically, about whether we can build them a website that will rank in Google.

;;;

Of course, with enough time & budget, the answer is "sure!"

;;;

But, more often than not, their ideas of SEO date back to the time they got their first websites built. 

;;;

Which, for most customers, was mid-2000s.

;;;

When Amazon looked like this:

![Amazon in 2006](https://www.dropbox.com/s/zgrrdxepc11q93i/amazon-2006.png?dl=1)

;;;

And made "only" $8 Billion/year

;;;

(Today it's closer to **$300 Billion**)

---

## So, let's update some ideas.

---

## Idea #1. SEO is just about "keywords"

;;;

### Usually sounds like:
If I put the right words on the page in the right place, my website should rank in Google.

;;;

If it were this easy, wouldn't everyone rank?

;;;

### This idea gets perpetuated by:
- Web building software (Wix, Squarespace, etc.)
- Marketing agencies
- Web designers
- Basically everyone trying to sell you a website

;;;

## Why?

;;;

Because we know that's what people think is the real value of a website. 

Note:
- Some may actually not know better

;;;

What sounds better than "new website = barrage of online customers beating down your door for (effectively) free!"

---

## But it's not true.

;;;

Again, I refer you to:
![Chart of website growth over time](git clone git@bitbucket.org:superwebpros/swp-sanity.git)

;;;

Remember, this shows **websites**, not _webpages_. There are _billions more **pages**_ on the Internet.

---

## So, what gives?

;;;

What these businesses are referring to is making it easy to do _on-page SEO_, which refers to the placement of text in "privileged" places on a website.

;;;

On-page SEO = 1 dimension of SEO.
(There's a guide on the 3 dimensions of SEO in another post)<!-- class="fragment"></!>

Note: 
Need to link to that guide here.

;;;

More specifically, they're referring to putting keywords in the "meta" area of a website such as the "title," "meta description," or image ALT tags.
(There's a guide on this, too!)


Note:
Link to our guide on on-page SEO.

;;;

Putting keywords here is important, both for SEO & click through rates from Google to your website.

;;;

But it's _basic_ and is **necessary** to _move the needle_, but _doesn't move it much_.

---

So, if "words in the right place" is basic, but not needle-moving, what else is needed? 

;;;

Stay tuned for part 2.
