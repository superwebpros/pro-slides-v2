# A Web SITE vs a Web PAGE

A _webSITE_ is a **collection** of one or more _webPAGEs_.

;;;

A website is "complete" when:

Each of the webpages has been built with the **right information**, **presented** in the right way, **functionally**.

Note:
- Like a house
- Living room, kitchen, couple of bedrooms (not sure how many), and mabye some other rooms where I can do some stuff.

;;;

Focus on _individual pages_ until _the entire site_ is done.

;;;

## But what about "global settings or layouts?"
- Should be part of the "brand guidelines"
- Or, in some cases, a _template_ will be used for _collections of information_.
- But the template would go through the same process I'm going to describe.

Note: 
- Probably more important for a template since tens or hundreds of pages could be impacted.
- Separate video

---

# Note
All publication is an exercise in _communication._ In order for us (or any) publisher to be successful, you have to be committed to providing the information we need. 

(Hence, our content briefs) <!-- .element: class="fragment" -->

Note:
- Example: your spouse. "Take out the garbage"
- What she doesn't say is "right now"
- So, I think "I'll get to it later." Next thing I know, "Why havne't you taken the garbage out?"
- This is how communication works. In your case, you've got to think about _what someone hears_ as mediated through a 3rd party.

---

# Step 0
Complete your content brief.
For each page you want created <!-- .element: class="fragment" -->

---

# 3 Step Webpage Process

1. Blueprint
2. Prototype
3. Development

---

# Step 1: Blueprint
**Purpose**: Make sure the right _information_ is being communicated on a given page.

;;;

## What we're looking for

<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <ul>
        <li class="">Structure: Does the flow of the page make sense?</li>
        <li class="fragment">Completeness: Is there any information missing or unaccounted for?</li>
        <li class="fragment">Notice there are no graphics or real copy, only placeholders</li>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <video data-autoplay src="https://www.dropbox.com/s/y1qvbk9x3g1znfv/example-blueprint.mp4?dl=1"></video>
    </div>
  </div>
</div>

---

# Step 2: Prototype
**Purpose**: Make sure the _presentation_ is good.

;;;

## What we're looking for

<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <ul>
        <li class="">Look & Feel: Does the page match branding/aesthetic?</li>
        <li class="fragment">Images & Graphics: Do they work to support communication?</li>
        <li class="fragment">Typography & Spacing: Do they help manage information flow & clarity?</li>
        <li class="fragment">Expect broken links, forms that don't work, and iframes that don't load. We want to get the <em>design</em> right.</li>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <video data-autoplay src="https://www.dropbox.com/s/028yw8pmbp08m09/prototype-example.mp4?dl=1"></video>
    </div>
  </div>
</div>

---

# Step 3: Development
**Purpose**: Make sure the _page works_.

;;;

## What we're looking for

<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <ul>
        <li class="">Does it work?</li>
        <li class="fragment">Link to other pages</li>
        <li class="fragment">Forms, videos, etc.</li>
        <li class="fragment">SEO Information</li>
        <li class="fragment">Approval = Done and we can move on to the next page.</li>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <video data-autoplay src="https://www.dropbox.com/s/028yw8pmbp08m09/prototype-example.mp4?dl=1"></video>
    </div>
  </div>
</div>

---

# Why this process?

## Focus <!-- .element: class="fragment" -->

;;;

## Focus
- We have found that, unless an experienced PM, managing multiple pages at a time is _overwhelming_.
- Simplifcation = easier, faster, cheaper

;;;

# No process is perfect. We need your help:
1. Create content briefs for each page
2. Keep feedback scoped on a per-page basis
3. Give timely feedback

;;;

# A note about "re-work"

Rework is expensive & what causes delays. We're fine with revisions (they're unlimited), but **moving backwards kills project timelines.**

;;;

## Story Time
- Sydney Opera House supposed to be $7Million and completed by Australia Day 1963.
- Ended up being 102 Million and opening 10 years later.
- Complex project, poorly managed. 

;;;

## Page-based Approach Reduces that likelihood

But not infallible.

;;;

Once a step is approved, it's best to move on. 

We can always make updates after a site is live. <!-- .element: class="fragment" -->

