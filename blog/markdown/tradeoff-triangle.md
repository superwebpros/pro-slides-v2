## Opportunity Costs & Tradeoffs

In economics, "Opportunity Cost" refers to the idea that making a decision for _one_ thing, often excludes (at least) _one other_ thing.

;;;

**Tradeoffs** are the "brass tacks" of those costs. What, _specifically_ you get (or give up) when making one decision over another.

;;;

## Example
Buying a new car

<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <img src="https://www.dropbox.com/s/97pte7glvfsv2fq/mercedes-example.jpg?dl=1" alt="Example of Mercedes C300">
        <ul>
          <li>Can get gas anywhere</li>
          <li>Luxury interior</li>
          <li>It's a Mercedes!</li>
        </li>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <img src="https://www.dropbox.com/s/vs65e096d7jr151/lhd-model-3-social.png?dl=1" alt="Example of Tesla 3">
      <ul>
        <li>0-60 in 3.1 seconds</li>
        <li>350 mile range</li>
        <li>Cool</li>
      </li>
    </div>
  </div>
</div>

---

## The Tradeoff Triangle

In software (or web) development, the _main_ tradeoffs are usually between:
1. Scope/Quality (_what_ gets done)
2. Cost (_how much_ it costs)
3. Time/speed (_how long_ it'll take)

;;;

Those tradeoffs are especially strong when _the work being done has customization involved._

---

<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <h3>When it comes to development, it <em>is very, very, (very) rare</em> that you can get ALL 3.</h3>
      <p>Be prepared to <strong>pick 2</strong>.
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <img src="https://www.dropbox.com/s/vx50j0arq1a7zwu/tradeoff-triangle.png?dl=1" alt="example of tradeoff triangle">
    </div>
  </div>
</div>

---

## Examples of Tradeoffs

---

## Example 1
"I want a website with the custom designed graphics, enterprise infrastructure, will rank in Google on day one, etc."

;;;

## The premium is clearly on "Scope." 
But, what's _the next most important thing_?  

;;;

## Priority = Scope
What next?

<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <ul>
        <li class="fragment fade-in-then-semi-out"> Option 1: Saving money? Then, expect it to take a long time, with a lot of back-and-forth.</li>
        <li class="fragment fade-in-then-semi-out"> Option 2: Getting it done quickly? Then expect to pay for it.</li>
      </ul>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <img src="https://www.dropbox.com/s/a4ygzjcscjydpt7/tradeoff-triangle-scope-fixed.png?dl=1" alt="person looking at tradeoffs">
    </div>
  </div>
</div>

---

## Example 2

"I need a website tomorrow"

;;;

The premium is clearly on speed/timing. But, what's _the next most important thing_?

;;;

## Priority = Speed
What next?

<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <ul>
        <li class="fragment fade-in-then-semi-out"> Option 1: Saving money: Expect a simple template with minor changes.</li>
        <li class="fragment fade-in-then-semi-out"> Option 2: Customization: Expect to pay dearly for rush fees, etc.</li>
      </ul>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <img src="https://www.dropbox.com/s/k6bw3lir6l7ovfe/tradeoff-triangle-scope-fixed-timing.png?dl=1" alt="person looking at tradeoffs">
    </div>
  </div>
</div>

---

## Example 3

"I can't really afford much"

;;;

(You should probably just do it yourself, using any of the great off-the-shelf tools out there)

;;;

The premium is clearly on cost control. But, what's _the next most important thing_?

;;;

## Priority = Cost
What next?

<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <ul>
        <li class="fragment fade-in-then-semi-out"> Option 1: Getting it done quickly: Expect an 'off-the-shelf' template with very few customizations.</li>
        <li class="fragment fade-in-then-semi-out"> Option 2: Customization: You're lucky if this project ever gets done. It won't be a priority to anyone.</li>
      </ul>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <img src="https://www.dropbox.com/s/lxg5ih7e1o6g3c1/tradeoff-triangle-scope-fixed-cost.png?dl=1" alt="person looking at tradeoffs">
    </div>
  </div>
</div>

---

#### There are 3 sides to the "Tradeoff Triangle." 
## Choose 2.

---

## Where Does SuperWebPros Fit In?

We try to satisfy each of these dimensions in these ways:

- For the "budget conscious," we have [Fixed Scope Websites](https://www.superwebpros.com/web-design-services) and [Unlimited Web Design Services](https://www.superwebpros.com/unlimited-web-design-services) that you can budget around.
- For the "time conscious," we have [Over 100 "Super Sites"](https://explore.superwebpros.com) ready to be deployed and can be easily modified by our team to suit your needs...quickly. And, our **Super Support** team is geared around fixing cusotmer website issues within 1-2 business days (depending on your package & request).
- For those that need a "custom solution," we can help with that, too. However you'd have to [Book a Call](https://www.superwebpros.com/call-a-web-pro), so we can discuss it.

;;;

Ultimately, our [Unlimited Web Design Service](https://www.superwebpros.com/unlimited-web-design-service) is designed to be the "Coup de Grace" to the "Tradeoff Triangle"

---

## Unlimited Web Design

- Flat monthly rates
- Unlimited 'minor fixes' and updates
- Unlmiited 'new content'
- Unlimited 'functionality' enhancements

;;;

## Unlimited Web Design

## The tradeoffs?

- Time - Our service level is 3-5 days turnaround <!-- .element: class="fragment fade-in-then-semi-out" -->
- Scope creep - Only one page/project active at a time <!-- .element: class="fragment fade-in-then-semi-out" -->
- Over 100 "Super Sites" to start from! <!-- .element: class="fragment fade-in-then-semi-out" -->

---

No perfect solution, but doing our best to get you there!
