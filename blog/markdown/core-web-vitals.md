## What Are Core Web Vitals?

A set of specific factors that Google considers important in a webpage's overall user experience

;;;

Used to measure how users perceive the experience of interacting with a web page beyond the content.

---

## 3 Main Core Web Vitals

1. **Largest Contentful Paint (LCP)** - Measures time it takes for the largest content element to load
2. **First Input Delay (FID)** - Measures time it takes for a page to become interactive
3. **Cumulative Layout Shift (CLS)** - Measures visual stability of a page

