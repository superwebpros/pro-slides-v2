## Two ad metrics to watch
1. Link clicks vs Page views <!-- .element: class="fragment fade-in-then-semi-out" -->
2. Conversion rates <!-- .element: class="fragment fade-in-then-semi-out" -->

---

## Most people watch conversion rates
Tells you how many people completed a specific goal (downloads, calls, purchases, etc)

;;;

## Low conversion rates can often mean:
- Poor landing page design
- Poor/unclear offer
- Poor user experience
- Wrong audience/offer mix
- And more...

---

## But the ratio between link clicks & page views is _much clearer_

;;;

## And much less discussed.

---

## An example (from one of our own campaigns)

![One of our campaigns](https://www.dropbox.com/s/k4936u7ng66fuft/screenshot-fb-campaign.png?dl=1)

;;;

## Things that are good

<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <ul>
        <li>Good reach</li>
        <li>Good number of clicks</li>
      </ul>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <img src="https://www.dropbox.com/s/sffroab6aar953p/Instagram%20Post%20-%2012.png?dl=1" alt="Screenshot of good ad distribution & click through rates">
    </div>
  </div>
</div>

;;;

## Not so good

<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <p>So many clicks. Way less page views.</p>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <img src="https://www.dropbox.com/s/394bn4a8jre1e24/Instagram%20Post%20-%2013.png?dl=1" alt="Screenshot of good ad distribution & click through rates">
    </div>
  </div>
</div>

;;;

## Really not good...

<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <p>Cost per Landing Page View is <strong>nearly 2x</strong> higher</p>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <img src="https://www.dropbox.com/s/emql4m7f72gyih3/Instagram%20Post%20-%2014.png?dl=1" alt="good cost per click, but cost per page view is way higher">
    </div>
  </div>
</div>

---

## Why this descrepancy?

---

### One answer:
## Page Speed.

;;;

When you see a lot of clicks, but not that many views, it means **people were interested in the offer, but not willing to _wait_ to learn more.**

;;;

### ProTip
> There will always be a gap between link clicks & page views
But, if that rate starts to jump up beyond 10-15%, **test that page's speed.**

Note:
- People click the link on accident, for instance

---

## Places to test page speed:

- **Google's PageSpeed Tool** - [https://developers.google.com/speed/pagespeed/insights/](https://developers.google.com/speed/pagespeed/insights/)
- **Pingdom's Website Speed Test** - [https://tools.pingdom.com/](https://tools.pingdom.com/)
- **GTMetrix** - [https://gtmetrix.com/](https://gtmetrix.com/)

---

Run the test on your landing page (and any other pages you're driving paid traffic to) to get specific insights on how to fix them to make them faster

---

Then, forward them to your web developer, so they can start working on improvements.

;;;

### ProTip
> Be aware, fixing page speed issues can get pretty complicated - especially if there's outdated software. There may be fees associated with updating the site. In some cases, you may need to rebuild the website or invest in a landing page builder for advertising.

---

## Note that you may want to pause ads while improvements are underway.

---

## Once your page speed is back up, resume ads & watch your ROI jump up!

---

<div class="row middle-lg">
  <div class="col-lg-12">
    <div class="box">
      <h2 class="text__center">Want a complete analysis of your website's page speed?</h2>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <p class="text_small align-left">Our <strong>comprehensive</strong> page analysis tools will run every page on your website through Google's PageSpeed test, then provide a <strong>detailed plan</strong> for improving your website's page speed for just $199.<p>
      <p class="text_small align-left">Learn more at <a class="text_small" href="https://www.superwebpros.com/website-speed-audit/">https://www.superwebpros.com/website-speed-audit/</a>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <img src="https://www.superwebpros.com/wp-content/uploads/2020/10/Graphic-PageSpeed-Report.png" alt="Example of SuperWebPros Pagespeed assessment">
    </div>
  </div>
</div>









