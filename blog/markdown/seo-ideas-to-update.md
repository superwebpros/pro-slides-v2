## What does this mean for your business?

;;;

It's probably time to "unlearn" some stuff about the web that you've heard in the past.

---

## 1. On-page SEO is Table Stakes
- "Keywords in the right places" is foundational 
- But not the only thing <!-- .element: class="fragment fade-in-then-semi-out" -->

(By a long shot) <!-- .element: class="fragment fade-in-then-semi-out" -->

Note: On-page SEO defined as page-level optimizations. Traditionally, 'meta tags,' etc. But so much more now (as we'll see going through this)

---

## 2. Content matters
- Product/service information <!-- .element: class="fragment fade-in-then-semi-out" -->
- Blogging <!-- .element: class="fragment fade-in-then-semi-out" -->
- Video content <!-- .element: class="fragment fade-in-then-semi-out" -->

Note:
Deeper information = better for search AND customers

;;;

For an example of 10+ types of blog posts you can use to improve your business' SEO, [visit our blog post online](https://www.superwebpros.com/blog/10ish-types-of-posts-to-help-improve-small-business-seo/)

<a class="btn" href="https://www.superwebpros.com/blog/10ish-types-of-posts-to-help-improve-small-business-seo/">Read the Post</a>

---

## 3. So does conversion
- Everything online is measured <!-- .element: class="fragment fade-in-then-semi-out" -->
- "Engagement" improves ranking <!-- .element: class="fragment fade-in-then-semi-out" -->

;;;

## Metrics to watch
- Sessions (how many people come)
- Bounce rate (come, then leave)
- Time on site (how long on the site)
- Goal completions (do they do what you need them to do)

;;;

To get started on understanding metrics, check our [web analytics primer for small business](https://www.superwebpros.com/blog/web-analytics-101-small-business-guide/)

<a href="https://www.superwebpros.com/blog/web-analytics-101-small-business-guide/" class="btn">Read the Post</a>

---

## 4. Really, it's data
Content, videos, etc. are _data_ which is consumed, indexed, and/or transformed by _machines_.

Note:
Google's goal: Organize the world's information

;;;

**Structured Data** makes it easier for machines to process information.

;;;

## An Example
![Example of structured data using actor Chadwick Boseman's information](https://www.dropbox.com/s/ew5z4ojufz17ozv/chadwick_boseman_structured_data.png?dl=1)


---

## 5. And, user experience
- Google's goal is to return _the best_ results to answer a customer's **query**.
- "Best result" is more than "just information" -> it's the best presentation of that information for a given customer/audience

Note:
- For local businesses, competition based on content is easier than "national" competition
- Even if it's harder for them to rank for "short-tailed" keywords.

;;;

## (Some) Factors that contribute to user experience
1. Page Speed
2. First Contentful Paint (FCP)
3. Largest Contentful Paint (LCP)
4. First Input Delay (FID)
5. Cumulative Layout Shift (CLS)
8. Accessible for all people

Note:
- Hero Carousels = not a great user experience (usually)
- Visually impaired visitors

;;;

Find out how your website performs with our [Site Speed Audit](https://www.superwebpros.com/website-speed-audit/)

<a class="btn" href="https://www.superwebpros.com/website-speed-audit/">Order an Audit</a>

---

## 6. "External" signals
"Factors" outside of your website have a significant impact on your ranking:
- Social shares <!-- .element: class="fragment fade-in-then-semi-out" -->
- Backlinks <!-- .element: class="fragment fade-in-then-semi-out" -->

Note:
- How we know if our content is any good?

;;;

Nobody knows what all of them are.

---

## 7. Real SEO isn't cheap
## (Or Easy) <!-- .element: class="fragment fade-in-then-semi-out" -->

;;;

The companies that rank best provide _great information_ and _promote it_ **consistently.**

Note:
A lot of SEO comes down to being a great publisher & a great promoter

;;;

## The web is super competitive
![The change in number of websites from 2000 to 2018](https://www.dropbox.com/s/jzus2e78iewcno5/internet-over-time.png?dl=1)

(by the way, this is web _sites_, not web **pages**)

;;;

To compete requires **a commitment** to:
1. Technical optimizations (security, speed, content architecture)
2. On-page optimzations (great content, relevant keywords, structured data)
3. Off-page optimizations (promoting content, getting good backlinks, Google business profile, local press)

---

## 7. "Other" Factors
There are tons of other factors that influence search:
- How long the site has been around
- How much competition there is for a given term
- What kinds of competition there are
- Stuff nobody knows about (including Google)

Note:
The algorithm is all AI-driven.

---

## It _is_ possible to compete in SEO

---

## But it requires _investment_

Note:
Internet so much more competitive than it was when you built your first website (most likely)

---

## What to do if you're just starting out?
- Advertise to a small set of well-optimized landing pages <!-- .element: class="fragment fade-in-then-semi-out" -->
- Start laying a good foundation <!-- .element: class="fragment fade-in-then-semi-out" -->
- Start building your email list <!-- .element: class="fragment fade-in-then-semi-out" -->

Note:
- Good foundation = well-heeled site. Partner(s) to help you keep it up-to-date and optimized
- Email list = "owned channel"

;;;

## Focus on SEO _after_ you've got at least one other sales channel up and profitable

Note:
- For most industries. Affiliate marketers, media, and companies with a **significant** content advantage probably need to focus on SEO, early.

---

## Need help with _your_ website?
<p>Try <strong>Unlimited Web Design</strong> by SuperWebPros</p>
<p class="fragment fade-in-then-semi-out">It's designed to scale with your business</p>
<p class="fragment fade-in-then-semi-out">(and your budget)</p>

;;;

## Learn more
[https://www.superwebpros.com/unlimited-web-design-service/](https://www.superwebpros.com/unlimited-web-design-service/)






