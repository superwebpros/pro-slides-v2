# Super Site Basic

---

# Design Kit Approach

We use Design Kits to create website frameworks. These are pre-designed components designed to:

- Look great
- Be easy to use
- Be easy to maintain
- Be easy to scale

;;;

## Example Design Kit

[https://miro.com/app/board/uXjVN17Q0bA=/](https://miro.com/app/board/uXjVN17Q0bA=/)

;;;

# Process
Our process is simple, straightforward, and designed to scale. Here's how it works:

1. Foundation - We put together the branding guide for your site, which includes your logo, colors, and fonts
1. Layout - We lay out each page of your site in a way that makes sense for your business and your content.
2. Content - We add content to the page, including text, images, and videos, as applicable.
3. Draft - We send you a draft of the page for your review.
4. Revise - You send us any changes you'd like to make to the page using our on-page commenting tool.
5. Publish - We publish the page to your site.

Note:
- We'll work with you to determine the best layout for your site.
- We annotate the page with comments to make it easy for you to provide feedback.

;;;

## Process Layout
<div class="mermaid">
    <pre>
        %%{init: {'theme': 'dark', 'themeVariables': { 'darkMode': true }}}%%
        gantt
            dateFormat YYYY-MM-DD
            axisFormat %j
            title Supersite Project Plan
            section Onboarding
            Customer setup in ProHQ :t1, 0, 1d
            Get access to credentials :crit, t2, after t1, 2d
            Onboarding complete : milestone, m1
            section Design
            Create Figma Style Guide : d2, after t2, 3d
            Start Page Layouts : d4, after d2, 5d
            Page Layouts approval : milestone, m5
            section Development
            Setup Wordpress development environment : dev1, after t2, 3d
            Develop individual pages : dev4, after dev1, 5d
            Pages built : milestone, m7
            section Deployment
            Integration testing : dep1, after dev4, 3d
            Publish website :crit, milestone, m8
            Project complete :crit, milestone, m9
    </pre>
</div>

---

## How do we get there?

1. Pre-optimized Wordpress Super Site Design Kit with re-usable modules installed on (y)our server ~~($999)~~
2. 7 Core Layouts Customized for Use & Scaling ~~($4,999)~~
3. Copywriting for the 5 Core Pages ~~($999)~~

;;;

## But We want to go further...

1. On-page SEO ~~($999)~~
2. Super Support Membership ~~($4,788)~~
3. Google Trifecta Configuration ~~($498)~~   
4. "How To" Video Library ~~($99)~~

;;;

## Super Site Basic Plan
Pro-fessional Wordpress Super Site Design Kit with re-usable modules installed on (y)our server, backed by Super Support

| Item | Value |
| --- | --- |
| **Website** | |
| Pre-optimized Wordpress Super Site Design Kit with re-usable modules installed on (y)our server | $999 |
| 7 Core Layouts Customized for Use & Scaling | $4,999 |
| Copywriting for the 5 Core Pages<sup>1</sup> | $1,000 |
| 1 Year of Website Hosting<sup>2</sup> | $468 |
| 1 Year of Email Marketing Software Hosting<sup>3</sup> | $468 |
| 1 Year of SSL<sup>4</sup> | $99 |

;;;

| Item | Value |
| --- | --- |
| **Marketing & Support** | |
| 1 Year of Super Support Membership<sup>6</sup> | $4,788 |
| Quarterly Strategy Meeting with a Pro | $1,200 |
| On-page SEO Optimization | $999 |
| Google Trifecta Configuration | $499 |
| 1 Year License for premium themes & plugins | $499 |
| Local SEO Listings with NAP Management for 1 Year <sup>5</sup> | $488 | 
| Pro-fessional WP Theme Bundle<sup>7</sup> | $169 |
| Premium Easy-to-Use Page Builder<sup>8</sup> | $59 |
| Google Business Profile Training | $99 |
| "How To" Video Library<sup>9</sup> | $99 |

;;;

| Item | Value |
| --- | --- |
| **Total Value** | **$16,932** |
| **Your Price** | **$7,499** |
