# Agenda:

1. Current State
2. Competition Review
3. The Opportunity
4. Recommendations

---

# Current State

;;;

## Current State

1. On-page SEO needs moderate improvement
2. Pagespeed needs improvement
3. Core Web Vitals, some URLs don’t provide a good page experience


Note:

- We ran the site on your GSC data
- We also ran the site through some of our own tools to get full metrics

;;;

## 1. On-page SEO needs moderate improvement

1. Many pages don't have SEO Meta Descriptions
2. Some pages need to add Meta Keywords
3. Several pages don't have Title "H1" tags
4. Some pages have Title "H1" duplicated

;;;

## 2. Pagespeed needs improvement

1. The site's Speed Index is 15.6s (mobile) and 2.8s (desktop)
2. Largely driven by images that are too large

Note:
Image size averages over 2.4MB

;;;

## 3. Core Web Vitals

#### What is LCP? Why does it matter?

👉 Largest Contentful Paint (LCP): It represents how quickly the main content of a web page loads. <!-- .element: class="fragment" -->

[Source: Google Search Console](https://search.google.com/search-console/core-web-vitals?resource_id=https%3A%2F%2Fshop.fitnesssports.com%2F)

<img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/lcp.png" width="400"/> <!-- .element: class="fragment" -->


;;;

### For Mobile

👉 19 URLs - LCP issue: longer than 4s 

[Source: Google Search Console](https://search.google.com/search-console/core-web-vitals?resource_id=https%3A%2F%2Fshop.fitnesssports.com%2F)

<img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/cwv-m-fs.png" height="500" width="650"/> <!-- .element: class="fragment" -->


---

# Competition Review

;;;

## Competitors Sites Reviewed

We reviewed several competitors to see how they are performing in search results. Here are a few salient ones:

1. [Gazelle Sports](https://www.gazellesports.com/)

2. [Dickpond Athletics](https://www.dickpondathletics.com/)

3. [First to the Finish](https://www.firsttothefinish.com/)

;;;

### General Observations

👉 Status compared to your strongest competitors.

<img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/fs-competitors.png" height="350"/>


---

# The Opportunity

;;;

## Ideal Customer Profiles


| Anne Goldberg | Barry West |
| --- | --- |
| <img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/fs-avatar.png" height="350"/> | <img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/fs-avatar2.webp" height="350"/> |
| Corporate professional | Team Manager |
| [Profile](https://app.superwebpros.com/#CustomerAvatar/view/66294dfe20411eded) | [Profile](https://app.superwebpros.com/#CustomerAvatar/view/663b9a5d41ad2a372) | 



;;;

## Goals

1. Show up in search results for high-quality keywords.

2. Get more visitors on your email list.

3. Position Fitness Sports for success in target products.

---

# Recommendations

Note:

- Foundational & Strategic Recommendations

;;;

## Foundational SEO Recommendations

Based on our review, we recommend the following foundational changes to the site:

1. Add Meta Descriptions to all pages 🚩🚩
2. Add keywords to page titles 🚩🚩🚩
3. Shrink large images to improve page speed ([Smush](https://wordpress.org/plugins/wp-smushit/)) 🚩🚩🚩
4. Remove duplicate H1 tags 🚩🚩🚩🚩
5. Add H1 tags to the pages that are missing them. 🚩🚩🚩🚩

;;;

## Strategic Recommendations

Based on our review, we recommend the following strategic updates to the site:

1. Update the site's information architecture <!-- .element: class="fragment" -->

2. Implement search, navigability, and e-Commerce best practices on the site <!-- .element: class="fragment" -->

3. Use Keyword Clusters to improve content <!-- .element: class="fragment" -->

4. Create connected journey throughout the site <!-- .element: class="fragment" -->

;;;

## 1. Set up Foundational Information Architecture

Information architecture is the structural design of important information.
It involves organizing, structuring, and labeling content so users and search engines can find it easily.


;;;

### 1.1 Revamp "Hero Section" to make it easier for users to find what they're looking for.

- There's a lot of things going on that confuse a user's path. <!-- .element: class="fragment" -->

- Too many CTAs in the same section overwhelm the user <!-- .element: class="fragment" -->

- Without a search bar to help the user find what they are looking for. <!-- .element: class="fragment" -->

;;;

<img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/fs-hero.png" height="350" width="750"/> 

;;;

Here's an example of Gazelle Sports:

- 1 clear CTA. <!-- .element: class="fragment" -->

- A chabot to help the users find what they are looking for. <!-- .element: class="fragment" -->

- A search bar for the same purpose. <!-- .element: class="fragment" -->

;;;

Source: [Gazelle Sports](https://www.gazellesports.com)
<img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/gazelle-hero.png" height="350" width="750"/> 


---


## 2. Implement Search, Navigability, and List-building Best Practices On The Site

1. Add an Exit Pop-Up to capture and save the emails of users who are leaving. <!-- .element: class="fragment" -->

2. Add offer discount coupons in exchange for their emails. BONUS: Make them in-store specific with geo targeting. <!-- .element: class="fragment" -->

3. Add a Search Bar, to improve the user's search experience. <!-- .element: class="fragment" -->

4. Add a ChatBot, to guide users to what they are looking for. <!-- .element: class="fragment" -->

;;;

#### You need to grow your contact list (buyers and non-buyers) to capitalize that organic traffic and be able to communicate with them more easily in the future.

;;;

### 2.1 Add an Exit Pop-Up to capture and save the emails of users who are leaving.

<img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/exitpopup.png" 
height="400" width="750"/> 

;;;

#### With this implementation we could:


1. Get more visitors on your email list  <!-- .element: class="fragment" -->

2. Get more repeat visitors <!-- .element: class="fragment" -->

3. Increase sales conversions <!-- .element: class="fragment" -->


;;;

### 2.2 Offer discount coupons in exchange for their emails. 

<img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/disc-cupon.png" height="400" width="750"/> 

;;;


#### This helps with:

1. Attracting New Customers <!-- .element: class="fragment" -->

2. Encouraging Repeat Purchases <!-- .element: class="fragment" -->

3. Increasing Average Order Value <!-- .element: class="fragment" -->


---


## 3. Update Content Using Relevant Keywords

### Organize content around Keyword Clusters


Keyword clusters are groups of keywords that are closely related to each other. They are used to help search engines understand the content of a page.

;;;

#### Example - Google Search Console

[![GSC Keywords](https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/gsc-fs.png)](https://search.google.com/search-console/performance/search-analytics?resource_id=https%3A%2F%2Fshop.fitnesssports.com%2F)

;;;

### 3.1 Understanding the content and KW that your competition has.

;;;

#### This shows:

1. Total number of keywords each one has.

2. Total number of unique keywords each one has.

<img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/fs-kwcompetitors.png" height="400" width="750"/> 

;;;

### 3.2 We extract their "Unique KWs" and put them into lists to show you how each one is different and give you a clear path of content.


;;;

Unique KW lists for each main competitor: 

1. [UNIQUE_GazelleSport](https://app.superwebpros.com/#KeywordCluster/view/66328e4f6cb9d5d4f)

2. [UNIQUE_Dickpondathletics](https://app.superwebpros.com/#KeywordCluster/view/66328e4f6e90838fe)

3. [UNIQUE_FirstToTheFinish](https://app.superwebpros.com/#KeywordCluster/view/66328e4f6df43b82b)

4. [UNIQUE_TCrunningco](https://app.superwebpros.com/#KeywordCluster/view/66328e4f6d4b8b9be)


Other KW Clusters:

- [Full List of KW Cluster](https://app.superwebpros.com/#KeywordCluster)

;;;

### 3.3 We suggest updates on 'Learning Center', 'Fitting Process' and 'Events' pages to make them keyword-focused.


The current 'Learning Center', 'Fitting Process' and 'Events' are not keyword-driven and with opportunities for improvements in the description. This makes it difficult for search engines to understand what the page is about.


Note:

- The tone reads like an interview


---

## 4. Link Relevant Content Together

;;;

### 4.1 Enhance the 'Blog Articles Section' to increase its conversion

<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <p>Current article page</p>
      <img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/fs-blog.png"/>
    </div>
    <div class="col-md-6">
      <p>Improvement proposal</p>
      <img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/fs-blog3.png"/>  
    </div>
  </div>
</div>

<p>Goals:</p>
        <ul>
          <li>Increase conversions</li>
          <li>Attracting New Customers</li>
          <li>Get more visitors on your email list</li>
        </ul>



---

# Recap


| Category                                   | Recommendations                                                                                      |
| ------------------------------------------ | ---------------------------------------------------------------------------------------------------- |
| **Foundational Changes**                   | Add Meta Descriptions to all pages                                                                   |
|                                            | Add keywords to page titles                                                                          |
|                                            | Shrink large images to improve page speed (Using [Smush](https://wordpress.org/plugins/wp-smushit/)) |
|                                            | Remove duplicate H1 tags                                                                             |
|                                            | Add H1 tags to the pages that are missing them                                           |
| **1. Information Architecture**              | Revamp "Hero Section" to make it easier for users to find what they're looking for.                                         |
| **2. Implement eCommerce Best Practices On The Site.**     | Implement Search, Navigability, and List-building Best Practices On The Site.                                            |
|                                            | Add an Exit Pop-Up to capture and save the emails of users who are leaving.                      |
|                                            | Offer discount coupons in exchange for their emails.                                                   |
|                                            | Add a Search Bar, to improve the user's search experience.                                                   |
|                                            | Add a ChatBot, to guide users to what they are looking for.                                                   |
| **3. Update Content Using Keyword Groups** | Use the strategic keyword lists we shared to compete more intelligently                                           |
|                                            | Updates on 'Learning Center', 'Fitting Process' and 'Events' pages to make them keyword-focused.                             |
| **4. Link Relevant Content Together**      | Enhance the 'Blog Articles Section' to increase its conversion.                             |
|                                            | Have a clear call to action on every page.                                                            |

---

# Bonus Recommendations

;;;

## Bonus Recommendations

1. If you advertise using PPC, consider targeting competitor brand names - especially for products you want to target <!-- .element: class="fragment" -->

2. Continuing to write about keywords (KW) and keyword clusters (KW clusters) to become an authority. <!-- .element: class="fragment" -->

3. Implement the transactional email marketing system. Acquire the emails of visitors to continue communicating with them via email and eventually convert them into buyers. <!-- .element: class="fragment" -->

4. Implement Algolia or Typesense to improve the search experience on the site. <!-- .element: class="fragment" -->

---

## How We Can Help

1. SE-Pro Plan - Our monthly service designed to implement SEO fixes and keep content current.
2. Essential Ecommerce Email Workflows - Set up an email marketing server and configure 5 essential workflows for engaging and converting visitors.
3. Add Algolia or Typesense to improve the search experience.

;;;

## SE-Pro Plan
Rehabilitate your on-page SEO and get the content you need to start ranking.

| | Basic | Pro |
| --- | --- | --- |
| On-page SEO Fixes | &#9989; | &#9989; |
| SEO Dashboard | &#9989; | &#9989; |
| Monthly SEO Strategy Call | &#9989; | &#9989; |
| SEO Optimization of New Content | &#9989; | &#9989; |
| Connect SEO Content to Conversion | &#9989; | &#9989; |
| SEO-driven Content Creation |  | 4/mo |
| **Monthly Price** | $749 | $999 |

_All plans are month-to-month with a 3-month minimum commitment._

&#129321; **Save <u>$3,000</u>** on the Pro Plan with an annual commitment! &#129321;

;;;

## Essential Ecommerce Email Workflows
The key transactional workflows that get customers on your list and buying more, more often.

| Feature | Value |
| --- | --- |
| Set up personal email marketing server | $1999 |
| Lead capture popup setup | $749 |
| Integrate popup with your Email Marketing Server | $399 |
| Create email templates | $799 |
| Welcome Series | $500 |
| Abandoned Cart Series | $500 |
| Post-Purchase Series | $500 |
| Win-Back Series | $500 |
| Expiring Offer Series | $500 |
| Anniversary Series | $500 |
| **Total Value** | **$6,946** |
| **Your Price** | **$4,999** |

Add stock status integration for just $1,499!

&#129321; **Save <u>30%</u>** when you bundle! &#129321;

;;;

## Integrate Enterprise Search
Implement Algolia or Typesense search to understand what your visitors are searching for on your site. Use this to drive more insight into what people are looking for and how to better serve them.

**Project Estimate: $7,450 (10 blocks)**

;;;

## Total Investments

| Service | Price | Frequency |
| --- | --- | --- |
| SE-Pro Plan | $749 or $999 | Monthly |
| Essential Ecommerce Email Workflows | $4,999 | One-time |
| Stock Status Integration | $1,499 | One-time |
| Integrate Enterprise Search | $7,450 | One-time |


---

# Next Steps

1. [Fitness Sports SEO Slides](https:slides.superwebpros.com/seo/fitnesssport.html) <!-- .element: class="fragment" -->

2. [Download Fitness Sport SEO Recommendations](https://s3.amazonaws.com/resources.superwebpros.com/bb/pdf/general/FitnessSport_SEOReport.pdf) <!-- .element: class="fragment" -->

3. [Access your SEO Dashboard](https://lookerstudio.google.com/reporting/b40ce16f-e4b6-4a72-bc61-5779ca190c1a) <!-- .element: class="fragment" -->

4. [Access your Custom Avatars](https://app.superwebpros.com/#CustomerAvatar) <!-- .element: class="fragment" -->

5. [Access your KW Clusters](https://app.superwebpros.com/#KeywordCluster) <!-- .element: class="fragment" -->

6. [Access your JourneyMaps](https://app.superwebpros.com/portal/customers#CustomerJourney) <!-- .element: class="fragment" -->

7. [Access your Contents Briefs](https://app.superwebpros.com/#ContentBrief) <!-- .element: class="fragment" -->






Note:

- This is set up in ProHQ for you already
npm st