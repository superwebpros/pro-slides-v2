# Agenda

1. Current State
2. Competition Review
3. The Opportunity
2. Recommendations

---

# Current State

;;;

## Current State

1. Low search visibility
2. Low Click Through Rates (CTR)
3. On-page SEO needs moderate improvement
4. Pagespeed needs significiant improvement 

Note:
- We ran the site on your GSC data
- We also ran the site through some of our own tools to get full metrics

;;;

## 1. Low Search Visibility

1. Most traffic comes from branded keywords
2. Then, portolio projects (by name)
3. No visibility from high-quality keywords

;;;

## 2. Low Click Through Rates (CTR)
The site's average CTR is 0.36%. This is low, even for a site with low search visibility.

> This represents low search intent; people are not clicking on your site when they see it in search results.

;;;

## 3. On-page SEO needs moderate improvement

1. Your pages have no SEO Meta Descriptions
2. Several pages have duplicate "H1" tags

;;;

## 4. Pagespeed needs significiant improvement

1. The site's average PageSpeed score is 18 (out of 100)
2. Page size averages over 2MB
3. Largely driven by images that are too large

---

# Competition Review

;;;

## Competitor Sites Reviewed
We reviewed several competitors to see how they are performing in search results. Here are a few salient ones:

1. [Christman Company](https://www.christmanco.com/)
2. [Granger Construction](https://www.grangerconstruction.com/)
3. [JRM Construction Management](https://www.jrmcm.com/)
4. [Kiewit](https://www.kiewit.com/)
5. [McCarthy](https://www.mccarthy.com/)

;;;

### General Observations

👉 Most competitor search results are based on branded terms <!-- .element: class="fragment" -->    

👉 Outside of branded terms, most competitors show up when people search for the projects they've completed <!-- .element: class="fragment" -->  

👉 High-quality keywords are not generally targeted, even by very large companies <!-- .element: class="fragment" -->  

❗️ But most sites have information architecture that is well-organized and COULD be optimized for search <!-- .element: class="fragment" -->  

;;;

### The lack of targeting is a great opportunity!

;;;

### Example 1: Christman Company
Branded: 4,400 searches per month

<img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/christman-co-branded.png" height="200"/>

Unbranded: 126 searches per month  

<img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/christman-co-unbranded.png" height="200"/>

;;;

### Example 2: McCarthy Information Architecture  

![McCarthy Navigation](https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/cleanshot-2024-03-05-at-14.43.34.png)

;;;

### Example 3: SERP for "Construction Management Company"  

<iframe width="1000" height="300" src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/cleanshot-2024-03-05-at-14.37.06.png"></iframe>

---

# The Opportunity

;;;

## TODO Ideal Customer Profiles

| Scott | Michael | Tom |
| --- | --- | --- |
| ![Scott](https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/wieland-avatar-scott.webp) | ![Michael](https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/wieland-avatar-michael.webp) | ![Tom](https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/wieland-avatar-tom.webp) |
| Real Estate Developer | VP, Facilities | COO/VP of Mfg |
| [Profile](https://app.superwebpros.com/portals/customers/#CustomerAvatar/view/65e60120db7e15f45) | [Profile](https://app.superwebpros.com/portals/customers/#CustomerAvatar/view/65e8834497cd819b7) | [Profile](https://app.superwebpros.com/portals/customers/#CustomerAvatar/view/65e888cb2f34b69e3) |

;;;

## Goals

1. Show up in search results for high-quality keywords
2. Convert more visitors into leads
3. Position Wieland for success in target verticals

Note:
- Emphasis on different locations
- Get into Higher Ed
- Get into things besides multi-family

---

# Recommendations

Note: 
- Foundational & Strategic Recommendations

;;;

## Foundational Recommendations  
Based on our review, we recommend the following foundational changes to the site:

1. Add Meta Descriptions to all pages 🚩🚩🚩  
2. Add keywords to page titles 🚩🚩🚩🚩  
2. Shrink large images to improve page speed ([Smush](https://wordpress.org/plugins/wp-smushit/)) 🚩🚩🚩🚩   
3. Remove duplicate H1 tags 🚩🚩   
4. Persist on getting Google Business pages for each location 🚩🚩🚩🚩🚩   

;;;

## Strategic Recommendations
Based on our review, we recommend the following strategic updates to the site:

1. Update the site's information architecture
2. Use keyword groups to inform updated content <!-- .element: class="fragment" -->  
3. Create connected journey throughout the site <!-- .element: class="fragment" -->  

;;;

## 1. Set up Foundational Information Architecture
Information architecture is the structural design of important information. It involves organizing, structuring, and labeling content so users and search engines can find it easily.

1. Add "Locations" & "Markets" pages. Augment "Services" pages. <!-- .element: class="fragment" -->
2. Revamp navigation to make it easier for users to find what they're looking for. <!-- .element: class="fragment" -->
3. (Optional) Add faceted search for portfolio items. <!-- .element: class="fragment" -->

;;;

## 2. Update Content Using Relevant Keywords
1. Rewrite service pages to be informative and keyword-driven <!-- .element: class="fragment" -->
2. Write 'market' pages to be informative and keyword-driven <!-- .element: class="fragment" -->
3. (Optional) Use your blog to show thought leadership in your target verticals <!-- .element: class="fragment" -->

;;;

## 3. Link Relevant Content Together
1. Create bi-directional links between service, portfolio, and market pages <!-- .element: class="fragment" -->
2. Have a clear call to action on every page <!-- .element: class="fragment" -->
3. (Optional) If you have a blog, link to it from the service, market, and portfolio pages <!-- .element: class="fragment" -->

---

# 1. Set up Foundational Information Architecture

---

## 1.1 Add "Locations" & "Markets" pages. Augment "Services" pages.

1. Add "Locations" pages to the site for your Florida & Louisiana offices. Make sure to include the address of your office and, potentially, a map.
2. Make your "Services" more robust and keyword-driven. This will help you show up in search results for high-quality keywords.
3. Create pages for the different "Markets" you serve. This should be distinct from the portfolio and provide an opportunity to add context & case studies in your target verticals.

;;;

### 1.1.1 Example - Create Pages for Each Location
Source: [https://www.mccarthy.com/locations/atlanta](https://www.mccarthy.com/locations/atlanta)

[![McCarthy Navigation](https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/cleanshot-2024-03-04-at-17.18.09@2x.png)](https://www.mccarthy.com/locations/atlanta)

;;;

### 1.1.2 Example - Expanded Services

<div class="container-fluid">
  <div class="row">
    <div class="col-md-4">
      <strong>JRM</strong>
      <img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/cleanshot-2024-03-04-at-17.23.31@2x.png"/>
    </div>
    <div class="col-md-4">
        <strong>Christman</strong>
        <img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/cleanshot-2024-03-04-at-17.24.50@2x.png"/>
    </div>
    <div class="col-md-4">
        <strong>Wieland</strong>
        <img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/cleanshot-2024-03-04-at-17.31.15@2x.png"/>
    </div>
  </div>
</div>

;;;

### 1.1.3 Example - Markets

![Kiewet Navigation](https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/wieland-navigation-kiewet.png)

Note:
- Directionally correct, but could be better

---

## 1.2 Revamp navigation to make it easier for users to find what they're looking for
The current navigation hides important pages and makes it difficult for users to find what they're looking for.

1. Unhide "Services", "Locations", and "Markets"
2. Use dropdown menus to make it easier for users to find what they're looking for

---

## 1.3 Add faceted search to [portfolio](https://wielandbuilds.com/portfolio/) page (Optional)

<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/wieland-faceted-search-christman.png"/>
    </div>
    <div class="col-md-6">
        <p>Faceted (filterable) search:</p>
        <ul>
          <li>Allows users to filter results based on different criteria</li>
          <li>Great for users who are browsing</li>
          <li>Can provide inspiration, even in unrelated industries</li>
        </ul>
    </div>
  </div>
</div>


Note:
- Many of the buildings are pretty; like browsing social media

---

# 2. Update Content Using Relevant Keywords

;;;

## Organize content around Keyword Clusters

Keyword clusters are groups of keywords that are closely related to each other. They are used to help search engines understand the content of a page. 

;;;

### Example - Google Search Console

![GSC Keywords](https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/wieland-example-many-queries.png)


;;;

## We've put together some clusters for you

1. [Higher Ed Construction Management](https://app.superwebpros.com/portal/customers/#CustomerJourney/view/65e87ed44bf552ee8)
2. [Industrial Construction Management](https://app.superwebpros.com/portal/customers/#CustomerJourney/view/65e85d1a6e4ca60c8)
3. [Multi-family Construction Management](https://app.superwebpros.com/portal/customers/#CustomerJourney/view/65e88151c45b176df)

---

## 2.1 Rewrite service pages to be informative and keyword-driven  

The current service pages are thin and not keyword-driven. This makes it difficult for search engines to understand what the page is about.

Note:
- The tone reads like an interview

;;;

## 2.1 Example - Christman Company  

<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <a href="https://www.christmanco.com/Services/Construction-Management" target="_blank"><img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/christman-keywords.png"/></a>
    </div>
    <div class="col-md-6">
        <p>This page:</p>
        <ul>
          <li>Has a relevant keyword in the “H1” section of the page</li>
          <li>Works relevant keywords into the text (see highlights)</li>
          <li>Uses graphics, as well as text to communicate their content.</li>
        </ul>
    </div>
    </div>
</div>

---

## 2.2 Write 'market' pages to be informative and keyword-driven

;;;

### 2.2 Example

<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <a href="https://www.mccarthy.com/projects/education" target="_blank"><img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/wieland-example-education-page.png"/></a>
    </div>
    <div class="col-md-6">
        <p>This page:</p>
        <ul>
          <li>Explains their philosophy & approach to work</li>
          <li>"Sells" it</li>
          <li>And shows off the work</li>
        </ul>
    </div>
  </div>
</div>


Note:
- Nobody is really killing it here


;;;

### 2.2 Example - Kiewet

<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <a href="https://www.kiewit.com/markets/oil-gas-chemical/" target="_blank"><img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/wieland-industry-oil-gas.png"/></a>
    </div>
    <div class="col-md-6">
        <p>This page:</p>
        <ul>
          <li>Deep dives into expertise</li>
          <li>Explains their philosophy & approach to work</li>
          <li>Relates to other facets of the market</li>
          <li>Links to portfolio</li>
          <li>❌ Is keyword driven</li>
        </ul>
    </div>
  </div>
</div>

---

## 2.3 Use blog to show thought leadership in target verticals (Optional) 
Nearly everyone has "news", but very few have a blog. This is a great opportunity to show thought leadership in your target verticals. 

<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <a href="https://newsroom.kiewit.com/kieways/operationsstartcard/" target="_blank"><img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/wieland-example-blog-post-kiewet.png"/></a>
    </div>
    <div class="col-md-6">
        <p>This page:</p>
        <ul>
          <li>Shows thought leadership & innovation</li>
          <li>Explains process</li>
          <li>❌ Is keyword driven</li>
        </ul>
    </div>
  </div>
</div> 

Note:
- Bluish ocean

---

# 3. Link Relevant Content Together

---

## 3.1 Create bi-directional links between service, portfolio, and market pages

<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/wieland-example-linked-content.png"/>
    </div>
    <div class="col-md-6">
        <p>For example:</p>
        <ul>
          <li>Link to relevant portfolio from "services"</li>
          <li>Consider linking to "services" from portfolio</li>
          <li>Include links to both on newly created market pages</li>
        </ul>
    </div>
  </div>
</div>

;;;

### 3.1 Example - Kiewet

<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <a href="https://www.kiewit.com/markets/oil-gas-chemical/" target="_blank"><img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/wieland-industry-oil-gas.png"/></a>
    </div>
    <div class="col-md-6">
        <p>This page:</p>
        <ul>
          <li>Deep dives into expertise</li>
          <li>Explains their philosophy & approach to work</li>
          <li>Relates to other facets of the market</li>
          <li>Links to portfolio</li>
          <li>❌ Is keyword driven</li>
        </ul>
    </div>
  </div>
</div>

---

## 3.2 Have a clear call to action on every page
While it's not likely you'll close deals from the site, it can still be a great way to start a conversation. Given the centrality of relationships, we recommend personalization as well as more 'generic' calls to action.

;;;

## 3.2 Example - McCarthy
Here, McCarthy introduces the primary point of contact for starting the engagement:

![McCarthy CTA](https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/wieland-example-personalized-contact.png)

---

## 3.3 Link to relevant blog posts from the service, market, and portfolio pages (Optional) 

Consider a post carousel that shows the most recent posts, scoped to the page the user is on.

---

# Recap

| Category | Recommendations |
|----------|-----------------|
| **Foundational Changes** | Add Meta Descriptions to all pages |
|                          | Add keywords to page titles |
|                          | Shrink large images to improve page speed (Using [Smush](https://wordpress.org/plugins/wp-smushit/)) |
|                          | Remove duplicate H1 tags |
|                          | Persist on getting Google Business pages for each location |
| **Strategic Updates**    | Extend the site's information architecture |
|                          | Use keyword groups to inform updated content |
|                          | Create connected journey throughout the site |
| **1. Extend Information Architecture** | Add "Locations" & "Markets" pages. Augment "Services" pages. |
|                                            | Revamp navigation to make it easier for users to find what they're looking for. |
|                                            | (Optional) Add faceted search for portfolio items. |
| **2. Update Content Using Keyword Groups** | Rewrite service pages to be informative and keyword-driven |
|                                            | Write 'market' pages to be informative and keyword-driven |
|                                            | (Optional) Add a blog to show thought leadership in your target verticals |
| **3. Link Relevant Content Together**         | Create bi-directional links between service, portfolio, and market pages |
|                                            | Have a clear call to action on every page |
|                                            | (Optional) If you have a blog, link to it from the service, market, and portfolio pages |

---

# Bonus Recommendations

;;;

## Bonus Recommendations
1. If you advertise using PPC, consider targeting competitor brand names - especially for verticals you want to target <!-- .element: class="fragment" -->  

2. Write whitepapers highlighting case studies to show how your past experience translates to success in your target verticals. Promote these via PPC, display ads, and direct LinkedIn outreach. <!-- .element: class="fragment" -->  

3. Acquire email addresses of target vertical decision-makers and run display ads to them directly (promoting white paper). <!-- .element: class="fragment" -->  

4. Get your site listed on relevant industry directories. <!-- .element: class="fragment" --> 

5. Double down on your blog to show thought leadership in your target verticals. Email 2x/month to your list. <!-- .element: class="fragment" -->

---

# Next Steps

1. We will connect GA4 to your current website <!-- .element: class="fragment" -->

2. [Download the Wieland SEO Slides](https:slides.superwebpros.com/seo/wieland.html) <!-- .element: class="fragment" -->

3. [Download the Wieland SEO Recommendations](https://s3.amazonaws.com/resources.superwebpros.com/pdf/reports/wieland.pdf) <!-- .element: class="fragment" -->

4. [Access your SEO Dashboard](https://lookerstudio.google.com/reporting/ce8f8cc3-c994-40e8-ab1c-41d328bc4b6a) <!-- .element: class="fragment" -->  

5. [Access your ProHQ JourneyMaps](https://app.superwebpros.com/portal/customers#CustomerJourney) <!-- .element: class="fragment" -->

6. [Start working through the list!](https://app.superwebpros.com/portal/customers/#AccountMilestone/view/65e8828f0ff3378cc) <!-- .element: class="fragment" -->

Note: 
- This is set up in ProHQ for you already