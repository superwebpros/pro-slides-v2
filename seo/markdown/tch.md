# Agenda:

1. Current State
2. Competition Review
3. The Opportunity
4. Recommendations

---

# Current State

;;;

## Current State

1. On-page SEO needs moderate improvement
2. Pagespeed needs improvement
3. The complete sitemap was not indexed


Note:

- We ran the site on your GSC data
- We also ran the site through some of our own tools to get full metrics

;;;

## 1. On-page SEO needs moderate improvement

1. Many pages don't have SEO Meta Descriptions
2. Several pages don't have Title "H1" tags
3. Some pages have Title "H1" duplicated

;;;

## 2. Pagespeed needs improvement

1. The site's Speed Index is 13.4s (mobile) and 3.2s (desktop)
2. Largely driven by images that are too large

Note:
Image size averages over 2.4MB

;;;

## 3. The complete sitemap was not indexed

Google Search Console was not configured for the entire domain (already corrected), some pages were not appearing for organic searches.



---

# Competition Review

;;;

## Competitors Sites Reviewed

We reviewed several competitors to see how they are performing in search results. Here are a few salient ones:

1. [McMurray Hatchery](https://www.mcmurrayhatchery.com/)

2. [Cackle Hatchery](https://www.cacklehatchery.com/)

3. [Hoovers Hatchery](https://www.hoovershatchery.com/)

;;;

### General Observations

👉 Status compared to your strongest competitors.

<img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/competitiors_comparison2.png" height="350"/>

;;;

👉 Your strongest competitors started earlier with SEO, so there's some catch-up.

#### You 

<img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/traffic-tch.png" height="350" width="800"/> 

;;;

#### Your Competitors

<img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/traffic-competitors.png" height="350" width="800"/> 

;;;

### Traffic comparison

<img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/traffic-comparison.png" height="400"/>

;;;

👉 They are paying for traffic on Google.

👉 Everyone has a good distribution of traffic between Brand and Non-Brand (>75% non-brand). <!-- .element: class="fragment" -->

👉 Competitors' Non-Brand traffic mainly targets their products. <!-- .element: class="fragment" -->

❗️ But most sites have an information architecture that is not well organized and COULD be better optimized for search and conversion. <!-- .element: class="fragment" -->

;;;

### SERP for "Chick Hatchery"

There are many opportunities with the search intent available.

<iframe width="1100" height="300" src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/serp.png"></iframe>

;;;

### The lack of optimization on your pages is a great opportunity to convert visitors into buyers!

---

# The Opportunity

;;;

## Ideal Customer Profiles

Chris Hensworth

![Chris](https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/theChickHatchery-avatar.webp)

Business Owner

[Profile](https://app.superwebpros.com/portal/customers/#CustomerAvatar/view/661eb93b4c4b62c04)

;;;

## Goals

1. Show up in search results for high-quality keywords

2. Get more visitors on your email list

3. Position The Chick Hatchery for success in target verticals

---

# Recommendations

Note:

- Foundational & Strategic Recommendations

;;;

## Foundational Recommendations

Based on our review, we recommend the following foundational changes to the site:

1. Add Meta Descriptions to all pages 🚩🚩
2. Add keywords to page titles 🚩🚩🚩
3. Shrink large images to improve page speed ([Smush](https://wordpress.org/plugins/wp-smushit/)) 🚩🚩🚩
4. Remove duplicate H1 tags 🚩🚩🚩🚩
5. Add H1 tags to the pages that are missing them. 🚩🚩🚩🚩

;;;

## Strategic Recommendations

Based on our review, we recommend the following strategic updates to the site:

1. Update the site's information architecture <!-- .element: class="fragment" -->

2. Implement search, navigability, and email building best practices on the site <!-- .element: class="fragment" -->

3. Use Keyword Clusters to improve content <!-- .element: class="fragment" -->

4. Create connected journey throughout the site <!-- .element: class="fragment" -->

;;;

## 1. Set up Foundational Information Architecture

Information architecture is the structural design of important information.
It involves organizing, structuring, and labeling content so users and search engines can find it easily.

👉 Revamp navigation to make it easier for users to find what they're looking for. <!-- .element: class="fragment" -->


;;;


## 2. Implement Search, Navigability, and List-building Best Practices On The Site

1. Reorganizing the structure of the Home page to enhance conversion. <!-- .element: class="fragment" -->

2. Add an Exit Pop-Up to capture and save the emails of users who are leaving. <!-- .element: class="fragment" -->

3. Add offer discount coupons in exchange for their emails. <!-- .element: class="fragment" -->

4. Upgrade use of Typesense, to understand what your visitors are searching for on your site
 <!-- .element: class="fragment" -->

;;;

#### You need to grow your contact list (buyers and non-buyers) to capitalize that organic traffic and be able to communicate with them more easily in the future.



;;;

## 3. Update Content Using Relevant Keywords

👉 Rewrite "Poultry Care" pages to be informative and keyword-driven <!-- .element: class="fragment" -->

👉 Update "Product Pages" descriptions to improve conversion <!-- .element: class="fragment" -->



;;;

## 4. Link Relevant Content Together

1. Create bi-directional links between Blog, Home, Poultry Care pages to Products pages  <!-- .element: class="fragment" -->

2. Have a clear call to action on every page <!-- .element: class="fragment" -->


---

# 1. Set up Foundational Information Architecture

;;;

## 1.1 Revamp navigation to make it easier for users to find what they're looking for.

Making the main links easily accessible, quickly and efficiently, focusing on conversion.

Source: [https://www.mcmurrayhatchery.com](www.mcmurrayhatchery.com)
<img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/menu-mc.png" height="350" width="750"/>


---

# 2. Implement ecommerce best practices on the site.

;;;

## 2.1 Reorganizing the structure of the Home page to enhance conversion. 

1. Main banner with featured products + budgets (for authority)  <!-- .element: class="fragment" -->

2. Featured categories section <!-- .element: class="fragment" -->

3. Special offers or discount section <!-- .element: class="fragment" -->

4. Customer testimonials or reviews <!-- .element: class="fragment" -->

5. New arrivals or featured products section <!-- .element: class="fragment" -->

6. Features articles section <!-- .element: class="fragment" -->

7. Last call to Action to gain a Coupon <!-- .element: class="fragment" -->


;;;

### Example: Main banner with featured products + budgets (for authority)

<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/heroS-tch.png"/>
    </div>
    <div class="col-md-6">
        <p>Main purpose:</p>
        <ul>
          <li>Highlight Featured Products</li>
          <li>Establish Authority</li>
          <li>Enhance User Experience</li>
        </ul>
    </div>
  </div>
</div>

;;;


### Example: Featured categories section

<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/categories-tch.png"/>
    </div>
    <div class="col-md-6">
        <p>Main purpose:</p>
        <ul>
          <li>Facilitate Navigation</li>
          <li>Drive Traffic to Key Pages</li>
          <li>Optimize for SEO</li>
        </ul>
    </div>
  </div>
</div>


;;;

## 2.2 Add an Exit Pop-Up to capture and save the emails of users who are leaving.

<img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/exitpopup.png" 
height="400" width="750"/> 

;;;

#### With this implementation we could:


1. Reduce bounce rates <!-- .element: class="fragment" -->

2. Increase conversions <!-- .element: class="fragment" -->

3. Capture more leads on your email list <!-- .element: class="fragment" -->


;;;

## 2.3 Offer discount coupons in exchange for their emails. 

<img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/disc-cupon.png" height="400" width="750"/> 

;;;

#### This helps with:

1. Attracting New Customers <!-- .element: class="fragment" -->

2. Encouraging Repeat Purchases <!-- .element: class="fragment" -->

3. Increasing Average Order Value <!-- .element: class="fragment" -->

---

# 3. Update Content Using Relevant Keywords

;;;

## Organize content around Keyword Clusters


Keyword clusters are groups of keywords that are closely related to each other. They are used to help search engines understand the content of a page.

;;;

### Example - Google Search Console

[![GSC Keywords](https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/GSC-tch.png)](https://search.google.com/u/1/search-console?resource_id=sc-domain%3Athechickhatchery.com)

;;;

## 3.1 Understanding the content and KW that your competition has.

;;;

### This shows:

1. Total number of keywords each one has.

2. Total number of unique keywords each one has.

<img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/uniqueKW.png" height="400" width="750"/> 

;;;

## 3.2 We extracted some KW lists for which your competition is successful, in order to give a clear content route.

;;;

List of Keywords, starting from you competitors: 

1. [KW gap ALLcompetitors and you](https://app.superwebpros.com/portal/customers/#KeywordCluster/view/662c01bb70f4812a4)

2. [mcMurray UNIQUE KW](https://app.superwebpros.com/portal/customers/#KeywordCluster/view/662c01bb71e3ae6e9)

3. [cackle UNIQUE KW](https://app.superwebpros.com/portal/customers/#KeywordCluster/view/662c01bb73ac4e1bd)

4. [Hoovers UNIQUE KW](https://app.superwebpros.com/portal/customers/#KeywordCluster/view/662c01bb72c8e209a)


;;;

## 3.3 We suggest updates on 'Product Pages' and 'Poultry Care' pages to make them more informative and keyword-focused.


The current "Poultry Care Section" and "Product Pages" are not keyword-driven and with opportunities for improvements in the description. This makes it difficult for search engines to understand what the page is about.


Note:

- The tone reads like an interview


---

# 4. Link Relevant Content Together

;;;

## 4.1 Create bi-directional links between Blog, Home, Poultry Care pages to Products pages

<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/Article-Layout.png"/>
    </div>
    <div class="col-md-6">
        <p>Goals:</p>
        <ul>
          <li>Increase conversions</li>
          <li>Attracting New Customers</li>
          <li>Capture leads on your email list</li>
        </ul>
    </div>
  </div>
</div>


;;;

## 4.2 Have a clear call to action on every page


1. Guides User Behavior <!-- .element: class="fragment" -->

2. Enhances User Experience <!-- .element: class="fragment" -->

3. Facilitates Site Navigation <!-- .element: class="fragment" -->



---

# Recap


| Category                                   | Recommendations                                                                                      |
| ------------------------------------------ | ---------------------------------------------------------------------------------------------------- |
| **Foundational Changes**                   | Add Meta Descriptions to all pages                                                                   |
|                                            | Add keywords to page titles                                                                          |
|                                            | Shrink large images to improve page speed (Using [Smush](https://wordpress.org/plugins/wp-smushit/)) |
|                                            | Remove duplicate H1 tags                                                                             |
|                                            | Add H1 tags to the pages that are missing them                                           |
| **Strategic Updates**                      | Set up Foundational Information Architecture                                                           |
|                                            | Use keyword groups to inform updated content                                                         |
|                                            | Create connected journey throughout the site                                                         |
| **1. Extend Information Architecture**     | Revamp navigation to make it easier for users to find what they're looking for.                                         |
| **2. Implement eCommerce Best Practices On The Site.**     | Reorganizing the structure of the Home page to enhance conversion.                                            |
|                                            | Add an Exit Pop-Up to capture and save the emails of users who are leaving.                      |
|                                            | Offer discount coupons in exchange for their emails.                                                   |
|                                            | Set up Typesense.                                                   |
| **3. Update Content Using Keyword Groups** | Use the strategic keyword lists we shared to compete more intelligently                                           |
|                                            | Updates on 'Product Pages' and 'Poultry Care' pages to make them more informative and keyword-focused.                             |
| **4. Link Relevant Content Together**      | Create bi-directional links between Blog, Home, Poultry Care pages to Products pages.                             |
|                                            | Have a clear call to action on every page.                                                            |

---

# Bonus Recommendations

;;;

## Bonus Recommendations

1. If you advertise using PPC, consider targeting competitor brand names - especially for products you want to target <!-- .element: class="fragment" -->

2. Write content using keywords (KW) and keyword clusters (KW clusters) to become an authority. <!-- .element: class="fragment" -->

3. Implement the broadcast and transactional email marketing system we previously suggested (and have queued up). Acquire the emails of visitors to continue communicating with them via email and eventually convert them into buyers. <!-- .element: class="fragment" -->

;;;

## How We Can Help

1. SE-Pro Plan - Our monthly service designed to implement SEO fixes and keep content current.
2. Essential Ecommerce Email Workflows - Set up an email marketing server and configure 5 essential workflows for engaging and converting visitors.
3. Add search analytics to Typesense to understand what your visitors are searching for on your site.

;;;

## SE-Pro Plan
Rehabilitate your on-page SEO and get the content you need to start ranking.

| | Basic | Pro |
| --- | --- | --- |
| On-page SEO Fixes | &#9989; | &#9989; |
| SEO Dashboard | &#9989; | &#9989; |
| Monthly SEO Strategy Call | &#9989; | &#9989; |
| SEO Optimization of New Content | &#9989; | &#9989; |
| Connect SEO Content to Conversion | &#9989; | &#9989; |
| SEO-driven Content Creation |  | 4/mo |
| **Monthly Price** | $749 | $999 |

_All plans are month-to-month with a 3-month minimum commitment._

&#129321; **Save <u>$3,000</u>** on the Pro Plan with an annual commitment! &#129321;

;;;

## Essential Ecommerce Email Workflows
The key transactional workflows that get customers on your list and buying more, more often.

| Feature | Value |
| --- | --- |
| Set up personal email marketing server | $1999 |
| Lead capture popup setup | $749 |
| Integrate popup with your Email Marketing Server | $399 |
| Create email templates | $799 |
| Welcome Series | $500 |
| Abandoned Cart Series | $500 |
| Post-Purchase Series | $500 |
| Win-Back Series | $500 |
| Expiring Offer Series | $500 |
| Anniversary Series | $500 |
| **Total Value** | **$6,946** |
| **Your Price** | **$4,999** |

Add stock status integration for just $1,499!

&#129321; **Save <u>30%</u>** when you bundle! &#129321;

;;;

## Add search analytics to Typesense
Upgrade your Typesense search to understand what your visitors are searching for on your site. Use this to drive more insight into what people are looking for and how to better serve them.

**Project Estimate: $2,247 (3 blocks)**

;;;

## Total Investments

| Service | Price | Frequency |
| --- | --- | --- |
| SE-Pro Plan | $749 or $999 | Monthly |
| Essential Ecommerce Email Workflows | $4,999 | One-time |
| Stock Status Integration | $1,499 | One-time |
| Add search analytics to Typesense | $2,247 | One-time |

---

# Next Steps

1. [The TheChickHatchery SEO Slides](https:slides.superwebpros.com/seo/the-chick-hatchery.html) <!-- .element: class="fragment" -->

2. [Download the TheChickHatchery SEO Recommendations](https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/TheChickHatcherySEOReport.pdf) <!-- .element: class="fragment" -->

3. [Access your SEO Dashboard](https://lookerstudio.google.com/reporting/ac075be1-857e-4900-bc56-9b3c29e8a409) <!-- .element: class="fragment" -->

4. [Access your ProHQ JourneyMaps](https://app.superwebpros.com/portal/customers#CustomerJourney) <!-- .element: class="fragment" -->




Note:

- This is set up in ProHQ for you already
