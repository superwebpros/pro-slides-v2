# About SuperWebPros
- Founded 2016
- 10 Employees worldwide
- Solely focused on websites, SEO, AI, and automation
- Industry agnostic

Note:
- Wordpress
- Shopify
- Integrations, data pipelines, etc.

;;;

## Core Values
> A Pro is _SHARP_

1. **Super** - A Pro aims to excel in everything we do. 

2. **Helpful** - We help our customers and each other. 

3. **Agile** - We are curious, growth-minded, & build a culture of rapid learning and iteration.

4. **Reliable** - We do what we say we will do and aim to do it quickly.

5. **Proactive** - We create better outcomes for customers - and make each other better - when we seek opportunities to improve, unprompted.

---

## Business Goals

1. **Diversification**: Wieland operates in multiple sectors, including manufacturing, industrial, heavy industrial, pulp and paper, warehouse distribution, multifamily, churches, and professional offices, aiming to be "category killers" in several areas.

2. **Geographic Focus**: The company is expanding its presence in different regions, particularly Florida and Louisiana.

3. **Higher Education Market Entry**: Wieland is making a five-year commitment to pursue work in the higher education construction market in Michigan to build a portfolio and compete for these projects.


;;;

## Business Goals

4. **Brand Repositioning**: In markets like Orlando, Wieland is working to change perceptions about their expertise, investing in marketing to highlight their industrial capabilities.

5. **Delivery Method Expertise**: Wieland aims to capitalize on their expertise in different project delivery methods, particularly as a construction manager, which accounts for about 80% of their work.

6. **Industry Education**: Wieland sees an opportunity in educating potential clients about construction costs, delivery methods, and industry trends through targeted content marketing.

---

## Wieland Assessment
The full assessment can be found in our [original SEO audit](https://slides.superwebpros.com/seo/wieland.html).

| Category | Recommendations |
|----------|-----------------|
| **Foundational Changes** | Add Meta Descriptions to all pages |
|                          | Add keywords to page titles |
|                          | Shrink large images to improve page speed (Using [Smush](https://wordpress.org/plugins/wp-smushit/)) |
|                          | Remove duplicate H1 tags |
|                          | Persist on getting Google Business pages for each location |
| **Strategic Updates**    | Extend the site's information architecture |
|                          | Use keyword groups to inform updated content |
|                          | Create connected journey throughout the site |
| **1. Extend Information Architecture** | Add "Locations" & "Markets" pages. Augment "Services" pages. |
|                                            | Revamp navigation to make it easier for users to find what they're looking for. |
|                                            | (Optional) Add faceted search for portfolio items. |
| **2. Update Content Using Keyword Groups** | Rewrite service pages to be informative and keyword-driven |
|                                            | Write 'market' pages to be informative and keyword-driven |
|                                            | (Optional) Add a blog to show thought leadership in your target verticals |
| **3. Link Relevant Content Together**         | Create bi-directional links between service, portfolio, and market pages |
|                                            | Have a clear call to action on every page |
|                                            | (Optional) If you have a blog, link to it from the service, market, and portfolio pages |

;;;

### Examples
- [Information Architecture](https://slides.superwebpros.com/seo/wieland.html#/3/5)
- [Location pages](https://slides.superwebpros.com/seo/wieland.html#/7/1)
- [Faceted Search](https://www.naimidmichigan.com/properties/?=undefined&safari-private=false)
- [Market pages](https://slides.superwebpros.com/seo/wieland.html#/12/2)
- [Human contact](https://slides.superwebpros.com/seo/wieland.html#/16/1)

---

## Comps
Just one of our clients:
1. [NAI MidMichigan](https://naimidmichigan.com)

**High-quality Competitors in your industry:**  
1. [Kiewit](https://www.kiewit.com/) - Exceptional information architecture and navigability
2. [McCarthy](https://www.mccarthy.com/) - Great overall design, architecture, and content
3. [Rockford Construction](https://rockfordconstruction.com/) - Exceptional information architecture, design, and solid content. Portal access for clients, residents, and team.
4. [The Boldt Company](https://www.boldt.com/) - Great overall design, architecture, and content. Clear calls to action throughout.

---

## Approach & Process
- **What software would you use?** _Wordpress_
- **Would you code it and design it? Explain why.** _Yes, but we would use an existing design kit and your brand standards, in order to save time and money._
- **How much control could WIELAND have?** _This is your website, so you would have full control._

;;;

## Approach & Process

- **Generally speaking, how long do these types of projects take?** _It varies, but our defaulut is to have it done inside of 30-90 days._
- **Would you want to host it?** _Most of our customers choose to host with us, but it's not required. We strongly recommend a managed provider like WPEngine or Kinsta._
- **What are WIELAND’s exit strategies, if it should need one?  Who would own the site, domain, content, etc**.  _You should own your own infrastructure and assets as much as possible._

---

## Thank You!

PDF Handout  
[https://storage.googleapis.com/resources.superwebpros.com/bb/pdf/wieland-supersite.pdf](https://storage.googleapis.com/resources.superwebpros.com/bb/pdf/wieland-supersite.pdf)