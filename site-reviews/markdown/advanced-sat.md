# Observations

## Good, SEO Friendly Content


## Conversion Paths

## Copywriting

---

# Questions

## Sidebar Questions

<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <h2>Which is it?</h2>
      <ul>
        <li>Reduce friction</li>
        <li>Two separate things?</li>
      </ul>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <img src="https://www.dropbox.com/s/86q089ywecdm36r/CleanShot%202021-01-27%20at%2015.09.54%402x.png?dl=1"/>
    </div>
  </div>
</div>

---

# Strategies

## Hooks vs. Nets

## LinkedIn Strategies

## Webinar/Demo Strategies

## Geofencing Strategies

## Other Channels