## Agenda

1. Introduction
2. Verify access to the things we need
3. Confirm the starting Supersite
4. Schedule the Content Intake

---

## Introduction

;;;

## Recap: Supersite Process

1. 7 Core Layouts
2. We will handle all copy and images up front
3. You'll have the opportunity to review & provide feedback on content
4. We will update it within a few days and then launch the site on our servers on your behalf
5. You'll have a free 2 week trial of Super Support where you can request additional changes or feedback that comes to mind after it's up.

Note:
- Can remove sections
- Can change images/videos
- Can change copy
- Can change colors
- Can change fonts
- CAN'T change layouts or add new sections (no design changes)
- We'll show you how to do that during the revision stage of the project (very easy to do)

;;;

> &#10071; The revision cycle will be focused on content, copy, colors, and fonts. Design (layout or new functions) changes extra.


;;;

> **Pro Tip:** Many people get stuck on perfectionism and never launch! A website is always a work in process and it's better to get something up and running than to wait for perfection.

Note:
- We've launched hundreds of sites and most get almost no traffic immediately, so there's time to tweak and make changes. 
- The sooner its live, the sooner Google gets it in the database.

---

## Things we'll need from you

1. **Domain Name** - We need access to the domain registrar to point the domain to our servers.
4. **Email Address for Forms** - We need an email address to send form submissions to.
6. **Logo & Content Files** - We need the logo and any other content files you have.
2. **Hosting** - We need access to the hosting account to set up the site. (Optional)
3. **Website** - We need access to the current website to pull content and images from (Optional).
5. **Social Media** - We need links for social media accounts to link to the site. (Optional)


;;;

## ProHQ
ProHQ is where she can communicate with us and add things we might need for the project.

1. Upload files and share comments in your Project
2. Site feedback creates Support Tickets

Note:
- Confirm access to ProHQ

;;;

Please open [https://app.superwebpros.com/portal/customers/](https://app.superwebpros.com/portal/customers/) & share your screen with us

;;;

## Passbolt
Passbolt is where we store passwords securely.

1. Confirm access
2. Confirm can see password folders

;;;

## Google Trifecta
Do you have the following:

1. Google Analytics
2. Google Search Console
3. Google Tag Manager
4. Google My Business

> &#10071; If not, we will send links on how to set these up and share access. You should own these accounts, not us.

Note:
- If not, we need to send links on how to set these up and share access.

---

## Confirm the starting Supersite
Have you made a choice on which Supersite you want to start with?

Note:
- Have 2 on the back burner
- If we need to change, we'll send options in 24 hours

---

## Schedule the Content Intake
What to expect on that intake:

1. We'll do a deep dive into your business and what sets you apart
2. Try to understand your services for the service pages
3. Uncover any offers or promotions you want to make sure the website promote

;;;

## How to Prepare for the Content Intake

1. **List of Services** - What do you offer?
2. **List of Offers** - What are you promoting?
3. **List of Team Members** - Who are the people behind the business?
4. **List of Testimonials** - What are your customers saying? (those can be added to ProHQ)
5. **List of FAQs** - What are the most common questions you get?
6. **Tone & Style** - What is the tone and style you want to convey?

;;;

> &#10071; If you have documents or copy that you want to make sure we use or see, please upload them to the Project in ProHQ.

;;;

## When would you like to schedule the Content Intake?

When would you like to [schedule the content intake](https://calendly.com/superwebpros-calendar/content-intake)?

---

## Thank You!


