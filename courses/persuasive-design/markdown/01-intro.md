Humans aren't rational. We're emotional. We're irrational. We're hackable.

Persuasive design is about leverating the irrational to improve outcomes for users and businesses.

We justify retroactively

Trigger irrationally, justify rationally

## Remember this?

![Dress](https://upload.wikimedia.org/wikipedia/en/2/21/The_dress_blueblackwhitegold.jpg)

Note:
- 2015 viral phenomenon
- 73% of people saw it as white and gold
- 27% saw it as blue and black

;;;





Core takeaway from this 'lesson':

Human minds are hackable. We can use this to improve outcomes for users and businesses.

## Purpose of the course
1. Introduce you to the 'bugs' in our brains that make us susceptible to persuasion
2. Share persuasive triggers that allow you to 'hack' these bugs
3. Give examples of persuasive design in action
4. How to build systems to leverage persuasive design to sell more, more often

;;;

## Course is designed to be practical
- Each video will give you a principle of persuasion
- Then show you how to use it
- Then show you examples of it in action
- Then help you learn how to use it yourself


## How this course is structured

;;;

## Resources and Community

;;;

- Hook: The "Dress"
    - What is persuasive design?
    - Why is it important?
    - How can you use it?
 - What is persuasive design?
 - Leveraging human psychology to improve outcomes
    - For the user
    - For the business


    Prime - Trigger - Deliver