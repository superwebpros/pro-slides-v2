## The Experiment




## The Results

## The Takeaway

## How to Use it

1. If you're a restaurant, always _overestimate_ the wait time.
2. Always present higher prices first.
3. Create bundles with a high-priced product as the anchor in a bundle
4. Stack prices or value, then 'back off'


1200 projects completed - $899 for a super site (anchor bias?)