1. There will always be an offer or offers
2. There will be a reason to respond right now
3. Give clear instructions
4. Tracking, measuring, and accountability
5. Follow-up
6. Leverage persuasion triggers in your copy
7. Don't do anything that doesn't align with these principles