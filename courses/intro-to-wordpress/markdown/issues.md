## Why Local Development?
- Developing a website on your computer
- Not (generally) exposed to the Internet <!-- .element: class="fragment" -->
- No server costs <!-- .element: class="fragment" -->
- Great for getting started, learning, testing, and prototyping <!-- .element: class="fragment" -->

---

## Issues with Local Development
Many plugins don't work

1. Security plugins <!-- .element: class="fragment" -->
2. Caching plugins <!-- .element: class="fragment" -->
3. Contact forms <!-- .element: class="fragment" -->
4. SEO plugins <!-- .element: class="fragment" -->

;;;

## Issues with Local Development
Migration to a live host can be difficult. 

Still, it's a great way to get up to speed quickly and learn the basics <!-- .element: class="fragment" -->
(Or, test new stuff)
