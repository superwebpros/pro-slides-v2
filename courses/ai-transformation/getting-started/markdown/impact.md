# Demo

note:
- What is AI & why does it matter?

---

## An American Folktale

<img src="https://s3.amazonaws.com/resources.superwebpros.com/images/midjourney_example_john-henry.png" style="width: 400px"/>

[https://www.midjourney.com/app/jobs/3009524c-4b43-44d5-9eeb-bf5142ea0aca/](https://www.midjourney.com/app/jobs/3009524c-4b43-44d5-9eeb-bf5142ea0aca/) / [GPT Prompt](https://chat.openai.com/share/65957855-8169-40cd-8326-db42b36f2cf5)

---

## Today

<img src="https://s3.amazonaws.com/resources.superwebpros.com/images/midjourney_black-and-white-modern-john-henry.png" style="width: 400px"/>

[https://www.midjourney.com/app/jobs/fd061169-5522-4a36-8a34-55123ef74acf/](https://www.midjourney.com/app/jobs/fd061169-5522-4a36-8a34-55123ef74acf/)


---

# The Big Idea:

## We need to begin AI Augmentation now

---

![Karim Lakhani](https://eiwpyksmmiv.exactdn.com/wp-content/uploads/2022/01/Karim-Lakhani-preferred-headshot-3.10.20_cropped-360x360-1.jpg)

#### "AI Won’t Replace Humans — But Humans With AI Will Replace Humans Without AI"  

_Karim R. Lakhani, Co-Director of the Laboratory for Innovation Science at Harvard Business School_, author [Competing in the Age of AI](https://www.amazon.com/Competing-Age-AI-Leadership-Algorithms/dp/1633697622/ref=asc_df_1633697622/)

---

# Goal for this module
30,000 foot view of how AI is being used & what results are being seen 

Note:
- Focus on Generative AI

---

# AI is already here

- You're _already_ using it (Siri, Alexa, Canva)
- Search Engines
- Social Media
- Digital Advertising
- Productivity Tools: Loom, Otter.ai, Notion, etc.
- Infrastructure (Dialpad, Twilio, etc.)
- And more...

Note:
- Expect to see more products have it embedded (Office, Google Suite, Notion, etc.)
- Example: Code/auto complete in my IDE

---

# What's Changed?

1. Accessibility
2. Interface
3. Adoption

Note:
- ChatGPT changed the interface in such a way that billions of people can now use it. It's put pressure on search, for instance.
- They also released an API, which provides programmatic accessibility, so applications can be built on top of it (or augmented with it)
- Adoption has increased because as accessibility increases, infrastructure cost of implemetnation is much lower than it used to be.
- Additionally, open source algorithms reduce the need for generalized data scientists

---

## How is AI being used?

| Use Case | What it does | Percent of Respondents |
| -------- | ------------ | ---------------------- |
| Robotic Process Automation | Automates repetitive tasks | 40% |
| Computer Vision | Analyzes images and video | 34% |
| **Natural Language Processing** 📈 | Analyzes text and speech | 31% |

_Source: McKinsey, [The State of AI in 2022](https://www.mckinsey.com/capabilities/quantumblack/our-insights/the-state-of-ai-in-2022-and-a-half-decade-in-review)_

Note: 
- We'll start with enterprise use cases, then come 'down market'

---

## Adoption is happening at the business unit

![Chart showing increasing use cases in the business units](/assets/chart_ai-use-case-at-business-unit.png)

Note:
- Idea is that AI is being released incrementally, not all at once.

;;;

### What are the top AI use cases?

![Chart showing top AI use cases](https://s3.amazonaws.com/resources.superwebpros.com/images/chart_ai-adopted-use-cases.png)

;;;

### What impacts are companies seeing?

![Chart showing top AI impacts](/assets/chart_ai-value-creation.png)

Note:
- Benefits to cost reduction in a potentially recessionary environment is a big deal.

---

## Leaders are growing faster than laggards are implementing

[McKinsey Report](https://www.mckinsey.com/capabilities/quantumblack/our-insights/the-state-of-ai-in-2022-and-a-half-decade-in-review)

- 25% of respondents attribute > 5% of EBIT attributable to AI investments
- 8% of respondents attribute > 20% of EBIT attributable to AI investments
    - Those 8% are getting to 20% faster than the 25% are getting to 5% or the laggards are starting.

Note:
- The companies making the investments are seeing bigger results, faster
- The 'toe dippers' are seeing some benefits, but not necessarily has huge.
- There are several reasons why the 'best' companies are seeing benefits and the worst ones aren't

;;;

## Investments are increasing substantially

Percent of enterprises with > 5% of tech budget allocated to AI

| Year | % Respondents |
| ---- | ---------------------- |
| 2018 | 40%                    |
| 2022 | 52%                    |
| 2025 | 63%                    |

Note:
- The most successful companies spending > 20% of technical budget on AI
- Moving budgets _out_ of IT and making them cross-functional

;;;

### How AI Strategy is being implemented

![Chart showing AI strategy implementation](https://s3.amazonaws.com/resources.superwebpros.com/images/chart_ai-strategy.png)

Note: 
- People Problem

;;;

### Data management is a problem

![Chart showing data management problems](https://s3.amazonaws.com/resources.superwebpros.com/images/chart_ai-data-health.png)

Note:
- Data Problem

;;;

### Knowing how to set up models & pipelines is a problem

![Chart showing model & pipeline problems](https://s3.amazonaws.com/resources.superwebpros.com/images/chart_ai-using-models-problem.png)

Note:
- Technical Problem

;;;

### Talent is a problem

![Chart showing talent problems](https://s3.amazonaws.com/resources.superwebpros.com/images/chart_ai-labor-problem.png)

Note:
- Labor/Talent Problem

;;;

### How are organizations solving this problem?

![Chart showing how organizations are solving the problem](/assets/chart_ai-how-talent-is-being-formed.png)

---

## Implications

1. AI is still in its infancy
2. But growing very quickly <!-- .element: class="fragment" -->
3. With measureable impacts to both revenue and cost structure <!-- .element: class="fragment" -->
4. Most companies don't have the context or talent to implement AI technologies as quickly as they'd like. <!-- .element: class="fragment" -->

---

# The Pro's AI Growthmap

<iframe width="500" height="500" data-src="https://s3.amazonaws.com/resources.superwebpros.com/pdf/pro-ai-growthmap.pdf"></iframe>

Note:
Link below

---

# What's Next?