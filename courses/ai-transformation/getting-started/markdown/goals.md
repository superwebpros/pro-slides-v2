# Thank you for leading!

note:
- I don't need to tell you that AI is a hot topic right now. It's everywhere. And it's changing the way we do business.
- Congratulations on being willing to take the first step towards organizational transformation. I'm grateful to be your guide on this journey.

---

# Course Goals
1. Demystify AI
2. Provide a framework & roadmap for leading your own AI transformation
3. Provide resources for leading your organization through an AI transformation
4. Share some concrete examples you can use to get started

Note:
1. Separate the hype from the reality
2. AI transformation requires some radically different thinking and operating
2. I'll share examples from our own experience
3. I can't solve every problem right now - because every business is different - but included in this course is a free 60 minute strategy call with me to help you get started.

---

# What we won't cover
1. This is a high-level course, we won't be getting into the weeds of technical implementation
2. We won't be covering AI algorithms or how they work
3. We won't be providing specific implementation advice for your business

note:
- We will be sharing examples and resources that can fuel your thinking and help you get started.
- We don't know your business at this point well enough to give you specific advice. That's what the strategy call is for.

---

# Prerequisites for a Successful Transformation
1. A willingness to learn
2. A willingness to change
3. A willingness to fail
4. A willingness to lead

---

# Assumptions
1. You have _at least_ 10-20 employees
2. Your business already has operating principles, processes, and systems
3. You track at least some information digitially in your business

note:
- CRM, database, Quickbooks, email, etc

;;;

## Traction
<div class="row">
  <div class="col-md-6">
    <img src="https://irp-cdn.multiscreensite.com/a055db6d/dms3rep/multi/EOS-Model.png"/>
  </div>
  <div class="col-md-6">
    <img src="https://m.media-amazon.com/images/I/81bziOVlOwL._SL1500_.jpg" width="275"/>
  </div>
</div>

[https://sprwb.pro/3SXMrTP](https://sprwb.pro/3SXMrTP)

Note:
- If you don't have a great handle on your business processes, systems, and people, I highly recommend reading Traction by Gino Wickman.
- This isn't a requirement for this course - nor is it the only way to run a business - but it is as straightforward a framework as I've found for getting your business in order. (and I went to business school) - especially if your business is < $50-$100M in revenue.
- It will be difficult to do an AI transformation if you don't have vision, data, and processes in place. It will also require your people to adapt, so you need to make sure you've got the right people to make this happen.

---

# Ready? Let's get started!