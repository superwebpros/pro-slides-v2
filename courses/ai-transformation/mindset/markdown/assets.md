# Does your company employ knowledge workers who do valuable things?

;;;

## What would happen if that person left?

;;;

## Do you own that knowledge? Is it really your asset?

note:
- Did you foot the bill for it?

---

# Quick Story

note:
- Marjan
- Ahmed

---

## Employees learn things AND that learning becomes their asset

note:
- If we're doing a good job, our employees leave with more valuable skills and knowledge than they had when they started working with us.
- But we're also investing in them. We're paying them to learn. And then they leave.

;;;

## Unless we capture it

note:
- Then it becomes a _shared_ asset. 
- We get a return on our investment and the employee gets upskilled; allowing them to be more valuable in the future - to you - or another company.

---

# Data is how we capture the asset
- Structured data
- Unstructured data

note:
- Structured: database, crm, etc.
- Unstrucutred: emails, voice memos, logs, etc.

;;;

## Knowledge + Data = Backbone of AI transformation

;;;

### Knowledge capture is how you
1. Generate a long-term return on your investment in human capital
2. Augment your existing staff
3. Reduce the cost of hiring new staff

---

# How to capture knowledge
1. Have a robust, well-documented knowledge capture system
2. Make it easy to learn & use
3. Reinforce the habit

note:
- knowledge capture can happen many ways
- we default to text/data entry, but there are many more interfaces

;;;

## Examples

![SuperWebPros Knowledge Base](https://s3.amazonaws.com/resources.superwebpros.com/images/pro-knowledge-base.png)

We use a self-hosted version of [discourse.org](https://www.discourse.org/)

;;;

## Examples

![Loom.com](https://s3.amazonaws.com/resources.superwebpros.com/images/screenshot-loom.png)

We also use [loom.com](https://loom.com/) for real-time video capture

;;;

## Examples

![Otter.ai](https://s3.amazonaws.com/resources.superwebpros.com/images/otter-ai.png)

and [otter.ai](https://otter.ai/) for (some) transcriptions

;;;

## Your toolkit may be different
The point is to have a system that allows you to capture knowledge in a reusable format.

---

# Mindset #1
As the principle investor in your team's knowledge, you should own a stake in it. You do that via data capture.




