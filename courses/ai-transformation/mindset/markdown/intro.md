# Technology isn't the biggest challenge. Culture is.

Note:
- People
- Their mindsets

;;;

## Culture is influenced by how we think about the world

---

# In this module

| From | To |
| --- | --- |
| Knowledge as information | Knowledge as an asset |
| Intuition | Intuition & Data |
| Transactions | Systems |
| Siloed departments | Cross-functional teams |
| Centralized decision-making | Distributed decision-making | 
| Convergent problem solving | Divergent & convergent problem solving |
| Rigid & risk-averse | Agile & experimental |

Note:
- Provide the frame and then the next lessons will go into more detail

;;;

## Preparing to Lead

Link to your Core Values

;;;

### Example: Our Core Values

> A Pro is SHARP

- Super
- Helpful
- Agile
- Responsive
- Proactive & Professional


note:
- If you're the visionary, you're likely also responsible for the culture
- Most successful cultures have strong core values. These are the principles that guide the organization's decisions and actions. To increase adoption, link the new mindsets to your Core Values.
- Example: Agile & Proactive