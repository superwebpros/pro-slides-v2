# Quick story

note:
- Neil Foster

;;;

## We had concrete data, but also a strong intuition

note:
- Both matter
- Our experience with a lot of businesses is that they don't manage their data well. They don't have a good way to capture it, store it, and use it to make decisions.
- In our example, we had tracking on individual performance metrics baked into our systems and data-driven folks like myself could query it

---

# The "Problems" with data
1. Not recognizing how much data there is
2. Too much to track
3. No clear data management processes
4. No capacity to analyze and interpret it
5. Data not connected to business outcomes

note:
- In the "Organizing for AI" module, we'll talk about how to organize your data and your team to make the most of it.
- We'll also talk about how to use AI help with analysis and interpretation

---

# For now...let's talk about outcomes

note:
- for those of you on EOS, your Scorecard is a great place to start

;;;

## What are the key drivers of your business?

- Sales
- Marketing
- HR
- Customer satisfaction
- Productivity
- Cycle time
- Inventory levels
- etc.

;;;

## What are the outcomes you want to achieve?

| Driver | Outcome |
| --- | --- |
| Sales | $1,000,000 new business/rep |
| Marketing | 100 new leads/week |
| HR | 10 new hires/month |
| etc| ... |

;;;

## What are the inputs to those outcomes?

| Driver | Outcome | Input |
| --- | --- | --- |
| Sales | $1,000,000 new business/rep | 100 calls/day |
| Marketing | 100 new leads/week | 1,000 page visits |
| HR | 10 new hires/month | 100 applicants/month |
| etc| ... | ... |

note: 
- leading indicators lead to the outcome, which is a lagging indicator

;;;

## How is the information tracked?

| Driver | Outcome | Input | Tracking |
| --- | --- | --- | --- |
| Sales | $1,000,000 new business/rep | 100 calls/day | Dialpad dials, Hubspot notes, split-test results |
| Marketing | 100 new leads/week | 1,000 page visits | Google Analytics, Google Ads, split-test results |
| HR | 10 new hires/month | 100 applicants/month | Indeed postings, Google Analytics |
| etc| ... | ... |

;;;

## Who is responsible for tracking it?

| Driver | Outcome | Input | Tracking | Owner |
| --- | --- | --- | --- | --- |
| Sales | $1,000,000 new business/rep | 100 calls/day | Dialpad dials, Hubspot notes, split-test results | Jesse |
| Marketing | 100 new leads/week | 1,000 page visits | Google Analytics, Google Ads, split-test results | Bernadette |
| HR | 10 new hires/month | 100 applicants/month | Indeed postings, Google Analytics | Cynthia |
| etc| ... | ... | ... | ... |

---

# Example

![Metabase screenshot](https://s3.amazonaws.com/resources.superwebpros.com/images/example-metabase-dashboard.png)

We use [metabase.com](https://www.metabase.com/) to track our key metrics. It's open source & connected to our databases so that it updates in real-time.

;;;


## It's important that owners know what data you will hold them accountable for
In your weekly meetings, you should follow up on the data that you're tracking.

note:
- Ideally use a dashboard that pulls in the data from the various sources
- In future modules, we'll talk about how to automate this process

---

# Mindset #2
Intuition, augmented by data is a powerful combination. Use metrics to evaulate key business outcomes. And both to make decisions.