## 1. Gather examples

---

## 2. The abstraction cycle
- Abstracting the example
  - Generating a prompt
  - Testing the prompt
- Abstracting the evaluator
  - Generating a rubric
  - Testing the rubric
- Testing the model

;;;

## The Abstraction Cycle
1. Frame
2. Abstract
3. Refine
4. Test

;;;

> Note that gpt-4o-mini is a great model for this.

---

## 3. Updating ProHQ
- Adding the fields we need
- Making the fields conditional
- Creating a Workflow
- Configuring n8n to receive the workflow

---

## 4. Generating the Agentic Framework for Post Creation
- Overview of an Agentic Framework
- Scaffolding the workflow
- Scaffolding the tool agents

---

## 5. Updating the Tool Agents
- Updating the Coordinator
- Configuring System Prompts
- Configuring User Prompts
- Configuring the Supervisor
- Passing Variables to the Tools
- Passing Data to ProHQ
- Logging the Data
- Testing the Tools

---

## 6. Generating the Agentic Framework for Post Evaluation
- Overview of an Agentic Framework
- Scaffolding the workflow
- Scaffolding the tool agents

---

## 7. Wrap up
