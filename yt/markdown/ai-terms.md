# What we'll cover

<ol>
  <li class="fragment fade-in-then-semi-out">Some key terms/components you should know about</li>
  <li class="fragment fade-in-then-semi-out">An example from our business</li>
<li class="fragment fade-in-then-semi-out">How to think about AI in your business</li>
</ol>

---

# 1. Request/Response Cycle

;;;

<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <h3>Request</h3>
      <img src="https://s3.amazonaws.com/resources.superwebpros.com/images/graphic_man-ordering-pizza-doodle.png" alt="Request/Response Cycle" width="100%">
    </div>
    <div class="col-md-6">
        <h3>Response</h3>
        <img src="https://s3.amazonaws.com/resources.superwebpros.com/images/graphic_person-delivering-pizza-doodle.png" alt="Request/Response Cycle" width="100%">
    </div>
  </div>
</div>

;;;

<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <h3>Request</h3>
      <img src="https://s3.amazonaws.com/resources.superwebpros.com/images/graphic_man-ordering-pizza-doodle.png" alt="Request/Response Cycle" width="100%">
    </div>
    <div class="col-md-6">
        <h3>Clients send requests to servers</h3>
        <ul>
            <li>Browser</li>
            <li>Watch</li>
            <li>Smartphone (App)</li>
            <li>Desktop App</li>
            <li>Smart Speaker</li>
            <li>...etc</li>
        </ul>
    </div>
  </div>
</div>

;;;

<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <h3>Response</h3>
      <img src="https://s3.amazonaws.com/resources.superwebpros.com/images/graphic_person-delivering-pizza-doodle.png" alt="Request/Response Cycle" width="100%">
    </div>
    <div class="col-md-6">
        <h3>Servers send response</h3>
        <pre>
            <code data-trim>
                <div class="card">
                    <h2 class="name">John Doe</h2>
                    <p class="info">Age: 30</p>
                    <p class="info">Address: 123 Main St</p>
                    <p class="info">Phone: 555-555-5555</p>
                    <div class="order">
                        <h3>Order Details</h3>
                        <p>Pizza: Pepperoni</p>
                        <p>Size: Large</p>
                        <p>Price: $12.99</p>
                    </div>
                </div>
            </code>
        </pre>
    </div>
  </div>
</div>

---

# 2. Servers

;;;

<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <img src="https://s3.amazonaws.com/resources.superwebpros.com/images/graphic_pizzeria-exterior-doodle.png" alt="Request/Response Cycle" width="100%">
    </div>
    <div class="col-md-6">
        <h3>Servers host applications</h3>
    </div>
  </div>
</div>

;;;

<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <h3>Inside servers</h3>
      <img src="https://s3.amazonaws.com/resources.superwebpros.com/images/graphic_floor-plan-kitchen.png" alt="Request/Response Cycle" width="100%">
    </div>
    <div class="col-md-6">
        <h3>Host:</h3>
        <ul>
            <li>Web apps</li>
            <li>Databases</li>
            <li>Process runners</li>
            <li>...etc</li>
        </ul>
    </div>
  </div>
</div>

;;;

<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <h3>Applications</h3>
      <img src="https://s3.amazonaws.com/resources.superwebpros.com/images/graphic_floor-plan-kitchen.png" alt="Request/Response Cycle" width="100%">
    </div>
    <div class="col-md-6">
        <h3>The "Cooking" Area</h3>
        <ul>
            <li>Process requests</li>
            <li>Implement business logic</li>
            <li>Run algorithms</li>
            <li>...etc</li>
        </ul>
    </div>
  </div>
</div>

;;;

<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <h3>Databases</h3>
      <img src="https://s3.amazonaws.com/resources.superwebpros.com/images/graphic_floor-plan-kitchen.png" alt="Request/Response Cycle" width="100%">
    </div>
    <div class="col-md-6">
        <h3>Storage</h3>
        <ul>
            <li>Dry goods</li>
            <li>Refridgerated Goods</li>
            <li>Equipment storage</li>
            <li>...etc</li>
        </ul>
    </div>
  </div>
</div>

Note: 
- Different items may require different types of storage

---

# Recap

<div class="container-fluid">
  <div class="row">
    <div class="col-md-3">
      <h4>Request</h4>
      <img src="https://s3.amazonaws.com/resources.superwebpros.com/images/graphic_man-ordering-pizza-doodle.png" alt="Request/Response Cycle" width="100%">
    </div>
    <div class="col-md-3">
        <h4>Response</h4>
        <img src="https://s3.amazonaws.com/resources.superwebpros.com/images/graphic_person-delivering-pizza-doodle.png" alt="Request/Response Cycle" width="100%">
    </div>
    <div class="col-md-3">
        <h4>Servers</h4>
        <img src="https://s3.amazonaws.com/resources.superwebpros.com/images/graphic_pizzeria-exterior-doodle.png" alt="Request/Response Cycle" width="100%">
    </div>
    <div class="col-md-3">
        <h4>Inside servers</h4>
        <img src="https://s3.amazonaws.com/resources.superwebpros.com/images/graphic_floor-plan-kitchen.png" alt="Request/Response Cycle" width="100%">
    </div>
  </div>
</div>

;;;

## What does this have to do with AI?

note:
- If you were running AI on your servers, this would be the general framework for how things would work
- Albeit a very simplified version
- For most people, this isn't how they will interact with AI

---

# 3. External Services

;;;

## Let's imagine...

<img src="https://s3.amazonaws.com/resources.superwebpros.com/images/graphic_man-filling-up-gas-doodle.png" width="500"/>

note:
- Something outside the pizza shop (server) that is needed to complete the job

---

# 4. APIs

;;;

## APIs are how machines talk to each other

;;;

<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <h3>Previous Example</h3>
      <img src="https://s3.amazonaws.com/resources.superwebpros.com/images/graphic_person-delivering-pizza-doodle.png" alt="Request/Response Cycle" width="100%">
    </div>
    <div class="col-md-6">
        <h3>How machines parse requests</h3>
        <pre>
            <code data-trim>
                {
                    "name": "John Doe",
                    "age": 30,
                    "address": "123 Main St",
                    "phone": "555-555-5555",
                    "order": {
                        "pizza": "pepperoni",
                        "size": "large",
                        "price": 12.99
                    }
                }
            </code>
        </pre>
    </div>
  </div>
</div>

---

# Example

;;;

## Extracting Keywords from Customer Response & Drafting a Blog Post

![example workflow](https://s3.amazonaws.com/resources.superwebpros.com/images/example_request-response-cycle.png)

---

# Just the tip of the iceberg...

;;;

## Things to consider

1. Data quality
2. Data storage
3. Privacy considerations
4. Which algorithms to use
5. Prompt engineering
6. Algorithm training
7. Model development
8. Build or Lease?

---

# The hardest question
How can I use AI to grow _my_ business?

;;;

## The answer is different for everyone

1. Tasks that require decision-making, but not continuous learning
2. Tasks that might get more efficient with a "co-pilot"
3. Tasks that require more information than a human can process
4. Tasks where the interface is the bottleneck
5. Tasks where integrations yield more complete results

note:
- document handling
- some aspects of customer service

;;;

# Not sure? Talk to a Pro!


