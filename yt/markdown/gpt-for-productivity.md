## Agenda

1. What is GPT?
2. Productivity Hacks
3. Supercharged Operational Hacks
4. The Future

---

## What is GPT?

GPT-3 is a language model that uses deep learning to produce human-like text. It takes in a prompt, and attempts to complete it.

;;;

### GPT is NOT Magic

It's math. It uses tokens and probabilities to predict the next word in a sequence given the previous words.

;;;

### The Key to GPT: Good Prompts

As a math/language model, GPT is only as good as the prompt you give it. The clearer the prompt, the better the output.

;;;

## Accessing GPT

https://chat.openai.com

---

## Productivity Hacks

;;;

### 1. Summarize Custom Text

> Please summarize the request in the following email. Then provide a list of items 'to dos' based on the email: {{ $json["body"][0]["bodyPlain"] }}

;;;

### 2. Generate Email Replies

> Based on the previous message and your summary above, please draft a reply to the customer. The previous message was: {{ $('Webhook').item.json["body"][0]["bodyPlain"] }}.

;;;

### 3. Summarize Public Information (Before 2021)

> Do you know the book "Made to Stick" by Chip and Dan Heath? What does their SUCCESS acronym mean again?

[Example: Lencioni "Death by Meeting"](https://chat.openai.com/share/021efe80-4f2a-4b21-a7c2-658dfaef8181)

;;;

### 4. Write Meeting Minutes

> Hello there, can you please help me create meeting minutes from these notes?

https://chat.openai.com/share/81d85fba-e67f-41a7-956e-34e0732eb886

;;;

### 5. Write "Difficult" Emails Quickly

> Help me write an email to a difficult customer explaining that we need to increase rates because they have scope creeped.

https://chat.openai.com/share/9efca694-f4ec-452a-874c-8571fbe01d27

Note:
- Use "tone" to tweak the response.

;;;

## 6. Write SEO Meta Descriptions & Social Media Posts

> Can you write an SEO Meta description for an article? Here's the summary: "Optimizing a landing page for conversions is a process. It involves researching the right offer, putting together a conversion strategy, designing the page, turning on conversion reporting, copywriting, and follow up. All these things help make sure people click on the offer and convert. It takes time, discipline, and structure, and data is important to know if it is working or not." The result needs to be 100 characters

https://chat.openai.com/share/f7af1344-e6a1-40df-8c15-f8db747339b4

;;;

## 8. Write Job Descriptions & Letters of Recommendation

> Please help me write a letter of recommendation for Courtney Man. She is applying to University of California Irvine's Master's of Human-Computer Interaction and Design. Previously, she was an intern with me in ui and ux when she was an undergrad and directly after graduation. What information would you need from me to help you write a great letter?

[UI/UX Interview Questions](https://chat.openai.com/share/67ee6c79-a645-4b37-afb9-da973327e635)

;;;

## 9. Use as a Substitute for Google

1. [Adobe Illustrator Automation](https://chat.openai.com/share/26a6efff-58e8-4540-8378-9001570b9b87)
2. [Creating Google Docs Template](https://chat.openai.com/share/435d76d5-2dca-4873-8177-f1a1923e9b22)

---

## Supercharged Operational Hacks

1. Automation
2. API Integration
3. AI

;;;

## Examples

1. [Summarize Emails, Todos, and Replies](https://n8n.superwebpros.com/workflow/1)
2. [Extract Keywords & Write Blog Posts](https://n8n.superwebpros.com/workflow/4)

---

## The Future

1. Maxmium Efficienty: API + Automation + AI
2. Best Quality: Your data + Language Models
3. Biggest Risks: Watch out for hallucinations