## The Asset Cleanup Plugin

---

## What it does

Removes unused CSS and JS files from your website

;;;

## Why it matters