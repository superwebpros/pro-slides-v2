# 1. Sign up for Feedly to monitor industry news

note:
- Feedly demo

---

# 2. Identify a relevant, high-ranking post from a "content" competitor

note:
- Angi may be a content competitor

---

# 3. Extract relevant queries from the post

Here is an article, can you please generate a list of queries that might result in this article showing up in search. I want CSV of queries and SEO keywords: https://blog.hubspot.com/marketing/ai-insights-for-new-year

note:
- Keywords, not queries
- Put into a Google Sheet

---

# 4. Generate an outline based on key takeaways from the post

We want to write a post on on "What is a customer journey map?". The top results in search right now are these posts: **{insert posts}** Can you please review each post, summarize the main ideas and takeaways for each one, and then generate an outline for me that incorporates the most relevant points from the combination of articles?

note:
- Take the highest performing posts
- Ask GPT to extract the summaries and takeaways
- You still need to read the posts

---

# 5. Generate a full content brief

This is helpful. Please create a content brief for a content creator to go ahead and produce this as a blog post. And also a youtube video

note:
- Use this to get a contour of your content
- Add your own mix to the content

---

# 5. Produce the content

- DO NOT copy/paste from ChatGPT. 
- Add your own angle & perspective

---

# Pitfalls
- It seems to do better with steps vs all at once <!-- .element: class="fragment fade-in-then-semi-out"-->
- Search volume? <!-- .element: class="fragment fade-in-then-semi-out"-->
- Competitive Difficulty? <!-- .element: class="fragment fade-in-then-semi-out"-->			
- Different SERP Results? <!-- .element: class="fragment fade-in-then-semi-out"-->
- On-page optimizations <!-- .element: class="fragment fade-in-then-semi-out"-->
- Backlinking <!-- .element: class="fragment fade-in-then-semi-out"-->
- Performance tracking <!-- .element: class="fragment fade-in-then-semi-out"-->