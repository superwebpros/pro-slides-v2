# Principles

1. Enhance people, productivity, processes <!-- .element: class="fragment fade-in-then-semi-out"-->
2. Drive quantifiable results <!-- .element: class="fragment fade-in-then-semi-out"-->
3. Prefer Open Source <!-- .element: class="fragment fade-in-then-semi-out"-->
4. Data is an asset <!-- .element: class="fragment fade-in-then-semi-out"-->

note: 
- Results = better, faster, cheaper
- Results = decision-making
- if it's digital it's data

---

# Last Year

1. APIs
2. AI
3. Automation

---

## Results

GT 2x nearly every relevant metric  

...and growing <!-- .element: class="fragment fade-in-then-semi-out"-->

;;;

## More importantly...

Results for customers as we start to help them with process automation & AI integrations

note:
- Often in some way integrated with web

;;;

## Workhorses

5. Baserow <!-- .element: class="fragment fade-in-then-semi-out"-->
4. Assembly AI <!-- .element: class="fragment fade-in-then-semi-out"-->
3. ChatGPT Plus <!-- .element: class="fragment fade-in-then-semi-out"-->
2. OpenAI API <!-- .element: class="fragment fade-in-then-semi-out"-->
1. n8n <!-- .element: class="fragment fade-in-then-semi-out"-->

;;;

## Not to mention ProHQ...

---

## One Surprising Technology...

Discourse Knowledge Base

---


# Implementing in 2024

;;;

## Evolving, not replacing

Note:
- What we've done has been successful. We'll do more
- Go to next level
- Not industry-specific

;;;

## Our Roadmap

1. API-first, "headless" websites <!-- .element: class="fragment fade-in-then-semi-out"-->
2. Digital Asset Management <!-- .element: class="fragment fade-in-then-semi-out"-->
3. Low-code Tools for non-developers <!-- .element: class="fragment fade-in-then-semi-out"-->

note:
- Data Management Storage

;;;

## Headless CMS

;;;

## Digital Asset Management

<div class="mermaid">
    <pre>
        %%{init: {'theme': 'dark', 'themeVariables': { 'darkMode': true }}}%%
        flowchart TD
            A[Data Management] --> B[Storage]
            A --> C[Extract, Load, Transform]
            A --> D[Visualization]
            B --> E[Relational Databases]
            B --> F[BigQuery]
            B --> G[Vector Databases]
            C --> Airbyte
            C --> n8n
            D --> Metabase
    </pre>
</div>

;;;

## Low Code Tool(s)
- Budibase

---

## Technology is only one part of the equation
