## November 2022

<p style="text-align:center;">The world changed.</p>

<img src="https://s3.amazonaws.com/resources.superwebpros.com/bb/image/general/superwebpros_funko_style_female_cyborg_with_yellow_hair_and_a_0a265a76-0120-40c9-94d4-d40577615c96_2.png" style="width: 400px; display:inline-block;"/>


;;;

## It is not the strongest of the species that survive, nor the most intelligent, but the one most _responsive to change._  
Charles Darwin, _The Origin of Species_

;;;

Darwin's observation today:

> "AI Won’t Replace Humans — But Humans With AI Will Replace Humans Without AI"  

_Karim R. Lakhani, Co-Director of the Laboratory for Innovation Science at Harvard Business School_, author [Competing in the Age of AI](https://www.amazon.com/Competing-Age-AI-Leadership-Algorithms/dp/1633697622/ref=asc_df_1633697622/)

---

# The Big Idea:

## We need to begin AI adaptation & augmentation now, or we will be left behind.

---

## Today's Roadmap

- ✅ 30,000 foot view of how AI is being used & what results are being seen  
- ✅ Provide a framework for thinking about AI in your business  
- ✅ Provide examples of how we're using AI in our business  

Note:
- Focus on Generative AI

;;;

## Not Covered

- ❌ non-Generative AI
- ❌ Security
- ❌ Legal
- ❌ Privacy
- ❌ Bias

Note:
- Not because not important, but because only so much time.
- Open questions that very smart people still working on/disagree about.
- Things are moving very fast, so it needs to be on your radar.

;;;

## The "AI Starter Bundle" 

[https://www.superwebpros.com/resources/ai-starter-bundle/](https://www.superwebpros.com/resources/ai-starter-bundle/)

Download our free starter bundle and get:

1. Our 1 page AI growthmap
2. 100+ AI Prompts you can use to start using ChatGPT productively today
3. AI Glossary - The most important terms to know when talking about AI

;;;

## The 4 Stages of the Growthmap

1. Explore & Experiment: AI as Novelty
2. Educate & Communicate: AI as Collaborator
3. Integrate & Automate: AI in Infrastructure
4. Innovate: AI as Disruptor


---

## AI was already here

- You're _already_ using it (Siri, Alexa, Canva)
- Search Engines
- Social Media
- Digital Advertising
- Productivity Tools: Loom, Otter.ai, Notion, etc.
- And more...

Note:
- Expect to see more products have it embedded (Office, Google Suite, Notion, etc.)
- Example: Code/auto complete in my IDE

;;;

## What Changed?

1. Interface - It became super easy to use
2. Accessibility - It can be used with multiple types of data, by anyone
3. Integration - It can be integrated into other tools
3. Adoption - As a result, it's able to be used waaay more

Note:
- ChatGPT changed the interface in such a way that billions of people can now use it. It's put pressure on search, for instance.
- They also released an API, which provides programmatic accessibility, so applications can be built on top of it (or augmented with it)
- Adoption has increased because as accessibility increases, infrastructure cost of implemetnation is much lower than it used to be.
- Additionally, open source algorithms reduce the need for generalized data scientists

---

## In general, how was AI being used?

| Use Case | What it does | Percent of Respondents |
| -------- | ------------ | ---------------------- |
| Robotic Process Automation | Automates repetitive tasks | 40% |
| Computer Vision | Analyzes images and video | 34% |
| **Natural Language Processing** 📈 | Analyzes text and speech | 31% |

_Source: McKinsey, [The State of AI in 2022](https://www.mckinsey.com/capabilities/quantumblack/our-insights/the-state-of-ai-in-2022-and-a-half-decade-in-review)_

Note: 
- We'll start with enterprise use cases, then come 'down market'

;;;

## Adoption is happening at the business unit

![Chart showing increasing use cases in the business units](/assets/chart_ai-use-case-at-business-unit.png)

Note:
- Idea is that AI is being released incrementally, not all at once.

;;;

### What are the top AI use cases?

![Chart showing top AI use cases](/assets/chart_ai-adopted-use-cases.png)

;;;

### What impacts are companies seeing?

![Chart showing top AI impacts](/assets/chart_ai-value-creation.png)

Note:
- Benefits to cost reduction in a potentially recessionary environment is a big deal.

---

## Leaders are growing faster than laggards are implementing

[McKinsey Report](https://www.mckinsey.com/capabilities/quantumblack/our-insights/the-state-of-ai-in-2022-and-a-half-decade-in-review)

- 25% of respondents attribute > 5% of EBIT attributable to AI investments
- 8% of respondents attribute > 20% of EBIT attributable to AI investments
    - Those 8% are getting to 20% faster than the 25% are getting to 5% or the laggards are starting.

Note:
- The companies making the investments are seeing bigger results, faster
- The 'toe dippers' are seeing some benefits, but not necessarily has huge. I'd wager that's changing now.

;;;

## Investments are increasing substantially

Percent of enterprises with > 5% of tech budget allocated to AI

| Year | % Respondents |
| ---- | ---------------------- |
| 2018 | 40%                    |
| 2022 | 52%                    |
| 2025 | 63%                    |

---

## We're  currently beating all of those benchmarks, with no signs of slowing down.

;;;

## But there were real challenges getting there.

;;;

### How AI Strategy is being implemented

![Chart showing AI strategy implementation](/assets/chart_ai-strategy.png)

Note: 
- People Problem

;;;

### Data management is a problem

![Chart showing data management problems](/assets/chart_ai-data-health.png)

Note:
- Data Problem

;;;

### Knowing how to set up models & pipelines is a problem

![Chart showing model & pipeline problems](/assets/chart_ai-using-models-problem.png)

Note:
- Technical Problem

;;;

### Talent is a problem

![Chart showing talent problems](/assets/chart_ai-labor-problem.png)

Note:
- Labor/Talent Problem

;;;

### How are organizations solving this problem?

![Chart showing how organizations are solving the problem](/assets/chart_ai-how-talent-is-being-formed.png)

---

# Practical Roadmap

;;;

## 4 Phases

1. AI as a Novelty - Explore & Experiment
2. AI as a Collaborator - Educate & Communicate <!-- .element: class="fragment" -->
3. AI in Infrastructure - Integrate & Automate <!-- .element: class="fragment" -->
4. AI as a Disruptor - Innovate <!-- .element: class="fragment" -->

Note:
- Will dive into each one of these, highlighting the opportunities, challenges, resources, and skills required for each.

---

## Phase 1: AI as a Novelty

Goal: Explore & Experiment

Use off the shelf tools to start exploring how AI might be useful in your business (for your team, operations, and/or customers)

;;;

| | | 
| --- | --- |
| **Biggest Opportunities** | Exploring the breadth of use-cases for off-the-shelf LLM/AI models for content-generation | 
| **Biggest Challenges** | LEARNING to write good prompts <br/> IMAGINING potential use cases <br/> PATIENCE with the limitations <br/> WILLINGNESS to iterate  |
| **Resources** | ChatGPT <br/> MidJourney / DALL-E <br/> "Normal" Office suite tools <br/> OTS Apps (Otter.ai) |
| **Skills Required** | Creativity <br/> Agile mindset <br/> Persistence |

;;;

## [SuperChat Demo](https://chat.superwebpros.com/c/new)

---

## Phase 2: AI as a Collaborator  

Goal: Educate & Communicate  

Create and use _shared tools_ to use AI across people, teams, and functions. Start to train people on how to use AI in their workflows. Gather examples. Lots of them.

;;;

| | | 
| --- | --- |
| **Biggest Opportunities** | Incorporate AI into your content creation & information management workflows. Use it to improve communication and knowledge management for yourself and team. | 
| **Biggest Challenges** | Building a habit of using the tools; team skepticism <br/> [Generation gaps](https://chat.openai.com/share/4b0af427-bcbb-4424-ad53-26280e23610c) in using tools <br/> Shifting to a <b>'data as an asset'</b> mindset <br/> Putting discipline around data management |
| **Resources** | Database(s) <br/> Knowledge Base / Knowledge Management <br/> Content Repository <br/> Data Storage (e.g., Object storage) |
| **Skills Required** | Knowledge Management / Organization <br/> Content Management <br/> Model Training |

;;;

## Demos

1. [The SuperWebPros Knowledge Base](https://kb.superwebpros.com/)
2. [The SuperWebPros Content Repository](https://br.superwebpros.com/database/161/table/726/3017)
3. [SuperChat Shared Prompt Library](https://chat.superwebpros.com)
3. [ProHQ](https://app.superwebpros.com/)

Note:
- [Template repository with pre-written customer emails/responses](https://br.superwebpros.com/database/161/table/731)
- [Improved training manuals/documentation](https://kb.superwebpros.com/t/wordpress-http-api-methods/369)
- [Team Learning & Training](https://chat.openai.com/share/849a650a-7c1d-44d6-b5a7-8ba5b1ff3450)
- [Help using software (Google Sheets, Excel, Photoshop, etc)](https://chat.openai.com/share/3073158b-3f1f-4f42-a1fb-0f25b71e8a48)

---

## Phase 3: AI in Infrastructure

Goal: Integrate & Automate  

Integrate AI into your operations and business process management software (ERP, CRM, etc)

;;;

| | | 
| --- | --- |
| **Biggest Opportunities** | Add natural language interfaces to your data and workflows | 
| **Biggest Challenges** | - Identifying processes that could benefit from AI implementation <br/> - Capturing raw data <br/> - Process optimization & development <br/> - Change management |
| **Resources** | - API infrastructure (n8n, Lambda, REST, etc) <br/> - ETL Tools, - Data Warehouses, etc. <br/> - Cloud processing <br/> - Vector Databases <br/> - LLM Libraries, AI Libraries |
| **Skills Required** | - Data management (ex: cleaning, pipelines) <br/> - API Development <br/> - No/Low Code App Building Tools |

;;;

## Demo

1. [SWP Transcripts](https://app.superwebpros.com/#Transcript)
2. [SWP Vector Store](https://q.superwebpros.com/dashboard)
3. [SWP Transcript to Vector Store Workflow](https://n8n.superwebpros.com/workflow/JKasNdycq9Qrul0B)
4. [SWP AI Agents in SuperChat](https://chat.superwebpros.com/c/new)

Note:
- [Email Summaries & Task Lists](https://n8n.superwebpros.com/workflow/1)
- Create a natural-language interface for your data
- Aggregate multiple databases into an embed store to cross-query data

---

## Phase 4: AI as a Disruptor

Goal: Innovate

Build industry-specific apps on top of AI APIs and cloud infrastructure

;;;

| | | 
| --- | --- |
| **Biggest Opportunities** | Build niche-specific products to sell to your industry. | 
| **Biggest Challenges** | Depends on the product |
| **Resources** | Completely depends on what you're building |
| **Skills Required** | Software development <br/> Data science/model building |

;;;

## Demo

1. [SWP Label Studio](https://ls.superwebpros.com/projects/3/data?tab=8&task=786)
2. [SWP Copywriting Agent](https://app.superwebpros.com/#CCopyPrompt/view/66a296320fad8d802) & [Script Evaulation](https://app.superwebpros.com/#CVideoScript/view/66a520fbcf9cb17b9)
3. [Video Script Evaluator Workflow](https://n8n.superwebpros.com/workflow/wuSlAWvH5F8dLc83)
4. [SuperChat assistants](https://chat.superwebpros.com/c/new)

---

# Where to Go Next?

;;;

## Getting Started

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <h4>Download our FREE AI Starter Bundle</h4>
            <img style="width: 200px;" src="https://s3.amazonaws.com/resources.superwebpros.com/images/qr-codes/ai-starter-bundle-500.png"/>
            <caption><a href="https://sprwb.pro/3t9fitG">https://sprwb.pro/3t9fitG</a></caption>
        </div>
        <div class="col-md-6">
            <h4>Have a Project in Mind?</h4>
            <img style="width: 200px;" src="https://s3.amazonaws.com/resources.superwebpros.com/images/qr-codes/ai-application-500.png"/>
            <caption><a href="https://sprwb.pro/3Zx9lmv">https://sprwb.pro/3Zx9lmv</a></caption>
        </div>
    </div>
</div>

;;;

<img style="width: 200px;" src="https://www.superwebpros.com/wp-content/uploads/2021/04/cropped-SWP-logo-transparent-cropped-1024x226.png" alt="">

# Thank You!

Reach me: <a href="mailto:jesse@superwebpros.com">jesse@superwebpros.com</a>


