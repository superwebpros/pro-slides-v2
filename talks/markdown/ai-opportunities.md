

## Enterprise Budgets are Growing
Percent of enterprises with > 5% of tech budget allocated to AI

| Year | Percent of Respondents |
| ---- | ---------------------- |
| 2018 | 40%                    |
| 2022 | 52%                    |
| 2025 | 63%                    |


---

## How is AI being used?

| Use Case | What it does | Percent of Respondents |
| -------- | ------------ | ---------------------- |
| Robotic Process Automation | Automates repetitive tasks | 40% |
| Computer Vision | Analyzes images and video | 34% |
| Natural Language Processing | Analyzes text and speech | 31% |


---

## Adoption is happening at the business unit

![Chart showing increasing use cases in the business units](/assets/chart_ai-use-case-at-business-unit.png)

;;;

### What are the top AI use cases?

![Chart showing top AI use cases](/assets/chart_ai-adopted-use-cases.png)

;;;

### What impacts are companies seeing?

![Chart showing top AI impacts](/assets/chart_ai-value-creation.png)

---

## Leaders are growing faster than laggards are implementing

[McKinsey Report](https://www.mckinsey.com/capabilities/quantumblack/our-insights/the-state-of-ai-in-2022-and-a-half-decade-in-review)

- 25% of respondents attribute > 5% of EBIT attributable to AI invesments
- 8% of respondents attribute > 20% of EBIT attributable to AI invesments
    - Those 8% are getting to 20% faster than the 25% are getting to 5% or the laggards are starting.

;;;

### How AI Strategy is being implemented

![Chart showing AI strategy implementation](/assets/chart_ai-strategy.png)

;;;

### Data management is a problem

![Chart showing data management problems](/assets/chart_ai-data-health.png)

;;;

### Knowing how to set up models & pipelines is a problem

![Chart showing model & pipeline problems](/assets/chart_ai-using-models-problem.png)

;;;

### Talent is a problem

![Chart showing talent problems](/assets/chart_ai-labor-problem.png)

;;;

### How are organizations solving this problem?

![Chart showing how organizations are solving the problem](/assets/chart_ai-how-talent-is-being-formed.png)

---

## Implications

1. Chat != AI. AI is much bigger
2. Despite the hype, AI is still in its infancy
3. But growing very quickly - especially at large companies (>50,000 employees)
4. With measureable impacts to both revenue and cost structure
5. Most companies don't have the context or talent to implement AI technologies as quickly as they'd like.

;;;

### Implications

1. The problems are broad and deep. 
2. If we can narrow (or find ways to help narrow) the problem space, we can help companies implement AI technologies faster and become a talent hub.

---

## Next Steps?

1. Conference
2. Upskilling

;;;

### Conference

1. Initially, thinking we needed to identify presenters with knowledge and/or experience in:
    1. Service Operations (e.g., call centers, field service, etc.)
    2. Marketing & Sales
    3. Strategy
    4. Corporate Finance
    5. Product & Service Development
    6. Supply Chain Management
    7. Human Resources
    8. Manufacturing

;;;

### But what if we focused on just one part of this, like Supply Chain Management?

---

## Upskilling: A few paths
Opportunity: APIs & 'off the shelf' tech are making model implementation much easier.

Note:
- Still need data scientists, etc., but most companies are upskilling software engineers and are looking to implement the technical side.

;;;

### Opportunities?

1. Create community program(s) to help people develop the skills to implement AI technologies using low-code or no-code apps.
2. Cross-functional corporate accelerator program to help companies implement AI technologies using low-code or no-code apps and pilot programs.

