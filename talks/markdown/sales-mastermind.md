<!-- Quick Demo Hook -->
## Let's Start with a Quick Win
### [Demo - Self Introduction](https://chat.salescoachescorner.com/c/new)

---

## Today's Journey
- Learn the fundamentals of prompting
- See it applied to a sales process
- Learn how to apply it to your business

---

<!-- Framework Context -->
## AI Growth Framework
1. AI as a Novelty - Use off the shelf tools to start exploring how AI might be useful in your business
2. AI as a Collaborator - Train your team to use the right tools & use AI to facilitate communication internally & externally. <!-- .element: class="fragment" -->
3. AI in Infrastructure - Integrate AI into your operations and business process management software (ERP, CRM, etc) <!-- .element: class="fragment" -->
4. AI as a Disruptor - Build industry-specific apps on top of AI APIs and cloud infrastructure <!-- .element: class="fragment" -->

---

## Why Start with Prompting?
- Foundational skill
- Builds understanding
- Increases tool effectiveness
- Creates flexibility

Note:
- Prompting is step 1 of 1
- It's the foundation of all AI tools and models

---

<!-- Prompting Section -->
## Creating Effective Prompts

1. Get the AI Ready
2. Frame the Agent
3. Ask the Agent to Think 
4. Write a Draft

;;;

### Step 1. Get the AI Ready

I'd like to create a series of AI agents who can help us improve `{{area_to_improve}}` for our business. I may propose some ideas for different kinds of agents and then you and I can discuss the merits of different types and create them. How does this approach sound?

;;;

### Step 2. Frame the Agent

Before we dive into any of those topics, let me share an example of the kind of agent I want to create. I don't want to use the example below, I want to show you a framework for what an effective agent might look like.

```
```ExampleOfAgent
PURPOSE:
This agent helps create clear, compelling, and compliant job descriptions that attract qualified candidates while accurately representing the role and company culture.

INTERACTION STYLE:
- Ask focused questions about the role and requirements
- Suggest improvements and best practices
- Flag potential bias or compliance issues
- Maintain conversational yet professional tone

PROCESS GUIDE:
1. Gather role basics and context
2. Define key responsibilities
3. Identify required and preferred qualifications
4. Incorporate company culture elements
5. Review and refine language
6. Add necessary disclaimers and requirements

INITIAL ENGAGEMENT:
Present these starting points to the user:
A) Start with basic role information
B) Review/update an existing job description
C) Focus on specific sections (responsibilities, requirements, etc.)
D) Address specific concerns (inclusivity, compliance, etc.)
E) Start with industry templates/examples

Wait for user selection before proceeding with relevant questions.
```endExample
```

Reflect back to me your understanding of this agent's construction before we proceed.

;;;

### Ask the Agent to Think
Let's proceed with the `{{assistant_name_from_list}}` Assistant. How do you propose we approach this assistant?

;;;

### Write a Draft
I think this is a good start. Please proceed with a draft of the agent's instructions. Present them as if it were a system message for the agent.

---

## Prompt Creator Template
### [Get it Here](https://s3.amazonaws.com/resources.superwebpros.com/bb/pdf/lead-magnet/prompt-mastery-guide.pdf)

---

<!-- Sales Process AI Opportunities -->
## AI in Your Sales Process
### Quick glimpse of what's possible

---

## Throughout Your Sales Cycle

| Pre-Meeting | Discovery | Solution Development | Negotiation/Close | Account Management |
|-------------|-----------|---------------------|------------------|-------------------|
| Gameplanner | From Problem to Benefit | Value Proposition Builder | Ask for the Sale | Close the File |
| Strong Self Introduction | Uncover Decision Makers | | Objection Handler | |
| Ground Rules | The Money Conversation | | Overcome PDKs | |
| | Wishy Washy Worder | | | |

---

<!-- One-Click Solutions -->
## 1. For Your Team

<div class="container-fluid">
    <div class="row">
        <div class="col-md-9">
            <img src="https://www.dropbox.com/s/mhgzkry0wfgg9k8/CleanShot%202025-01-20%20at%2011.16.51.png?st=bkfy60oc&dl=1"/>
        </div>
        <div class="col-md-3">
            <ol>
                <li>Shared Prompts</li>
                <li>Connect to Your Data</li>
                <li>Use Different Models</li>
            </ol>
    </div>
</div>

;;;

## 2. Off the Shelf Resources to Try

- Meeting Recordings: [Otter.ai](https://otter.ai) & [Fathom](https://fathom.video/)
- Foundational AIs: [Perplexity](https://perplexity.ai), [OpenAI](https://openai.com), [Claude](https://claude.ai), and [Gemini](https://gemini.google.com/app)
- Training & Podcasting - [Notebook LM](https://notebooklm.google/) ([Demo](https://notebooklm.google.com/notebook/14c70489-221b-43f2-812b-90e41ba3d711))
- [List of Resources to Try](https://storage.googleapis.com/resources.superwebpros.com/bb/pdf/ai-resources-to-try.pdf)

Note:
- Real power will come from integrations

---

<!-- Customer Experience -->
## Extending to Customers
### [SmartSites](https://www.superwebpros.com)

---

## Recap

;;;

## AI Growth Framework
1. 👉 AI as a Novelty - Use off the shelf tools to start exploring how AI might be useful in your business
2. 👉 AI as a Collaborator - Train your team to use the right tools & use AI to facilitate communication internally & externally.
3. AI in Infrastructure - Integrate AI into your operations and business process management software (ERP, CRM, etc)
4. AI as a Disruptor - Build industry-specific apps on top of AI APIs and cloud infrastructure

---

<!-- Closing -->
## Next Steps
1. Start with prompting using [prompt creator template](https://s3.amazonaws.com/resources.superwebpros.com/bb/pdf/lead-magnet/prompt-mastery-guide.pdf)
2. Try some of the [off the shelf resources](https://storage.googleapis.com/resources.superwebpros.com/bb/pdf/ai-resources-to-try.pdf)
3. Create a shared repository of prompts & tools for your team
4. Explore connecting AI to your data

---

## Questions?