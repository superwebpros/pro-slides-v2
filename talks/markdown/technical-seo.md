# Who Am I?

<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <h4>Jesse Flores, SuperWebPros</h4>
      <ul>
        <li>Corporate guy turned tech entrepreneur</li>
        <li>Started a software development company</li>
        <li>Practioner of "Inbound Marketing"</li>
        <li>Founder of web development company</li>
      </ul>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box" >
      <img alt="Jesse Flores Bitmoji" src="https://www.dropbox.com/s/115lxlk4golij5k/jesse-flores-bitmoji.jpeg?dl=1"/>
    </div>
  </div>
</div>

---

# Let me start with a secret...

;;;

## For the past few years, I've been really _frustrated_ with digital marketing.

;;;

We'd follow "best practices," but see little-to-no results

;;;

## Why?

---

## Stumbled across this little graphic:
![Content over the past 20 years](https://www.dropbox.com/s/zpj2vz38cifrdg1/internet-over-time.png?dl=1)

;;;

I was good at SEO in 2011

;;;

Who wasn't?

;;;

Today, it's _harder_, there's waaaaay more content.

;;;

And waaay more competition.

;;;

<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <p>I also started to notice that search results were changing. It's not just text anymore.</p>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <img src="https://www.dropbox.com/s/4dmqabd70mpskrh/different-types-of-data.png?dl=1">
    </div>
  </div>
</div>

Note:
Videos, maps, smart snippets, products also returned

---

# It was clear that *search was about way more than "just" keywords or blog posts.*

---

So, I started to dig in the best way I knew how:

By analyzing data <!-- .element: class="fragment" -->

;;;

## And there's a lot of data

;;;

If you know what to look for

---

# What I want to cover today
1. 4 Key Dimensions of Technical SEO to frame technical efforts
2. 6 pieces of "low hanging fruit" you can knock out ASAP
3. 7 tools you need in order to perform a "deep dive" for ongoing SEO & maintenance

---

# What is Technical SEO?
**Website** & **Server** optimizations that help crawlers crawl, index, and return your _information_ when a user asks a Search Engine a question.

Note:
- Google makes it's money when _users_ find it _useful_. Make your site great for users and the rest will take care of itself.
- Information could be pictures, videos, FAQ snippets, etc.

;;;

It's about helping **people** find answers to questions, _in the most effective way possible_, while making it **easy for bots** to crawl, parse, and slot your information for quick retrieval on massive servers & databases.

Note:
- Google is a business
- Crawls aren't free
- Storage isn't free


---

# 4 Key Dimensions of Technical SEO
1. Speed 
2. Security <!-- .element: class="fragment" -->
3. Indexability <!-- .element: class="fragment" -->
4. Usability/Accessibility <!-- .element: class="fragment" -->

---

# What's _NOT_ included in "Technical SEO"
1. Proactive keyword research 
2. Content strategy
3. Backlink building
4. Content promotion

Note:
These things are still important!

---

# 1. Speed
Hugely important - and only becoming more so

Especially for mobile devices <!-- .element: class="fragment" -->

Note:
- Probably a talk by itself
- Google's upcoming index consolidation

;;;

## Speed - The key question
Does your website load fast on a 3G connection with a mediocre smartphone?

;;;

## 2 Major Impediments to speed
1. "Heavy" pages
2. Lots of requests

;;;

## What makes a page "heavy?"

Simply put: how much data needs to be transmitted from the server to the network.

;;;

## Some factors that make pages "heavy"

<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <ul>
        <li>Big images</li>
        <li>Code bloat (often from your CMS, theme, plugins, internal CSS/Javascript)</li>
        <li>Poor (or no) caching policies</li>
        <li>Multiple requests (e.g., Javascript, 3rd party libraries)</li>
        <li>Blocking requests</li>
      </ul>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <img alt="annotated web page showing parsing logic" src="https://www.dropbox.com/s/ya81ul982lvp5zg/annotated-html-code.png?dl=1">
    </div>
  </div>
</div>

Note:
- Browsers parse pages top to bottom and don't move on to line 2 until line 1 is complete

---

# 2. Security
Is your website secure?

;;;

## Facets of Website Security
1. Encrypted connections (SSL)
2. No "Malicious" code
3. No outdated software, themes, or plugins

Note:
- Malicious code could, in fact, not be malicious. Ad tech in particular can give off false positives. Can also drive up advertising costs
- Important to check plugin repositories before usage

---

# 3. Indexability
Is your website crawlable, indexable, and architecturally "clean?"

;;;

## Facets of Indexability
1. XML Sitemaps indexed & synced with Google Search Console
2. Intelligible URL/site structure 
3. Structured data
4. Quality content
5. Few (or no) 404s
6. Clear redirects

;;;

## Facets of Indexability: URL/Site Structure

![Example of a well-constructured URL that's consistent in breadcrumb, title, video, and H1](https://www.dropbox.com/s/81ih6q6t1zyyy52/example-of-url-design.png?dl=1)

Note:
On-page hierarcy also good

;;;

## Facets of Indexability: Quality Content
1. No "thin" content
2. No "duplicate" content
3. "Canonicalized" content (where appropriate)
4. Internal linking (especially to "deep" content)

;;;

# 4. Usability/Accessibility
Is your website usable by a diverse group of users?

;;;

## Facts of Usability/Accessibility
1. Mobile-first
2. Readable fonts
3. Little to no "layout shift"
3. ALT tags to describe images
4. Screen-reader friendly

;;;

Can people use your website without being frustrated?

---

# 6 Common Pieces of "Low Hanging Fruit"
There are different tools to help with SEO audits, but here are some "no-brainers"

;;;

## 1. Don't Use Bloated Images
![Example of 2 images that look the same, but one is much bigger file size](https://www.dropbox.com/s/6xzbyz1m2x0ta8u/CleanShot%202020-12-08%20at%2014.41.37%402x.png?dl=1)

Note:
- Image can look the same, but different format creates a problem

;;;

- Keep images < 500kb
- .webp > .jpg > .png > .gif
- Shrink images before upload
- Consider image shrinking plugins/apps if you have a lot of legacy content

Note:
Problem with webp is browser support

;;;

## 2. Reduce (or better yet, remove) 3rd Party Javascript
- If there isn't a compelling reason to use a 3rd party script, don't use it
- Only load them on the pages that need them
- If they must be used, load them through Google Tag Manager
- If you can't use Google Tag Manager, set up your system to pre-fetch requests

Note:
Pre-fetching allows the browser to request resources silently and store it in its cache.

;;;

## 3. (Re)move Your Javascript
- Remove internal Javascript that's not serving a real purpose
- If possible, move it to the footer
- If possible, load asynchronously so it's not blocking page load

;;;

## 4. Thin out your code
- Don't use heavy themes or plugins
- Minify CSS, Javascript, and HTML where possible

Note:
- There are plugins that help with this

;;;

## 5. Use cache where you can
Caching stores a page on a browser or server, so the page isn't regenerated when requested. This makes pages load faster

Note:
- Optimize plugins
- WP Rocket
- Asset Loader

;;;

## 6. "Cheat" with Cloudflare

<div class="row middle-lg">
  <div class="col-lg-6">
    <div class="box">
      <p><a href="https://www.cloudflare">Cloudflare</a> can manage many of these things for you for free/$20 per month.</p>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="box">
      <img alt="Cloudflare screenshot" src="https://www.dropbox.com/s/k027z4op1dzkwir/CleanShot%202020-12-08%20at%2014.45.03.png?dl=1">
    </div>
  </div>
</div>

---

# Tools To Do Technical Analysis
Technical analysis is part of ongoing SEO maintenance - especially if you have lots of 'cooks in the kitchen.'

Note:
- Cooks = clients, employees

;;;

## Tools
1. [Deepcrawl](https://www.deepcrawl.com/)
2. [Ahrefs](https://www.ahrefs.com)
3. [Google PageSpeed API](https://developers.google.com/speed/docs/insights/v5/get-started)
4. Google Search Console
5. Google Analytics

;;;

## 2 Bonus Tools 
(Especially for larger websites)
1. Google BigQuery Engine <!-- .element: class="fragment" -->
2. Google Data Studio <!-- .element: class="fragment" -->

;;;

## Our Tool
![Grahpic of our website quality audit tool](https://www.dropbox.com/s/47h1562qjqgmzzq/screenshot-of-our-seo-tool.png?dl=1)

;;;

## This isn't for everyone, but it is helpful

---

# Whew!
What we covered:

1. 4 key dimensions of Technical SEO
2. 6 "quick wins" to improve your technical SEO
3. The tools you need to take it to the next level

Note:
This is only the tip of the iceberg

