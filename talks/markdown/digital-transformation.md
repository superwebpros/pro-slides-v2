# Story - Elderly Instruments

![Elderly Instruments Homepage](https://www.dropbox.com/s/szqtytw0ojo2f7y/CleanShot%202020-12-02%20at%2014.32.43.png?dl=1)

;;;

## Backstory
- Founded 48 years ago
- Would buy old guitars and start selling them out of dorm room in Ann Arbor
- Got into mail order in the 80s

;;;

## Backstory
- Business grew
- 90s: "accidentally" got into the Internet at suggestion of employee
- Business grew more; invested in "PICK" technologies infrastructure designed for retailers

;;;

## Backstory
- Hired a local friend who 'learned to code' 
- Hodge-podged systems together
- Connected a PHP website with their backend

;;;

## Backstory - Customer expectations changed
- Customers all over the world
- Amazon, etc. become a thing, so customers want:
  - Faster shipping
  - Better browsing, search, and purchase experiences
  - Self-service online when it made sense

;;;

## Uh Oh....
- Meanwhile, margins squeezed
  - More distribution of products online (Reverb, Fender, Martin)
- Relied on people to do work that software could do
- Sales decrease, costs increase

---

# What to do?

---

# Modernize

---

# But, problem...
- lack of technical know-how
- "legacy" systems
- Not even sure of "what's possible"

;;;

## Still have to operate while they figure it out

Note:
Losing money every day

---

# Solution: Upgrade. Everything.

---

# Upgrading
1. "Backoffice systems" - Support processes
  - Billing, sales, trade-ins, inventory, shipping, etc.
2. Automation of routine process
3. Integration with modern tools
  - Shopify
  - AI-driven email marketing
  - Recommendation engines
  - Shipping carriers, etc.
4. Literally, EVERYTHING

---

## Remember, still have to operate while it's being figured out.

---

## Oh, and losing money while doing it

---

# Quick Digression
Small businesses vs. large businesses

;;;

## Small business is nothing like big business

- Small business = "handshake agreements," "relationships," "craftspeople," very often "pants on fire"
- Large business = "contracts," "transactions," "organization," "automation"

;;;

In two words, **systems and scale**.

---

# What is Digital Transformation?

Help organizations (e.g., _organized businesses_) grow **profits** by implementing systems that grow revenue, improve customer experience, or cut costs.

;;;

## Examples
- Amazon's recommendation engine (grow revenue, improve experience)
- Walmart distribution (cut costs, grow profits)

;;;

## Examples
- Automation
- Machine Learning
- Cloud processing
- Systems integration
- The list goes on

;;;

# Meanwhile, things still have to run

;;;

Exxon can't stop selling gas just because they need to optimize a part of their business

---

# Two parts of this transformation
1. The software that powers the system(s) - Microsoft Dynamics 365, Netsuite, Oracle
2. The people side (define business rules, process management, helping the people who have to do the jobs, etc.)

---

It's a complex process with lots of room for error.

---

And most people don't know how systems work (or even how they _should_ really work)

;;;

Elderly sells instruments. They aren't technologists.

---

Larger companies have their own "Elderlys" inside their organization

;;;

Line managers manage widget production, _not_ deciding how to optimize widget forecasting, design, supply chain, etc.

---

Because this is complex, there is **lots** of room for error

;;;

Companies like Q:
- Identify gaps
- Help business owners/managers articulate requirements
- Help them translate data into the software framework
- Make sure technical gaps are filled & business requirements are translated into systems
- Hold client hands while this happens
- Maybe train staff or come up with a plan for training the staff

---

As a "trusted advisor," the job is to be the brain and security blanket for an implementation that can make - or break - a company

;;;

And impact dozens, hundreds, or thousands of employees

---

# Long story, short
- Big business/organizations need systems to compete
- Those systems are a complex mix of software and people
- Those systems power the business (it's the nervous center of the business)
- Digital transformations like Q does are about upgrading systems while still operating and (hopefully) making them more competitive/profitable in the mix

---

# End Note - Talent recruitment
- Generally, Investment Banks, Software Comapnies (Google, Facebook), Consultants are looking for the same talent pool
- Highly competitive
- Talent _is_ the product and inventory


