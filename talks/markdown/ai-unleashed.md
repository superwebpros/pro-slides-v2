## An American Folktale

<img src="https://s3.amazonaws.com/resources.superwebpros.com/images/midjourney_example_john-henry.png" style="width: 400px"/>

[https://www.midjourney.com/app/jobs/3009524c-4b43-44d5-9eeb-bf5142ea0aca/](https://www.midjourney.com/app/jobs/3009524c-4b43-44d5-9eeb-bf5142ea0aca/) / [GPT Prompt](https://chat.openai.com/share/65957855-8169-40cd-8326-db42b36f2cf5)

---

## Today

<img src="https://s3.amazonaws.com/resources.superwebpros.com/images/midjourney_black-and-white-modern-john-henry.png" style="width: 400px"/>

[https://www.midjourney.com/app/jobs/fd061169-5522-4a36-8a34-55123ef74acf/](https://www.midjourney.com/app/jobs/fd061169-5522-4a36-8a34-55123ef74acf/)


---

# The Big Idea:

## We need to begin AI Augmentation now

;;;

![Karim Lakhani](https://eiwpyksmmiv.exactdn.com/wp-content/uploads/2022/01/Karim-Lakhani-preferred-headshot-3.10.20_cropped-360x360-1.jpg)

#### "AI Won’t Replace Humans — But Humans With AI Will Replace Humans Without AI"  

_Karim R. Lakhani, Co-Director of the Laboratory for Innovation Science at Harvard Business School_, author [Competing in the Age of AI](https://www.amazon.com/Competing-Age-AI-Leadership-Algorithms/dp/1633697622/ref=asc_df_1633697622/)


---

## Roadmap

- ✅ 30,000 foot view of how AI is being used & what results are being seen  
- ✅ Provide a framework for thinking about AI in your business  
- ✅ Provide examples of how we're thinking about it in our business  

Note:
- Focus on Generative AI

;;;

## Not Covered

- ❌ non-Generative AI
- ❌ Security
- ❌ Legal
- ❌ Privacy
- ❌ Bias

Note:
- Not because not important, but because only so much time.
- Open questions that very smart people still working on/disagree about.
- Things are moving very fast, so it needs to be on your radar.

---

## AI is already here

- You're _already_ using it (Siri, Alexa, Canva)
- Search Engines
- Social Media
- Digital Advertising
- Productivity Tools: Loom, Otter.ai, Notion, etc.
- Infrastructure (Dialpad, Twilio, etc.)
- And more...

Note:
- Expect to see more products have it embedded (Office, Google Suite, Notion, etc.)
- Example: Code/auto complete in my IDE

;;;

## What's Changed?

1. Accessibility
2. Interface
3. Adoption

Note:
- ChatGPT changed the interface in such a way that billions of people can now use it. It's put pressure on search, for instance.
- They also released an API, which provides programmatic accessibility, so applications can be built on top of it (or augmented with it)
- Adoption has increased because as accessibility increases, infrastructure cost of implemetnation is much lower than it used to be.
- Additionally, open source algorithms reduce the need for generalized data scientists

;;;

## How is AI being used?

| Use Case | What it does | Percent of Respondents |
| -------- | ------------ | ---------------------- |
| Robotic Process Automation | Automates repetitive tasks | 40% |
| Computer Vision | Analyzes images and video | 34% |
| **Natural Language Processing** 📈 | Analyzes text and speech | 31% |

_Source: McKinsey, [The State of AI in 2022](https://www.mckinsey.com/capabilities/quantumblack/our-insights/the-state-of-ai-in-2022-and-a-half-decade-in-review)_

Note: 
- We'll start with enterprise use cases, then come 'down market'

---

## Adoption is happening at the business unit

![Chart showing increasing use cases in the business units](/assets/chart_ai-use-case-at-business-unit.png)

Note:
- Idea is that AI is being released incrementally, not all at once.

;;;

### What are the top AI use cases?

![Chart showing top AI use cases](/assets/chart_ai-adopted-use-cases.png)

;;;

### What impacts are companies seeing?

![Chart showing top AI impacts](/assets/chart_ai-value-creation.png)

Note:
- Benefits to cost reduction in a potentially recessionary environment is a big deal.

---

## Leaders are growing faster than laggards are implementing

[McKinsey Report](https://www.mckinsey.com/capabilities/quantumblack/our-insights/the-state-of-ai-in-2022-and-a-half-decade-in-review)

- 25% of respondents attribute > 5% of EBIT attributable to AI investments
- 8% of respondents attribute > 20% of EBIT attributable to AI investments
    - Those 8% are getting to 20% faster than the 25% are getting to 5% or the laggards are starting.

Note:
- The companies making the investments are seeing bigger results, faster
- The 'toe dippers' are seeing some benefits, but not necessarily has huge. I'd wager that's changing now.

;;;

## Investments are increasing substantially

Percent of enterprises with > 5% of tech budget allocated to AI

| Year | % Respondents |
| ---- | ---------------------- |
| 2018 | 40%                    |
| 2022 | 52%                    |
| 2025 | 63%                    |

;;;

### How AI Strategy is being implemented

![Chart showing AI strategy implementation](/assets/chart_ai-strategy.png)

Note: 
- People Problem

;;;

### Data management is a problem

![Chart showing data management problems](/assets/chart_ai-data-health.png)

Note:
- Data Problem

;;;

### Knowing how to set up models & pipelines is a problem

![Chart showing model & pipeline problems](/assets/chart_ai-using-models-problem.png)

Note:
- Technical Problem

;;;

### Talent is a problem

![Chart showing talent problems](/assets/chart_ai-labor-problem.png)

Note:
- Labor/Talent Problem

;;;

### How are organizations solving this problem?

![Chart showing how organizations are solving the problem](/assets/chart_ai-how-talent-is-being-formed.png)

---

## Implications

1. AI is still in its infancy
2. But growing very quickly <!-- .element: class="fragment" -->
3. With measureable impacts to both revenue and cost structure <!-- .element: class="fragment" -->
4. Most companies don't have the context or talent to implement AI technologies as quickly as they'd like. <!-- .element: class="fragment" -->

---

# Practical Roadmap

;;;

## 4 Phases

1. AI as a Novelty - Explore & Experiment
2. AI as a Collaborator - Educate & Communicate <!-- .element: class="fragment" -->
3. AI in Infrastructure - Integrate & Automate <!-- .element: class="fragment" -->
4. AI as a Disruptor - Innovate <!-- .element: class="fragment" -->

Note:
- Will dive into each one of these, highlighting the opportunities, challenges, resources, and skills required for each.

---

## Phase 1: AI as a Novelty

Goal: Explore & Experiment

Use off the shelf tools to start exploring how AI might be useful in your business (for your team, operations, and/or customers)

;;;

| | | 
| --- | --- |
| **Biggest Opportunities** | Exploring the breadth of use-cases for off-the-shelf LLM/AI models for content-generation | 
| **Biggest Challenges** | Learning to write good prompts <br/> Imagining potential use cases |
| **Resources** | ChatGPT <br/> MidJourney / DALL-E <br/> "Normal" Office suite tools <br/> OTS Apps (Otter.ai) |
| **Skills Required** | Creativity <br/> Agile mindset <br/> Persistence |

;;;

## Example

> SuperWebPros needs to hire a new sales rep to follow up on some marketing campaigns. We need a job description that we can post for recruiting.

[https://chat.openai.com/share/c8a60072-96e5-4ca3-93c1-d1eb817d07c4](https://chat.openai.com/share/c8a60072-96e5-4ca3-93c1-d1eb817d07c4)

Note:
- Write emails, blogs, social media posts, and more
- Create graphics for social media, blogs, and more
- Review terms and conditions
- ...etc

---

## Phase 2: AI as a Collaborator  

Goal: Educate & Communicate  

Train your team to use the right tools & use AI to facilitate communication internally & externally.

;;;

| | | 
| --- | --- |
| **Biggest Opportunities** | Incorporate AI into your workflows and use it to improve communication and knowledge management for yourself and team. | 
| **Biggest Challenges** | Building a habit of using the tools <br/> [Generation gaps](https://chat.openai.com/share/4b0af427-bcbb-4424-ad53-26280e23610c) in using tools <br/> Shifting to a 'data as an asset' mindset |
| **Resources** | Database(s) <br/> Knowledge Base / Knowledge Management <br/> Content Repository <br/> Data Storage (e.g., Object storage) |
| **Skills Required** | Knowledge Management / Organization <br/> Content Management <br/> Model Training |

;;;

## Example

> Johnn is a Content Manager at SuperWebPros and responsible for reviewing, improving, and publishing content for ourselves and customers. We want to make sure we have a breadth of high-quality, transactional emails for communication.

[Template repository with pre-written customer emails/responses](https://br.superwebpros.com/database/161/table/731)

Note:
- [Template repository with pre-written customer emails/responses](https://br.superwebpros.com/database/161/table/731)
- [Improved training manuals/documentation](https://kb.superwebpros.com/t/wordpress-http-api-methods/369)
- [Team Learning & Training](https://chat.openai.com/share/849a650a-7c1d-44d6-b5a7-8ba5b1ff3450)
- [Help using software (Google Sheets, Excel, Photoshop, etc)](https://chat.openai.com/share/3073158b-3f1f-4f42-a1fb-0f25b71e8a48)

---

## Phase 3: AI in Infrastructure

Goal: Integrate & Automate  

Integrate AI into your operations and business process management software (ERP, CRM, etc)

;;;

| | | 
| --- | --- |
| **Biggest Opportunities** | Add natural language interfaces to your data and workflows | 
| **Biggest Challenges** | - Identifying processes that could benefit from AI implementation <br/> - Process optimization & development <br/> - Change management |
| **Resources** | - API infrastructure (n8n, Lambda, REST, etc) <br/> - ETL Tools, - Data Warehouses, etc. <br/> - Cloud processing <br/> - Vector Databases <br/> - LLM Libraries, AI Libraries |
| **Skills Required** | - Data management (ex: cleaning, pipelines) <br/> - API Development <br/> - No/Low Code App Building Tools |

;;;

## Example

> Jesse meets with clients and prospects all the time. He needs to be able to quickly summarize meetings and make 'task lists' for delegation. Also, team members frequently collaborate and make decisions about projects in meetings. We need to be able to summarize those decisions and make them visible.

[Voice to Summary workflow](https://n8n.superwebpros.com/workflow/10)

Note:
- [Email Summaries & Task Lists](https://n8n.superwebpros.com/workflow/1)
- Create a natural-language interface for your data
- Aggregate multiple databases into an embed store to cross-query data

---

## Phase 4: AI as a Disruptor

Goal: Innovate

Build industry-specific apps on top of AI APIs and cloud infrastructure

;;;

| | | 
| --- | --- |
| **Biggest Opportunities** | Build niche-specific products to sell to your industry. | 
| **Biggest Challenges** | Depends on the product |
| **Resources** | Completely depends on what you're building |
| **Skills Required** | Software development <br/> Data science/model building |

;;;

## Example

- What can you imagine?!

---

# Where to Go Next?

;;;

## Getting Started

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <h4>Download our FREE AI Starter Bundle</h4>
            <img style="width: 200px;" src="https://s3.amazonaws.com/resources.superwebpros.com/images/qr-codes/ai-starter-bundle-500.png"/>
            <caption><a href="https://sprwb.pro/3t9fitG">https://sprwb.pro/3t9fitG</a></caption>
        </div>
        <div class="col-md-6">
            <h4>Have a Project in Mind?</h4>
            <img style="width: 200px;" src="https://s3.amazonaws.com/resources.superwebpros.com/images/qr-codes/ai-application-500.png"/>
            <caption><a href="https://sprwb.pro/3Zx9lmv">https://sprwb.pro/3Zx9lmv</a></caption>
        </div>
    </div>
</div>

;;;

<img style="width: 200px;" src="https://www.superwebpros.com/wp-content/uploads/2021/04/cropped-SWP-logo-transparent-cropped-1024x226.png" alt="">

# Thank You!

Reach me: <a href="mailto:jesse@superwebpros.com">jesse@superwebpros.com</a>


